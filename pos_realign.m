function out = pos_realign(srcdir, destdir, animalID, sessionNum, epochnum, epochname, rawpos)

% AKG July2016; Demetris Roumis August 2016
% uses dio pulses and cam module frame numbers (if available) to redistribute videotimestamps
% If there is a landmark gap (~0.5s) at the beginning of dios and videotimestamps, align to it (less common)
% Else, do a nearest neighbor only looking backwards from the videotimestamps to the dios. (common)
% Interpolate across timestamp repeats in videotimestamps (rare)


videotimestamps = rawpos{sessionNum}{epochnum}.data(:,1);
disp(sprintf('realigning s%d ep%d',sessionNum, epochnum))
% 1. load camera hardware frame counts, if possible
cd(sprintf('%s%s',srcdir,epochname))
try 
    camlog = readTrodesExtractedDataFile(sprintf('%spos_cameraHWFrameCount.dat',epochname(1:end-3)));
    %    get rid of wrap around so I can use the camlog values
    camlog.field.data = double(camlog.field.data);
    wrapz = diff(camlog.field.data) < 0;
    wrapval = camlog.field.data(find(diff(wrapz) < 0,1));
    cumcammult = cumsum([false, wrapz]);
    corrcamlog = cumcammult*wrapval+corrcamlog;
catch
    disp('no CameraHWframecount file found')
    corrcamlog = [];
end

% 2. load DIOs (from ff folder, not binaries)
cd(destdir)
try
    load(sprintf('%s%sDIO%02d.mat', destdir,animalID,sessionNum))
    for d = 1:length(DIO{sessionNum}{epochnum})
        %identify cam chan by rate; should be ~30hz on avg (btwn 28-32)
        % only look at times of state=1 (pulse onset)
        warning('off', 'MATLAB:mode:EmptyInput')
        if length(DIO{sessionNum}{epochnum}{d}.values)>1000  %if there are enough events.. DR: let's brainstorm more error-proof ways to detect cam channel.. maybe validate by reading the config for the channels that got suppressed in statescript?
            if isExcluded(1/mode(diff(DIO{sessionNum}{epochnum}{d}.times(DIO{sessionNum}{epochnum}{d}.values==1))),[28, 32]);
            ratecheck(d) = 1;
            end
        else
            ratecheck(d) = 0;
        end
    end
    if sum(ratecheck) == 0
        dios = [];
        disp('no camerasync chan found in dios')
    elseif sum(ratecheck) > 1
        disp(sprintf('multiple potential camerasync chans found. using ch %d',find(ratecheck>0,1)))
        % only use pulse onset times (value = 1)
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.values==1);
    else 
        %only use pulse onset times (value =1 )
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0)}.values==1);
        disp(sprintf('using camerasync chan %d for timestamp realigmnent',find(ratecheck>0)))
    end        
catch
    dios = [];
    disp('no dio files found for this session')
end

% -------Realignment Strategy # 1: Landmark Start Gap present in both DIO and videotimestamps (Best case scenario..)-----
% uses .5 sec gap in frame acquisition at recording start as landmark to align first pulse with correct frame.
% if camlogs available, use them to remove dios corresponding to dropped frames and realign based on order starting after the gap
% if camlogs unavailable, use FIRST 1000 timestamps/dios to calculate a standard delay. if delay-matched dio found, 
%   reassign timestamp. Deal with remaining laggy times by redistributing.

if isempty(dios)
    disp('currently no correction strategy without camera strobed; aborting pos_realign :(  ')
    out = videotimestamps;
    return; 
% elseif 
%1: DRfix check for repeats and interp videotimestamps
%elseif: there is are gaps with at least a frame before the gap for both dio and videotimestamps.. do anna's
%else do a nearest neighbor only looking backward (lookup(x, y, -1))
%fix anna's code to use unwrapped camlog instead of wrapped
    
else % first, initialize and determine whether landmark gap present
    
    newTimeStamps = zeros(length(videotimestamps),1);
    %initial recording starttime should be a field in dios, but currently isnt. for now, get it from Binary2FF_LFP output
    try
        load(sprintf('EEG/%seeg%02d-%d-01.mat',animalID,sessionNum,epochnum))
        starttime = double(eeg{sessionNum}{epochnum}{1}.starttime)/30000;
        endtime = double(eeg{sessionNum}{epochnum}{1}.endtime)/30000;
        % ANNA GET STARTTIME FROM .TIME INSTEAD OF LFP
    catch
        error('need to process LFP first to get starttime landmark')
    end
    gapfound = 0;
    
    findPulseGap = find(dios(1:100)-starttime>.4,1);
    if ~isempty(findPulseGap)
        if findPulseGap == 1
            disp('First pulse after gap is first dio');
            pulseIndAfterGap = findPulseGap;
        else
            disp(['First dio pulse after gap is #', num2str(findPulseGap)]);
            pulseIndAfterGap = findPulseGap;
        end
    else
        pulseIndAfterGap = 1;
        disp('no gap in dios found; using first dio instead')
        gapfound = 0;
    end
    findFrameGap = find(videotimestamps(1:100)-starttime>.4,1);
    if ~isempty(findFrameGap)
        if findFrameGap == 1
            disp('First frame after gap is first frame');
            frameIndAfterGap = findFrameGap;
        else
            disp(['First frame after gap is: ' num2str(findFrameGap)]);
            frameIndAfterGap = findFrameGap;
        end
    else
        frameIndAfterGap = 1;
        disp('no gap in frames found; using first frame instead')
        gapfound = 0;
    end
    % next, deal with any frames before the gap/ assign initial timestamp(s)
    if gapfound
        %newTimeStamps(frameIndAfterGap) = dios(pulseIndAfterGap);
        %dios(pulseIndAfterGap) = NaN;
        
        %% I'm a bit confused here..
        %fra:     gap     |
        %dio: | gap  | | |
        %for one of my epochs that starts with that relative timing, this become pulseIndAfterGap = 2 and frameIndAfterGap = 1 so newTimestamps(1:0) = dios(2:1)????
        %i think this will be very common as people have variable starting timepoints 
        %i think what we want is to start with is the timestamps of our first frame and do a nearest on the dio's, 
        
        if frameIndAfterGap<=pulseIndAfterGap %can just assign directly 
            newTimeStamps(1:frameIndAfterGap-1) = dios(pulseIndAfterGap-frameIndAfterGap+1:pulseIndAfterGap-1);
            
        else %if there aren't enough dios, interpolate
            newTimeStamps(1:frameIndAfterGap-1) = interp1([1 frameIndAfterGap],[starttime,dios(pulseIndAfterGap)],[1:frameIndAfterGap-1]);
        end
        %dios(1:pulseIndAfterGap-1) = NaN;
    elseif pulseIndAfterGap~=1 %gap in dios only; assign first frame to first dio after gap
        newTimeStamps(1) = dios(pulseIndAfterGap);
        %dios(1:pulseIndAfterGap) = NaN;
    else
        frameIndAfterGap = 1; % even if there was a gap in videots, ignore it
        if videotimestamps(1)<dios(1)  %can't use a dio if it's in the future
            newTimeStamps(1) = videotimestamps(1);
        else
            newTimeStamps(1) = dios(1);
            dios(1) = NaN;
        end
    end
    dioslost = find(diff(dios)>.04);
    dioslosttime = diff([dios(dioslost) dios(dioslost+1)],1,2); %get time lost from dropped dios
    %  if camlogs available, use them to get rid of specific dios
    if ~isempty(camlog)
        disp('using dios and camlog')
        framecountdiff = diff(corrcamlog);
        % identify location and # of dropped frames
        dropped_idx = find(framecountdiff>1 & framecountdiff<30000);  %deal with camcount wraparound
        num_dropped = framecountdiff(dropped_idx);
        %Exclude DIOs corresponding to dropped frames
        for d = 1:length(dropped_idx)
            dios = dios([1:dropped_idx(d),dropped_idx(d)+num_dropped(d):end]);
        end
        
        %reassign timestamps based on order from beginning
        if length(newTimeStamps(frameIndAfterGap:end))>=length(dios(pulseIndAfterGap:end))
            newTimeStamps(frameIndAfterGap:frameIndAfterGap+-1+length(dios(pulseIndAfterGap:end))) = dios(pulseIndAfterGap:end);
            if any(newTimeStamps(frameIndAfterGap:end)==0)
                disp('not enough dios, interping')
                toReplace = find(newTimeStamps==0);
                if toReplace(1)==0  %in case first timestamp is 0, highly unlikely
                    toReplace = toReplace(2:end);
                end
                newTimeStamps(toReplace)=interp1([toReplace(1)-1, length(newTimeStamps)+1],[newTimeStamps(toReplace(1)-1),endtime],toReplace);
                
            end
        else % too many dios, just disregard extra ones at end
            newTimeStamps(frameIndAfterGap:end) = dios(pulseIndAfterGap:pulseIndAfterGap+(length(newTimeStamps)-frameIndAfterGap));
        end
        
    else
        disp('using dios only')        
        %calculate standard delay based on first 1000 dios/timestamps
        %based on the assumption that the last 30s of data is likely the cleanest
        %dios=dios(dios<videotimestamps(end)); %if any dios occur after final timestamp, get rid of them
        delay = mean(videotimestamps(frameIndAfterGap:frameIndAfterGap+1000)-dios(pulseIndAfterGap:pulseIndAfterGap+1000));
        delaystd = std(videotimestamps(frameIndAfterGap:frameIndAfterGap+1000)-dios(pulseIndAfterGap:pulseIndAfterGap+1000));
        disp(sprintf('delay is %04f, std is %04f',delay,delaystd))
        if (delay <=0 | delaystd>=delay )
            error('delay doesnt make sense')
        end
        %calc standard inverval btwn frames (use to tell if timestamps are regularly spaced vs piled up)
        intervals = diff(videotimestamps);
        Fs = mean(intervals(1:1000));
        Fsstd = std(intervals(1:1000));
        intervals = [intervals;Fs]; %fix length
        
        % if there's a perfect dio match during periods of clean acquisition, assign them
        for frameind = frameIndAfterGap:length(videotimestamps)-1 %start from end and work backwards
            if (abs([intervals(frameind) intervals(frameind+1)]-Fs) < Fsstd) %if intervals are regular
                diomatch_idx = find((dios>(videotimestamps(frameind)-delay-delaystd) & dios<(videotimestamps(frameind)-delay)));
                if ~isempty(diomatch_idx) %match found! remove it from available pool(prevent repeats) and update newTimeStamps
                    newTimeStamps(frameind) = dios(diomatch_idx);
                    dios(diomatch_idx) = NaN;
                end
            end
        end
        
        %find any uncorrected timestamps and figure out what they should be based on neighbors
        if newTimeStamps(end)==0 %deal with end condition
            if ~isnan(dios(end))
                newTimeStamps(end) = dios(end);
                dios(end) = NaN;
            else
                newTimeStamps(end) = endtime;
            end
        end
        if newTimeStamps(frameIndAfterGap)==0 %deal with start condition
            if ~isnan(dios(pulseIndAfterGap))  %this is most likely
                newTimeStamps(frameIndAfterGap) = dios(pulseIndAfterGap);
                dios(pulseIndAfterGap) = NaN;
            elseif frameIndAfterGap==1  %this will be slightly off, but it shouldn't conflict
                newTimeStamps(frameIndAfterGap) = starttime;
            else
                newTimeStamps(frameIndAfterGap) = newTimeStamps(frameIndAfterGap-1)+delay;
            end
        end
        preprob = find(diff(newTimeStamps==0)==1);  %define borders of problem areas
        postprob = find(diff(newTimeStamps==0)==-1)+1;
        if length(preprob)~=length(postprob)
            error('imbalanced problems')
        end
        for p = 1:length(preprob)
            diosInRange = find(dios>newTimeStamps(preprob(p)) & dios< newTimeStamps(postprob(p)));
            if (postprob(p)-preprob(p)-1)==length(diosInRange)
                newTimeStamps(preprob(p)+1:postprob(p)-1) = dios(diosInRange);
                dios(diosInRange) = NaN;
            else % don't use dios, just interpolate between neighbors
                if preprob(p)<frameIndAfterGap  %special initial condition
                    newTimeStamps(preprob(p)+1:postprob(p)-1) = interp1([preprob(p) postprob(p)],[dios(pulseIndAfterGap) newTimeStamps(postprob(p))],[preprob(p)+1:postprob(p)-1]);
                else
                    newTimeStamps(preprob(p)+1:postprob(p)-1) = interp1([preprob(p) postprob(p)],[newTimeStamps(preprob(p)) newTimeStamps(postprob(p))],[preprob(p)+1:postprob(p)-1]);
                end
            end
        end        
    end
    
    check = find((videotimestamps-newTimeStamps)<0);  %sometimes the interpolation causes backwards correction
    if ~isempty(check)
        newTimeStamps(check) = videotimestamps(check);
    end
    %assess hehe
    disp(sprintf('putative dropped frames/extra dios: %d',length(dios(pulseIndAfterGap:end))-length(newTimeStamps(frameIndAfterGap:end))))
    disp(sprintf('any dios lost: %d',length(dioslost)))
    disp(sprintf('average lag: %04f',mean(videotimestamps-newTimeStamps)))
    if ~isempty(camlog)
        disp(sprintf('dropped frames:%d',sum(num_dropped)))
    else
        disp(sprintf('dios unused:%d',sum(~isnan(dios))))
    end
    
 end
    
% sanity check1: no original videotimestamp should ever precede corrected timestamps
if any((videotimestamps-newTimeStamps)<0)
    error('no original videotimestamp should ever precede corrected timestamps, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end
% sanity check2: no duplicate timestamps should ever be found
if any(diff(newTimeStamps)<=0)
    error('duplicate or inverted timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 
% sanity check3: no nan timestamps should happen
if any(isnan(newTimeStamps))
    error('nan timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 
% sanity check4: no zero timestamps should happen
if any(newTimeStamps==0)
    error('zero timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 

% testing
%     figure
%     plot(1:3,repmat(starttime,1,3),'*')
%     hold on
%     plot([dios(1:1000),videotimestamps(1:1000),newTimeStamps(1:1000)]','.-') 
%     title('check beginning')
%     figure
%     plot([dios(end-5000:end),videotimestamps(end-5000:end),newTimeStamps(end-5000:end)]','.-')
%     title('check end')
   
    out = newTimeStamps;
    
end 
