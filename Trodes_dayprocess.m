% Frank lab PRE-PROCESS
% BinaryFiles ---> Frank Framework Files
% Suggested Use: Make one script per animal to call this function
% Authors:
% Demetris: 2015 - 2019
% Anna

function Trodes_dayprocess(animal, date, day, varargin)
fprintf('dayprocess date %d day %d\n',date, day);
adjusttimestamps = 0;

% ------- Info structs ------------------
% Tags reserved: days, epochs, ntrodes
% empty days, epochs, ntrodes include ALL of that kind

% taskinfo
epochtypesordered = {};   % for basic run/sleep assignments
epochtags = [];   % for passing in any additional epoch labels 

updatetaskinfo = 0;

% tetinfo
ntrodetags = [];
updatetetinfo = 0;

% cellinfo
clustertags = [];
updatecellinfo = 0;

% ------------------ LFP ------------------
makefflfp = 0;
makereferencedfflfp = 0; useconfig = 0;
makeripplefilteredlfp = 0;  %ripple (140 - 250 Hz) samprate 1500 Hz
makethetafilteredlfp = 0; % theta (6-9 Hz) samprate 1500 Hz
makelowgammafilteredlfp = 0;  % lowgamma (20 - 50 Hz) samprat 1500 Hz
makefastgammafilteredlfp = 0; % highgamma (65 - 95 Hz) samprat 1500 Hz
makehighgammafilteredlfp = 0;  % fastgamma (65 - 140 Hz) samprat 1500 Hz
makenoisefilteredlfp = 0; % 500-600 Hz samprate 1500 Hz

% ------------------ Spikes ------------------
% clustered
rmdeadmdachans = 0; % Remove dead channels from mda files prior to sorting
% To just update cellinfo with metrics, run makeFFspikes with overwriteSpikes flag off
makeffspikes = 0;
overwritespikes = 0;
ntrodestosort = [];
sorter = 'ms4'
metricsperepoch = 1;

% clusterless marks 
makeffmarks = 0;
markntrodes = []; marksource = ''; 


% ------------------ Position and Behavior ------------------
makeffdio = 0;
makeffpos = 0;
makefflinpos = 0;
makelinearcoords = 0;
makeffbehavestate = 0;
addlinpostotask = 0;
fit_offset_override = 0;
cmperpix = [];
possource = '';
posignoretimesidx = [];
taskcoordinateprogram = [ {'wtrack'} {'getcoord_wtrack'}; {'sixarmtrack'} {'getcoord_6armtrack'}]; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
behavestateprograms = [ {'wtrack'} {'createrewardinfo_wtrack'} {[]} {[]}; {'sixarmtrack'} {'createrewardinfo_multW'} {[]} {[]}]; %each row: epochtype rewardinfoprogram inputdios outputdios
usevidframe = 0;
overwritetracknodes = 0;
overwriterewinfo = 0;

% ------------------ Event Detection ------------------
extractrippleskonsensus = 0; % new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m
extractripplesjy = 0;  epfilter = 'isequal($environment,''run'')';% extract consensus rips with gaussian noise estimate of threshold. defaults to run eps
extractnoisekonsensus = 0;
extractmuaripples = 0;  muasource = 'marks'; muanstd = 3;
ripmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
ripnstd = 2; % > # std ripples (start with 2)
maxpeakval = 2000;
automaxval = 0;
excludenoise = 0;
excludeevents = '';
noisemindur = 1/1500; % time (in seconds) which the signal must remain above threshold to be counted as noise (start with 1 sample)
noisenstd = 3; % > # std noise events (start with 2)
noise_excludetime = .5; % window around noise events to exclude
consensusriparea = ''; %define source region of ripples.. saved structure will contain this string e.g. 'por'

% ___________________ Debugging ___________________
checkforcorrupteddata = 0;
% ___________________
dp = struct; % packaged vars made by runDayPreProcess.. check it out man

% convert all input arguments to lowercase to deal with different capitalization conventions
for v = 1:length(varargin)
    if ischar(varargin{v})
       varargin(v) = lower(varargin(v));
    end
end

if ~isempty(varargin)
    assign(varargin{:});
end

dpf = fieldnames(dp);
for f = 1:length(dpf)
    eval([dpf{f} '= dp.' dpf{f} ';']);
    pause(.01) % don't outpace the eval
end
% ___________________ Var Capitalization Cluster F...ix _______________
% make all input params lowercase for universal compatibility
% w = evalin('caller', 'whos');
% w = whos;
% for a = 1:length(w)
%     eval([lower(w(a).name) '=' w(a).name ';']);
%     pause(.01) % don't outpace the eval
% end


%% Initialize
animalinfo = animaldef(lower(animal));
animal = animalinfo{1,3};
FFanimdir =  sprintf('%s',animalinfo{1,2});
rawdaydir = sprintf('%s%d/',animalinfo{1,4}, date);
preprocdaydir =  sprintf('%s%d/',animalinfo{1,5}, date);
if ~isdir(FFanimdir); mkdir(FFanimdir); end
if ~isdir([FFanimdir 'EEG']); mkdir([FFanimdir 'EEG']); end
if ~isdir(preprocdaydir); mkdir(preprocdaydir); end
cd(preprocdaydir)
%% check the log files of the binaries for corrupted data
if checkforcorrupteddata
    cd(preprocdaydir)
    cd ..
    %direct call to the system.. but there's no way to save the output
    !grep -H 'Warning' ./*/*/*.log
    %direct call to the system.. but output currently gets saved as 1D char string.. need to fix this
    [~,warninglog] = system('grep -H "Warning" ./*/*/*.log')
end
%% Adjust Timestamps
if adjusttimestamps
    adjust_timestamps(preprocdaydir);
end
%% LFP
if makefflfp   % unreferenced LFP 'eeggnd'
    Binary2FF_LFP(animal, preprocdaydir, FFanimdir, day);
end
if makereferencedfflfp % referenced LFP 'eeg'
    fprintf('lfpreferencing %d day %d\n',date, day);
    FF_LFPreferencer(animal, rawdaydir, FFanimdir, day, 'useconfig',useconfig);
end
%% DIO
if makeffdio
    Binary2FF_DIO(animal, preprocdaydir, FFanimdir, day);
end
%% POS
if makeffpos
    if isempty(cmperpix)
        for ep = 1:length(epochtypesordered{day})
            if strcmp(epochtypesordered{day}{ep},'sleep')
                cmperpix = [cmperpix sleepcmperpix];
            else
                cmperpix = [cmperpix runcmperpix];
            end
        end
    end
    Binary2FF_POS(animal, preprocdaydir, FFanimdir, day, date, 'cmperpix', ...
        cmperpix, 'posSource', possource, 'posignoretimesidx', posignoretimesidx, ...
        'fit_offset_override',fit_offset_override);
end

%% create or update tet/cell info
if updatetetinfo % init scrapes from lfp, marks
        tagNTrodes(FFanimdir, animal, day, ntrodetags);
end
if updatecellinfo % init scrapes from spikes
        tagClusters(FFanimdir, animal, day, clustertags);
end

%% CREATE/UPDATE TASK INFO STRUCTURE
%createtaskstruct has to come after makeFFpos

if updatetaskinfo % add epoch tags to TASK INFO STRUCT
    if ~isempty(epochtypesordered)
        cd(FFanimdir);
        disp('creating or updating taskstruct files')
        for t = 1:length(epochtypesordered)
            if strcmp(epochtypesordered{t},'sleep')
                minimalepochtags = {'environment', 'sleep', 'type', 'sleep'};
            else
                minimalepochtags = {'environment', epochtypesordered{t}, 'type', 'run'};
            end
            addtaskinfo(FFanimdir, animal, day, t, minimalepochtags); 
        end
    else
        error('epochtypesordered required')
    end
    if ~isempty(epochtags)  % this adds any additional tags that were passed in in DR format
        tagEpochs(FFanimdir, animal, day, epochtags);
    end
end

%% SPIKES
% remove dead channels from MDA files before sorting
% * id of the dead channel is based on the hardware number, not order in the config
% dead channel listings should first be in tetinfo
if rmdeadmdachans
    stripDeadMDAchans(animal, preprocdaydir, FFanimdir, day, date)
end
if makeffspikes
    Binarymda2FF_SPIKES(animal, preprocdaydir, FFanimdir, day, num2str(date),...
        'sorter',sorter,'ntrodes',ntrodestosort, 'metricsperepoch',...
        metricsperepoch, 'overwrite', overwritespikes);
end
if makeffmarks
    Binary2FF_MARKS(animal, preprocdaydir, FFanimdir, day, num2str(date), ...
        'ntrodes',markntrodes,'marksource',marksource);
end
%% create and add LINPOS LANDMARKS TO TASK STRUCT
if makelinearcoords 
    if isempty(cmperpix)
        for ep = 1:length(epochtypesordered{day})
            if strcmp(epochtypesordered{day}{ep},'sleep')
                cmperpix = [cmperpix sleepcmperpix];
            else
                cmperpix = [cmperpix runcmperpix];
            end
        end
    end
    if ~isempty(epochtypesordered{day})
        cd(FFanimdir);
        fprintf('creating and adding linpos landmarks to task \n')
        for ep = 1:length(epochtypesordered{day})
            dayepochlist = [day ep];
            if strncmp(epochtypesordered{day}{ep},'wtrack', length('wtrack'))
                coordprogramInd = find(strcmp(taskcoordinateprogram(:,1),'wtrack'));
                fprintf('using %s for the taskcoordinateprogram \n', ...
                    taskcoordinateprogram{coordprogramInd, 2});
                createtaskstruct(FFanimdir, animal, dayepochlist, ...
                    taskcoordinateprogram{coordprogramInd, 2}, 'overwrite', ...
                    overwritetracknodes);
            elseif strncmp(epochtypesordered{day}{ep},'sixarmtrack', length('sixarmtrack'))
                coordprogramInd = find(strcmp(taskcoordinateprogram(:,1),'sixarmtrack'));
                fprintf('using %s for the taskcoordinateprogram \n', ...
                    taskcoordinateprogram{coordprogramInd, 2});
                createtaskstruct(FFanimdir, animal, dayepochlist, taskcoordinateprogram...
                    {coordprogramInd, 2}, 'overwrite',overwritetracknodes, 'usevidframe', ...
                    usevidframe, 'cmperpix', cmperpix);
            else
                continue
                warning('unrecognized or missing coordinate program')
            end
        end
    else
        error('epochtypesordered required')
    end
end

%% CREATE LINPOS FILES
%this has to come after addlinpostotask (make linear coords)
if makefflinpos
    lineardayprocess(FFanimdir, animal, day, 'welldist', 5);
end
%% CREATE Ripple/Theta/Gamma-BAND LFP FILES
if makeripplefilteredlfp
    createRippleFilteredLFP(FFanimdir, animal, day)
end
if makethetafilteredlfp
    createThetaFilteredLFP(FFanimdir, animal, day)
end
if makelowgammafilteredlfp
    createLowGammaFilteredLFP(FFanimdir, animal, day)
end
if makefastgammafilteredlfp
    createFastGammaFilteredLFP(FFanimdir, animal, day)
end
if makehighgammafilteredlfp
    createHighGammaFilteredLFP(FFanimdir, animal, day)
end
if makenoisefilteredlfp
    % 500-600 Hz.. used for detection of noise events to be excluded from 
    % ripple event detection
    createNoiseFilteredLFP(FFanimdir, animal, day)
end
%% EXTRACT Noise Events CONSENSUS METHOD: this version individually squares, smooths, sqroots, then means traces
if extractnoisekonsensus
    for eventarea = 1:length(consensusriparea)
        kontetfilters = {sprintf('(isequal($area, ''%s'') && ~isequal($rip_detect, false))', consensusriparea{eventarea})}; %define source region of ripples
        if isempty(consensusriparea{eventarea}) || isempty(kontetfilters); disp('consensusRIParea and kontetfilters are empty!! aborting'); return; end
        extractNoiseConsensus(FFanimdir, animal,sprintf('%snoise', ...
            consensusriparea{eventarea}), 'noise', 'ripple', day, ...
            kontetfilters,noisemindur,noisenstd, 'NOISE_excludetime', noise_excludetime)
    end
end
%% EXTRACT RIPPLES CONSENSUS METHOD: this version individually squares, smooths, sqroots, then means traces
if extractrippleskonsensus
    for eventarea = 1:length(consensusriparea)
        kontetfilters = {sprintf('(isequal($area, ''%s'') && ~isequal($rip_detect, false))',...
            consensusriparea{eventarea})}; %define source region of ripples
        if isempty(consensusriparea{eventarea}) || isempty(kontetfilters)
            disp('consensusRIParea and kontetfilters are empty!! aborting');
            return
        end
        %         extractEventConsensus(FFanimdir, animalID, sprintf('%sripples',consensusRIParea{eventarea}), 'ripple',sessionNum,kontetfilters,RIPmindur,RIPnstd, 'maxpeakval', maxpeakval, 'automaxval', automaxval)
        AG_extractEventConsensus(FFanimdir, animal, sprintf('%sripples', ...
            consensusriparea{eventarea}), 'ripple',day,kontetfilters,ripmindur, ...
            ripnstd, 'excludeEvents', excludeevents, 'excludenoise', 0, 'maxpeakval', maxpeakval)
    end
end
%% EXTRACT RIPPLES KK/JAI METHOD: combine relevant tetrodes into a consensus trace, then estiamte gaussian noise in order to set a less arbitrary detection threshold
if extractripplesjy
    cd(FFanimdir);
    for riparea = 1:length(consensusriparea);
        conRIPtetfilters = {sprintf('(isequal($area, ''%s''))', consensusriparea{riparea})}; %define source region of ripples
        if isempty(consensusriparea{riparea}) || isempty(conRIPtetfilters); disp('consensusRIParea and conRIPtetfilters are empty!! aborting');
            return;
        end
        AG_extractRipplesJY(FFanimdir, animal, sprintf('%sripples',consensusriparea{riparea}), 'ripple',day,conRIPtetfilters,ripmindur,[],'epfilter',epfilter) %empty nstd means it will be calculated from noise estimate
    end
end

%% EXTRACT MUA RIPPLES : combine relevant tetrodes to generate mua trace, smooth, detect events above threshold (davidson 2009 method)
if extractmuaripples
    cd(FFanimdir);
    tetfilter = sprintf('(isequal($area, ''%s''))', consensusriparea{1});
    extractMUAevents(FFanimdir, animal, day, tetfilter, ripmindur, muanstd, muasource);
end

%% parses DIO into a behaveState struct
% DR w track version 
if makeffbehavestate
    if ~isempty(epochtypesordered{day})
        cd(FFanimdir);
        fprintf('creating behaveState struct for Day%d\n', day)
        for ep = 1:length(epochtypesordered{day})
            dayepochlist = [day ep];
            if strncmp(epochtypesordered{day}{ep},'wtrack', length('wtrack')) %will pickup anything that starts with 'wtrack'.. including 'wtrackrotated'
                behaveStateProgramInd = find(strcmp(behavestateprograms(:,1),'wtrack'));
                behaveStateProgram = behavestateprograms{behaveStateProgramInd, 2};
                inputdios  = behavestateprograms{behaveStateProgramInd, 3};
                outputdios = behavestateprograms{behaveStateProgramInd, 4};
                %                 disp(sprintf('using %s for the rewardInfoprogram', rewardInfoprogram));
                createBehaveState(FFanimdir, animal, dayepochlist, behaveStateProgram,inputdios,outputdios, 'overwrite', overwriterewinfo); %, 'overwrite',overwriteTrackNodes);
                %             elseif strncmp(epochtypesordered{t},'sixarmtrack', length('sixarmtrack'))
                %                 rewardprogramInd = find(strcmp(rewardInfoprogram(:,1),'sixarmtrack'));
                %                 disp(sprintf('using %s for the rewardInfoprogram', rewardInfoprogram{rewardprogramInd, 2}));
                %                 createrewardinfo(FFanimdir, animal, dayepochlist, rewardInfoprogram{rewardprogramInd, 2}); %, 'overwrite',overwriteTrackNodes, 'usevidframe', 1, 'cmperpix', cmperpix);
            else
                continue
                %                 error('unrecognized or missing rewardInfo program')
            end
        end
    else
        error('epochtypesordered required')
    end
end
%%
clear all
end