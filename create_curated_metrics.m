

% use the curated firings exported from mountainview to create curated
% cluster metrics

animal = 'JZ1';
date = [20161117];
pipe = 'ms2';

animalinfo = animaldef(lower(animal));
animalID = animalinfo{1,3}; %use anim prefix for name
PPanimdir =  sprintf('%s',animalinfo{1,5});
msOutputDir = sprintf('%s%d/%d_%s.mountain/output',PPanimdir,date, date, animal);
cd(msOutputDir)
pipeNTdirs = dir(sprintf('%s*',pipe));
setenv('LD_LIBRARY_PATH', '/usr/lib:'); % for this session only, use the native c compilers 
create_curatedmetrics = 'mp-run-process mountainsort.isolation_metrics --timeseries=raw.mda.prv --firings=firings.curated.mda --metrics_out=curated_metrics.json';
parfor iNT = 1:length(pipeNTdirs)
    cd(sprintf('%s/%s',msOutputDir,pipeNTdirs(iNT).name))
    if exist(fullfile(cd, 'raw.mda.prv'), 'file') && exist(fullfile(cd, 'firings.curated.mda'), 'file')
        [status,result] = system(create_curatedmetrics)
        disp(sprintf('created curated_metrics.json for ntrode# %s', pipeNTdirs(iNT).name))
    else
        disp(sprintf('missing raw.mda.prv or firings.curated.mda for ntrode# %s', pipeNTdirs(iNT).name))
    end
end