function createtetinfostruct(animdirect,fileprefix,append)
% createtetinfostruct(animdirect,fileprefix)
% createtetinfostruct(animdirect,fileprefix,append)
%
% This function creates a tetinfo file in the animal's data directory.
% For each tetrode, the depth and number of cells is saved.  If a tetinfo file
% exists and new data is being added, set APPEND to 1.

tetinfo = [];
if (nargin < 3)
    append = 0;
end
if (append)
    try
        load([animdirect, fileprefix,'tetinfo']);
    end
end

%create tetinfo for all tetrodes
% use the unreferenced 'eeggnd' files
eegfiles = dir([animdirect, 'EEG/', fileprefix, 'eeggnd*']);

for i = 1:length(eegfiles)
    load(sprintf('%s/%s/%s',animdirect,'EEG',eegfiles(i).name));
    allinfo = cellfetch(eeggnd, '', 'alltags', 1);
    if allinfo.index(1)<100 % will exlcude days with more than 2 digits (e.g. 1001)
        try
            idx = allinfo.index;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.samprate = allinfo.values{1}.samprate;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.clockrate = allinfo.values{1}.clockrate;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.starttime = allinfo.values{1}.starttime;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.endtime = allinfo.values{1}.endtime;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.refNTrode = allinfo.values{1}.refNTrode;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.nTrode = allinfo.values{1}.nTrode;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.nTrodeChannel = allinfo.values{1}.nTrodeChannel;
            tetinfo{idx(1)}{idx(2)}{idx(3)}.depth = [];
            tetinfo{idx(1)}{idx(2)}{idx(3)}.numcells = 0;
        catch
            warning('%s may be empty',eegfiles{i}{1})
        end
    end
end

save([animdirect,fileprefix,'tetinfo'],'tetinfo');