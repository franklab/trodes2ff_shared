%% For each tetrode, converts mountainsort spike indices to spike times, outputs each cluster's info as a struct

% Mari Sosa modified Jan 10 2017, after Jason Chung original July 03 2016 

%Requires: 
    %readmda.m, varargin2struct.m, loadjson.m, jsonopt.m, mergestruct.m, struct2jdata.m

%Inputs: 
    %firingsmda --> the CURATED firings.curated.mda from mountainsort **if there are 0 accepted clusters, this will be empty**
    %paramsjson --> the params.json file used for mountainsort clustering
    %real_timestamps --> read in from the ADJUSTED timestamps.adj.mda, separated by epoch (1 entry per sample of timeseries
    %real_timeinds --> indices of concatenated real_timestamps, separated by epoch
    %epoch --> current epoch number

%Output:
    %A single tetrode's spikes, containing cells with structs for each cluster on this tetrode
    %Clusters remain assigned to their cluster ID# as output by mountainsort

function tetrode = mda2spikes(firingsmda,paramsjson,real_timestamps,real_timeinds,epoch) 

tetrode={};
params = loadjson(paramsjson);
fdetails = dir(firingsmda);
clusters = readmda(firingsmda);

% if the firings file is empty once read in, there are no valid clusters
if fdetails.bytes==0 || isempty(clusters)  
    disp('no accepted clusters in this curation')
    tetrode = [];
else  % there are valid clusters! process them
 
    labels = clusters(3,:); % 3rd row: cluster labels
    timeinds = clusters(2,:); % 2nd row: index into timestamps (1st row is channel)(contains timeind of spikes from ALL eps concatenated)
    
    % Below we grab all the times for specific clusters
    final_cluster_labels = unique(labels);
    spikewidth = calc_mdaspikewidth;
    
    % convert real_timestamps from samples to seconds 
    real_timestamps_thisep = real_timestamps{epoch}/params.samplerate;
    
    % Iterate through clusters
    for ii=1:length(final_cluster_labels)
        
        %Get all spike indices for this cluster
         cluster_timeinds = timeinds(labels==final_cluster_labels(ii));
%       %Get cluster spikes that fall within the current epoch
         in_curr_epoch = cluster_timeinds>=real_timeinds{epoch}(1) & cluster_timeinds<=real_timeinds{epoch}(end);
         cluster_epochinds = cluster_timeinds(in_curr_epoch);
        
%         in_curr_epoch = timeinds>=real_timeinds{epoch}(1) & timeinds<=real_timeinds{epoch}(end);
%         cluster_epochinds = lookup(cluster_epochinds,real_timeinds{epoch});
%         labels(in_curr_epoch)==final_cluster_labels(ii);
                 
         if sum(cluster_epochinds)>0
            %Convert cluster spikes to within-epoch indices
            cluster_real_timeinds = lookup(cluster_epochinds,real_timeinds{epoch});        

            %Convert indices to times
            cluster_times=real_timestamps_thisep(cluster_real_timeinds); 
            % identify and remove any repeat timestamps; this can happen occaisionally as a result of cluster merging
            repeats = diff(cluster_times)==0;
            data=cluster_times(find([1 ~repeats]))'; %Put times into data
            %Construct cell struct, filled with NaN where data not available; timerange is start and end of epoch
            tetrode{final_cluster_labels(ii)}=struct('data',data,'descript','spike data','fields','time', 'depth', NaN, ...
                'label',final_cluster_labels(ii), 'spikewidth', NaN, 'timerange', [real_timestamps_thisep(1) real_timestamps_thisep(end)]);
            if ~isempty(spikewidth)
                tetrode{final_cluster_labels(ii)}.spikewidth = spikewidth(ii);
            end
        end
    end
    %clusterInds = find(~cellfun(@isempty,tetrode)); %get rid of empty cells from rejected/merged clusters
    %tetrode = tetrode(clusterInds); 
end

end