function [wellsdio, rewardinfo] = createBehaveState(FFanimdir, animalID, dayepochlist, behaveStateProgram,inputdios,outputdios, varargin)

% if (~isempty(varargin))
%     assign(varargin{:});
% end

%wtrack:
%createBehaveState_wtrack(directoryname,animalID,days,epochs,inputdios, outputdios, varargin)

%multiW: 
%createrewardinfo_multW(directoryname,prefix,days,epochs,rewarddelay,varargin)

%multiW_trackbacks: 
%createrewardinfo_multW_trackbacks(directoryname,prefix,days,epochs,rewarddelay,varargin)
eval(['behaveStateProgramHandle = @',behaveStateProgram,';']);
feval(behaveStateProgramHandle,FFanimdir,animalID,dayepochlist(:,1),dayepochlist(:,2),inputdios, outputdios, varargin);

end