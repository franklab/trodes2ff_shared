
function createBehaveState_wtrack(directoryname,animalID,days,epochs,inputdios, outputdios, varargin)

%Demetris Roumis April 2017
% produces the w track performance state series (<anim>BehavState.mat) from the DIO struct

% fields = ['lastlasttime lasttime currenttime lastlastwell lastwell currentwell correct inbound outbound timeportout wellportout'];
% initial(invalid) occurance of lastlasttime lasttime lastlastwell lastwell will be set to zero
% timeportout and wellportout of incorrect trials will be set to zero

%% From DIO, gets well start and end for all unique trajectories
overwrite = 0;
BehaveState.statechanges = [];
%set variable options
varargin = varargin{1}; %because passed from two-back functions
if (~isempty(varargin))
    assign(varargin{:});
end
try %does the BehaveState struct already exist? if so, append or overwrite for this day/epoch
%     eval(['load ',directoryname,animalID,'BehaveState']);
    load(sprintf('%s%sBehaveState.mat',directoryname, animalID), 'BehaveState');
catch
end
for day=days,
    DIOfile = sprintf('%s/%sDIO%02d.mat', directoryname, animalID, day);
    load(DIOfile);
    for epoch=epochs,
        try
            if ~isempty(BehaveState.statechanges{day}) %if we've loaded in a BehaveState struct for this day
                try %isempty will crash if the epoch exceeds current length.. in which case it's empty so continue to create it
                    if ~isempty(BehaveState.statechanges{day}{epoch})
                        if overwrite
                            disp(sprintf('overwriting previous BehaveState for day%d epoch%d',day, epoch))
                        else
                            continue %get out of this epoch since it's not empty and you don't want to overwrite
                        end
                    end
                catch
                end
            end
        catch
        end

        %gather the input ports next to their timestamps
        centerportIN = inputdios(1,2); %center well is the middle of three ints
        portINtimesWells = [];
        statechangeSeq = [];
        for i = 1:length(inputdios)
            portINtimesWells = [portINtimesWells; DIO{1,day}{1,epoch}{1,inputdios(i)}.times repmat(inputdios(i),length(DIO{1,day}{1,epoch}{1,inputdios(i)}.times),1)];
        end
        clockrate = DIO{1,day}{1,epoch}{1,inputdios(i)}.clockrate;
        [n,b]=histc(portINtimesWells(:,1),unique(portINtimesWells(:,1))); %get rid of duplicate timestamps in input.. seems like trodes bug
        portINtimes_fix = portINtimesWells(~(b == b(n>1)),:); %get rid of duplicate timestamps in input
        [~, portINsortind] = sort(portINtimes_fix(:,1)); %sort by time
        portINtimessort = portINtimes_fix(portINsortind,:);
        [pINdiff, ~, ~] = find(diff(portINtimessort(:,2))); %find the indices of the state changes
        portINtimesStateX = portINtimessort(pINdiff+1,:);
        lastlasttime = [0;0;portINtimesStateX(1:end-2,1)];
        lasttime = [0; portINtimesStateX(1:end-1,1)];
        lastlastwell = [0;0; portINtimesStateX(1:end-2,2)];
        lastwell = [0; portINtimesStateX(1:end-1,2)] ;
        statechangeSeq = [lastlasttime lasttime portINtimesStateX(:,1) lastlastwell lastwell portINtimesStateX(:,2) ];
        %correct if (currentouter ~= lastlast & last was center) or (currentcenter and last was an outer)
        statechangeSeq = [statechangeSeq ((statechangeSeq(:,6) ~= statechangeSeq(:,4) & (statechangeSeq(:,5) == centerportIN)) |...
            ((statechangeSeq(:,6) == centerportIN) & (statechangeSeq(:,5) ~= centerportIN)))]; 
        statechangeSeq = [statechangeSeq (statechangeSeq(:,5) ~= centerportIN)]; %inbound if last was an outer well
        statechangeSeq = [statechangeSeq (statechangeSeq(:,5) == centerportIN)]; %oubound if last was center well
        
        %% since, in Trodes, the output dio chans are staggered after all the inputs, search for their tag and convert
        original_ids = cellfetch(DIO{day}{epoch}, 'original_id');
        portOUTtimesWells = [];
        for outdio = 1:length(outputdios);
            outputdios_fix(outdio) = find(cell2mat(cellfun(@(x) strcmp(sprintf('Dout%d', outputdios(outdio)),x), original_ids.values, 'UniformOutput', false)));
            portOUTtimesWells = [portOUTtimesWells; DIO{1,day}{1,epoch}{1,outputdios_fix(outdio)}.times repmat(outputdios_fix(outdio),length(DIO{1,day}{1,epoch}{1,outputdios_fix(outdio)}.times),1)];
        end
        [n,b]=histc(portOUTtimesWells(:,1),unique(portOUTtimesWells(:,1))); %get rid of duplicate timestamps in output.. seems like trodes bug
        portOUTtimes_fix = portOUTtimesWells(~(b == b(n>1)),:); %get rid of duplicate timestamps in output
        [~, portOUTsortind] = sort(portOUTtimes_fix(:,1)); %sort by time
        portOUTtimessort = portOUTtimes_fix(portOUTsortind,:);
        statechangeSeq = [statechangeSeq zeros(size(statechangeSeq,1),2)];
        statechangeSeq(statechangeSeq(:,7)==1,[10 11]) = portOUTtimessort(lookup(statechangeSeq(statechangeSeq(:,7)==1,3), portOUTtimessort(:,1),1),:);  %look forward in time for the outputdio for each correct trial

        %% save the struct
        
        fields = ['lastlasttime lasttime currenttime lastlastwell lastwell currentwell correct inbound outbound timeportout wellportout'];
        BehaveState.statechanges{1,day}{1,epoch}.statechangeseq = statechangeSeq;
        BehaveState.statechanges{1,day}{1,epoch}.fields = fields;
        BehaveState.statechanges{1,day}{1,epoch}.trackbacks = 'no';
        BehaveState.statechanges{1,day}{1,epoch}.function = 'createBehaveState_wtrack';
        BehaveState.statechanges{1,day}{1,epoch}.task = 'wtrack';
        BehaveState.statechanges{1,day}{1,epoch}.clockrate = clockrate;
        
        save(sprintf('%s%sBehaveState.mat',directoryname, animalID), 'BehaveState');
       
        
    end
end
end


