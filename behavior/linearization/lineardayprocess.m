function lineardayprocess(directoryname,fileprefix,days, varargin)
%LINEARDAYPROCESS(directoryname,fileprefix,days, options)
%
%Runs linearizeposition for all run epochs in each day and saves the data in
%'linpos' in the directoryname folder.  See LINEARIZEPOSITION for the definitions 
%of the options.
%
%directoryname - example 'data99/user/animaldatafolder/', a folder 
%                containing processed matlab data for the animal
%
%fileprefix -    animal specific prefix for each datafile
%
%days -          a vector of experiment day numbers 
%
%                   'manual_segment' - [day1 epoch1 startindex1 endindex1 SEGMENTnumber1 ;  day2 epoch2 ...]
                        %   each row corresponds to a set of indices that
                        %   need to be manually changed -- this is after
                        %   you've verified separately that these indices
                        %   are currently being misclassified
                        %   (dfskk_thetaprecess)
                        % added kk 3.24.14
%  varagin:
%       maxv - specify the max velocity allowable between 2 time points
%           (restricts hugh jumps from errors in position reconstruction)
%       welldist - the radius around wells (in cm) to detect excursions
%           from and entries into the well zone (to define trajectory starts
%           and ends)
% 


lowercasethree = '';

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'lowercasethree'
            lowercasethree = varargin{option+1};
    end
end

days = days(:)';

for day = days
   linpos = [];
   
   dsz = '';
   if (day < 10)
      dsz = '0';
   end

   eval(['load ',directoryname,fileprefix,'task',dsz,num2str(day), '.mat']);
   eval(['task = ',lowercasethree,'task;'])
   for i = 1:length(task{day})   
      if (~isempty(task{day}{i})) && ~isfield(task{day}{i},'type') && ~isfield(task{day}{i},'environment')
         disp(sprintf('this epoch (d%d e%d) has no .type or .environment field!',day,i))
         
      %hard match for wtrack or wtrackrotated
      % else, run opech not containing 'open' go to multi w.. 
      % reason is many different naming conventions for multi w types
      elseif ((~isempty(task{day}{i})) && (strcmp(task{day}{i}.type,'run')) && ...
              ((strcmp(task{day}{i}.environment,'wtrack')) || ...
              (strcmp(task{day}{i}.environment,'wtrackrotated'))))
            
            disp(['Day ',num2str(day), ', Epoch ',num2str(i)])
            fprintf('w linearization for epoch env: %s \n', task{day}{i}.environment)
            index = [day i];
            [linpos{day}{i}.statematrix,linpos{day}{i}.segmenttable, linpos{day}{i}.trajwells, ...
                linpos{day}{i}.wellSegmentInfo, linpos{day}{i}.segmentInfo] ...
                = linearizeposition_wtrack(directoryname,fileprefix, index, varargin{:});
            linpos{day}{i}.statematrixfields = ['time startwell endwell segment segheaddir velocity lineardist'];
            linpos{day}{i}.segmenttablefields = ['trajnum segnum segmentID'];
         
      elseif ((~isempty(task{day}{i})) && (strcmp(task{day}{i}.type,'run')) ...
              && isempty(strfind(lower(task{day}{i}.environment),'open')) && ~isempty(task{day}{i}.environment))
          
            disp(['Day ',num2str(day), ', Epoch ',num2str(i)])
            fprintf('multi w linearization for epoch env: %s \n', task{day}{i}.environment)
            index = [day i];
            [linpos{day}{i}.statematrix,linpos{day}{i}.segmenttable, linpos{day}{i}.trajwells, ...
                linpos{day}{i}.wellSegmentInfo, linpos{day}{i}.segmentInfo]...
                = linearizeposition_multW(directoryname,fileprefix, index, varargin{:});
            linpos{day}{i}.statematrixfields = ['time startwell endwell segment segheaddir velocity lineardist'];
            linpos{day}{i}.segmenttablecolumns = ['trajNum segNumInTraj segmentID'];
      end
   end
   
   eval([lowercasethree,'linpos = linpos;']);
   eval(['save ',directoryname,fileprefix,'linpos',dsz,num2str(day),' ',lowercasethree,'linpos']);
     
end

