% [linpos, lincoordlist] = LINEARIZEPOS(posstruct, taskstruct, index)
%   01.18.17: renamed to createlinpos for clarity
%          Takes each position from the specified element of posstruct and
%  	   projects it onto the linear trajectory segments for the environment
%  	   and task defined in the specified element of taskstruct.
%          This routine finds the segment closest to each position, projects
%          the position to the closest location on the segment.
%	   linpos is the list of linearized positions and
%          lincoordlist is the number of the coordinate list and the segment
%          the animal is on at each time point.

% MS modified 1.10.17 from Annabelle Singer original
% MS 10.4.18 added manual segment assignment from Kenny 

function [linpos, segmentIndex, segdist] = createlinpos(pos, task, index, coord, varargin)

disp('Getting linpos...')
% pos = posstruct{index(1)}{index(2)}.data;
% task = taskstruct{index(1)}{index(2)};
linpos = pos;

z = find(pos(:,2) == 0);
nonz = find(pos(:,2));
pos = pos(nonz,:);

segmentIndex = zeros(size(pos,1), 1);

newpos = pos;

maxv = 500;
manual_segment = [];

%set variable options
for option = 1:2:length(varargin)-1
    switch varargin{option}
        case 'maxv'
            maxv = varargin{option+1};
        case 'manual_segment'
            manual_segment = varargin{option+1};
    end
end


% set the variable for the list of projection coordinates
trajcoord = task.linearcoord;

% project each position point onto each segment
ntraj = length(trajcoord);
nseg = length(coord);
maxsegdiff = 2; % set default maximum allowed seg diff at 2

% maxsegdiff is the maximum allowed number of segments that
% could be traversed from one time point to the next
if (size(coord,1) <= 3)
    % there are only one or two segments, so maxsegdiff isn't really
    % meaningful. We set it to 10 so that it is ignored
    maxsegdiff = 10;
else
    maxsegdiff = min([(length(coord)-2) maxsegdiff]);
end

inbound = 0;
lastvalid = -1;
segdist = zeros(size(pos,1),1);
for i = 1:size(pos,1)
    % project each position point onto the linear segments
    if (pos(i,2) == 0)
        % this is an invalid point, so set newpos to zero for this position
        newpos(i, 2:3) = 0;
    else
        tmppos = [];
        for j = 1:nseg
            
            % here input to projectpoint a SINGLE (x,y) coord (current 2D position), and the two (x,y) endpoint coords of a line segment
            try    % using smoothed position, if it exists
                tmppos{j} = projectpoint(pos(i,6:7), coord{j});
                % tmppos :
                % rows: one row for each segment
                % [sm-x sm-y distance onseg segnum] =
                % [xpos-of-closest-point-on-seg, ypos-of-closest-point-on-seg, distance-to-closest-point, whether-projected-point-was-within-segment, segment-number]
            catch
                %                 if do_once == 0
                disp('(!!) using x and y from pos{}{} instead of sm-x and sm-y..')
                %                 do_once = 1;
                %                 end
                tmppos{j} = projectpoint(pos(i,2:3), coord{j});
            end
            
            % change the segment number to equal the segment index
            tmppos{j}(:,5) = j;
        end
        
         % By sorting by distance, take the point that is the least distance from the segments and that
        % does not force an excessive velocity from the last point    
        tmppos = cat(1,tmppos{:});
        tmppos = sortrows(tmppos,3);
          
        % check the velocity to the last valid point -- if velocity for some segment exceeds
        % maxv, then disqualifies the segment from consideration
        if (lastvalid ~= -1)
                tmppnt = [0 0];
                tmppnt(1,1) = newpos(lastvalid,2); % x coord of last valid pt
                tmppnt(1,2) = newpos(lastvalid,3); % y coord of last valid pt
                % calculates velocity by calculating the distance from last **line-projected** valid pt to **line-projected** current point, then dividing
                % by time step
                vel = dist(tmppnt,tmppos(:,1:2)) ./ (newpos(i,1) - ...
                    newpos(lastvalid,1));
                % get the number of segments we will have moved across
                segdiff = abs(tmppos(:,5) - lastseg);
                % the next point is the first point in the list with a
                % corresponding velocity less than maxv
                newind = min(find((vel < maxv) & (segdiff <= maxsegdiff)));
                % sanity check for large discrepancies from the animal's
                % real current position - not used to alter assignment
%                 realgap = dist(pos(i,2:3),tmppos(:,1:2));
%                 if newind~=1 && (realgap(newind)-min(realgap))>5
%                     keyboard
%                 end
            if ~isempty(manual_segment)
                % if match day and epoch, then check to see if this
                % position index is to be manually assigned its segment #
                ind = find(manual_segment(:,1)==index(1) & manual_segment(:,2)==index(2))';
                for kk = ind
                    if i >= manual_segment(kk,3) && i <= manual_segment(kk,4)
                       % reassign
                       newind = find(tmppos(:,5)==manual_segment(kk,5)); 
                    end
                end
            end
        
%                     % if the closest point forces a transition between the
%                     % first and the last segment, use the next closest point.
%                     % This works both within and across linearcoord lists
%                     if ((abs(currentseg - lastseg)) == maxsegdiff)
%                         sprintf('correcting segment on point %d', i)
%                         ind = find(vel < maxv);
%                         if (size(ind,1) > 1)
%                             newind = ind(2);
%                         else
%                             newind = [];
%                         end
%                     end


            % If we couldn't find a linear position that passed all criteria:
            if isempty(newind)
                % set this point to zero, as it violates the velocity cutoff for
                % both linearcoord lists or the original point was zero
                sprintf('Warning: zero position at position element %d, terpos{%d}{%d}\n', i, index(1), index(2))
                
                newpos(i,2:3) = [0 0];
            else

                % Assign the segment index and distance
                currentseg = tmppos(newind,5);
                segmentIndex(i) = currentseg;
                newpos(i,2:3) = round(tmppos(newind, 1:2)); % rounds to the nearest cm and installs into col 2 + 3 of newpos
                lastseg = currentseg; %tmppos{clist}(newind{clist}, 5);
%                 segdist(i) = sqrt(sum((tmppos{clist}(1:2) - coord{clist}(1,:)).^2)); %the distance along the segment
                segdist(i) = sqrt(sum((tmppos(newind,1:2) - coord{currentseg}(1,:)).^2)); %the distance along the segment
                lastvalid = i;
            end
        else
            % the last point was not valid or no last valid segment has yet been found,
            % so pick the point with the minimum distance (always for the very first position point)
            newind = 1;
            [v, newind] = min(tmppos(:,3));
            newpos(i,2:3) = round(tmppos(newind, 1:2));
            segmentIndex(i) = tmppos(newind, 5);
            lastseg = tmppos(newind, 5);
            lastvalid = i;
        end
    end
end

% put the zeros back in to position, if they exist

linpos(nonz,2:3) = newpos(:,2:3);





