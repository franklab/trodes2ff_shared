function [data] = copyAndUpdateTrodesExtractedDataFile(filename_orig, filename_new, field_new, varargin)  
% writeUpdatedTrodesExtractedDataFile(fname_orig, fname_new, fields_new)
% 
% This is not a generic function to write Trodes Extracted Data from
% scratch. Rather, it is a way to modify a single field's data values. 
% It does not write the header from scratch; it just copies it over from 
% the old file. Thus, the field name, and field datatypes are not allowed 
% to be modified.
%
% fname_orig: the filename of the extracted 
% fname_new:  a string containing the name of the binary file to write
% field_new: a 'field' struct (as returned by) readTrodesExtractedDataFile, 
%    with modified data
%
%  field_new.name -- the name of the data (must match a field in original)
%  field_new.type -- the data type
%  field_new.data -- the data, an m by n vector (usually n=1)
%
%  To adjust timestamps by an offset, call this function like this:
%
%  fname_orig = 'timestamps.dat';
%  fname_new = 'timestamps.adj.dat';
%  dat = readTrodesExtractedDataFile(fname_orig);
%  field_tt = dat.fields(strcmp({dat.fields.name},'trodestime'));
%  field_tt.data = field_tt.data + offset;
%  writeUpdatedTrodesExtractedDataFile(fname_orig, fname_new, field_tt)
%
%  NB this code is not very memory-efficient, but the whole process of 
%  adjusting timestamps is an ugly hack to begin with! :)
%
%  Tom Davidson, July 2019 - tjd@alum.mit.edu

%  TODO:
%   -use new readtrodesexportfile (just parse header?)

debug = false;

if (~isempty(varargin))
    assign(varargin{:});
end

if numel(field_new) > 1
  error('Can only update one field at a time');
end

% use a temporary name in case updating fails
fname_tmp = [filename_new, '_temp'];

% Read in original file to parse header data (we don't need the actual data
% because we are going to copy the entire file, but this is the only helper
% function available).
dat_orig = readTrodesExtractedDataFile(filename_orig);
nfields_orig = size(dat_orig.fields,2);

% get index of field to update
update_fidx = find(strcmp(field_new.name, {dat_orig.fields.name}));
if isempty(update_fidx)
  error(sprintf('Field ''%s'' not found in original file, can''t update.', field_new.name));
end

% check to make sure data type and size have not changed
assert(isequal(dat_orig.fields(update_fidx).type, field_new.type), 'field_new type has changed from original.')
assert(isequal(dat_orig.fields(update_fidx).columns, field_new.columns), 'field_new columns has changed from original.')
assert(isequal(dat_orig.fields(update_fidx).bytesPerItem, field_new.bytesPerItem), 'field_new bytesPerItem has changed from original.')
assert(isequal(size(dat_orig.fields(update_fidx).data), size(field_new.data)), 'field_new data size has changed from original file');

% free up memory, we don't need the data anymore
for fi = 1:nfields_orig
  dat_orig.fields(fi).data = [];
end

% Copy entire file (we'll modify the changed data in place)
[success, message] = copyfile(filename_orig, fname_tmp, 'f');
if ~success
  error(message);
end

fid = fopen(fname_tmp, 'rb+');

% get header size
nlines = 0;
while 1
  hline = fgetl(fid);
  nlines = nlines+1;
  if strcmp(hline, '<End settings>')
    headersize = ftell(fid);
    break
  end
  if hline(1) == -1 || isempty(hline) || nlines > 100
    error('Did not find end of header.');
  end
end

% get field and packet sizes
field_szs = [dat_orig.fields.bytesPerItem] .* [dat_orig.fields.columns];
bytesPerPacket = sum(field_szs);
byteOffsets = cumsum([0 field_szs(1:end-1)]);

% Now modify just the data for the field to be updated

% Here's the 'fread' command used by readTrodesExtractedDataFile:
% tmpData = fread(fid,[fields(i).columns,inf],[num2str(fields(i).columns),'*', fields(i).type,'=>',fields(i).type],skipBytes);

% But 'fwrite' does not support writing repeated entries before skipping, 
% so we'll loop, calling fwrite 1x per column.
for ci = 1:field_new.columns
  col_offset = (ci-1)*field_new.bytesPerItem;
  skipBytes = bytesPerPacket-field_new.bytesPerItem; % since we're writing one column at a time
  fseek(fid, headersize + byteOffsets(update_fidx) + col_offset, 'bof');

  % NB: Fwrite skips skipBytes *before* writing the first entry, so we need 
  % to write the first entry without skipping, then call fwrite again with 
  % skipping for the rest of the data. (If we instead compensated using an
  % offset, we could end up running off the beginning of the file.
  fwrite(fid, field_new.data(1,ci), field_new.type);
  wcount(ci) = fwrite(fid, field_new.data(2:end,ci), field_new.type, skipBytes);
end
if debug, disp(wcount+1), end

fclose(fid);

movefile(fname_tmp, filename_new)
