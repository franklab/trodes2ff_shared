function createThetaFilteredLFP(directoryname,fileprefix,days, varargin)
%function thetadayprocess(rawdirectory,daydirectory,fileprefix,days, varargin)
%
%Applies a theta filter to all epochs for each day and saves the data in
%in the EEG subdirectory of the directoryname folder.
%
%daydirectory - example '/data99/user/animaldatafolder/', a folder
%            containing processed matlab data for the animal
%
%fileprefix -   animal specific prefix for each datafile (e.g. 'fre')
%
%days -         a vector of experiment day numbers
%
%options -
%     'system', 1 or 2
%        specifies old (1) or new/nspike (2) rigs. Default 2.
%
%     'daytetlist', [day tet ; day tet ...]
%        specifies, for each day, the tetrodes for which theta
%        extraction should be done
%
%     'downsample', factor
%        saves only every factor points (eg. 1:factor:end).
%                 Default 10
%
%     'f', matfilename
%        specifies the name of the mat file containing the
%        theta filter to use
%        (default /usr/local/filtering/thetafilter.mat).
%        Note that the filter must be called 'thetafilter'.

daytetlist = [];
f = '';
defaultfilter = 'thetadayprocess_filter.mat';
if ~isempty(varargin)
    assign(varargin{:})
end
% [otherArgs] = procOptions(varargin);

% if the filter was not specified, load the default
if isempty(f)
    eval(['load ', defaultfilter]);
    thetafilter.kernel = interp(thetafilter.kernel,10); %upsample to 1500Hz for consistency with other filters DR 
    thetafilter.samprate = thetafilter.samprate*10; %upsample to 1500Hz  for consistency with other filters DR
else
    eval(['load ', f]);
end
minint = -32768;

days = days(:)';

for day = days
    % create the list of files for this day that we should filter
    if (isempty(daytetlist))
        tmpflist = dir(sprintf('%s/EEG/*eeg%02d-*.mat', directoryname, day));
        flist = cell(size(tmpflist));
        for i = 1:length(tmpflist)
            flist{i} = sprintf('%s/EEG/%s', directoryname, tmpflist(i).name);
        end
    else
        % find the rows associated with this day
        flist = {};
        ind = 1;
        tet = daytetlist(find(daytetlist(:,1) == day),2);
        for t = 1:length(tet);
            currtet = tet(t);
            tmpflist = dir(sprintf('%s/EEG/*eeg%02d-*-%02d.mat', ...
                directoryname, day, currtet));
            nfiles = length(tmpflist);
            for i = 1:length(tmpflist)
                flist{ind} = sprintf('%s/EEG/%s', directoryname, tmpflist(i).name);
                ind = ind +1;
            end
        end
    end
    
    % go through each file in flist and filter it
    for fnum = 1:length(flist) % (by tetrode)
        % get the tetrode number and epoch
        % this is ugly, but it works
        dash = find(flist{fnum} == '-');
        epoch = str2num(flist{fnum}((dash(1)+1):(dash(2)-1)));
        tet = str2num(flist{fnum}((dash(2)+1):(dash(2)+3)));
        %load the eeg file
        load(flist{fnum});
        a = find(isnan(eeg{day}{epoch}{tet}.data));
        samprate = eeg{day}{epoch}{tet}.samprate;
        
        [lo,hi]= findcontiguous(a);  %find contiguous NaNs
        for i = 1:length(lo)
            disp('NaN period detected')
            if lo(i) > 1 & hi(i) < length(eeg{day}{epoch}{tet}.data)
                fill = linspace(eeg{day}{epoch}{tet}.data(lo(i)-1), ...
                    eeg{day}{epoch}{tet}.data(hi(i)+1), hi(i)-lo(i)+1);
                eeg{day}{epoch}{tet}.data(lo(i):hi(i)) = fill;
            end
        end
        
        %Trodes LFP extracted as int16.. change to double here bc filtfilt only supports double
        eeg{day}{epoch}{tet}.data = double(eeg{day}{epoch}{tet}.data); %this may also be happening inside some versions of filtereeg2
        disp(sprintf('Theta (6-9Hz) filtering day-%d ep-%d tet-%d',day, epoch,tet))
        theta{day}{epoch}{tet} = DR_filtereeg2(eeg{day}{epoch}{tet}, thetafilter, 'int16', 1); %changed from filtereeg2 to DR_filtereeg2 CW 12/2/19
        % replace the filtered invalid entries with nan
        for i = 1:length(lo)
            if lo(i) > 1 & hi(i) < length(theta{day}{epoch}{tet}.data)
                theta{day}{epoch}{tet}.data(lo(i):hi(i)) = nan;
            end
        end
        % save the resulting file
        thetafile = sprintf('%s/EEG/%stheta%02d-%d-%02d.mat', directoryname, fileprefix, day, epoch, tet);
        save(thetafile, 'theta');
        clear theta
    end % tetrode
    fprintf('.');
end
