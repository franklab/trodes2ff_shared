function createcellinfostruct(animdirect,fileprefix,append)
% createcellinfostruct(animdirect,fileprefix)
% createcellinfostruct(animdirect,fileprefix,append)
%
% This function creates a cellinfo file in the animal's data directory.
% For each cell, the spikewidth, mean rate, and number of spikes is saved.  If a cellinfo file
% exists and new data is being added, set APPEND to 1.

cellinfo = [];
if (nargin < 3)
    append = 0;
end
if (append)
    try
        load(fullfile(animdirect,[fileprefix,'cellinfo']));
    end
end
spikefiles = dir(fullfile(animdirect,[fileprefix,'spikes*']));
for i = 1:length(spikefiles)
    load(fullfile(animdirect, spikefiles(i).name));
    allinfo = cellfetch(spikes, '', 'alltags', 1);
    idx = allinfo.index;
    for j = 1:size(allinfo.index,1)
        info = allinfo.values{j,1};
        cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.depth = [];
        cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.label = info.label;
        cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.timerange = info.timerange;
        cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.spikewidth = info.spikewidth;
        
        if ~ismember('numspikes', fieldnames(info))
            numspikes = size(info.data,1);
            cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.numspikes = numspikes;
        else
            cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.numspikes = [];
        end
        if ~ismember('meanrate', fieldnames(allinfo.values{1}))
            epochtime = diff(info.timerange)/10000;
            cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.meanrate = ...
                cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.numspikes/epochtime;
        else
            cellinfo{idx(j,1)}{idx(j,2)}{idx(j,3)}{idx(j,4)}.meanrate = [];
        end
    end
end

save(fullfile(animdirect,[fileprefix,'cellinfo']),'cellinfo');
