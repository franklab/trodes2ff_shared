function FF_LFPreferencer(animalID, rawdaydir,FFanimdir, sessionNum, varargin)

eegdir = [FFanimdir 'EEG/'];
fprintf('lfpreferencer day %d\n',sessionNum);
% AKG, DR 2016-17
% uses lfp ref chan listed in eeggnd file or config file, if provided

%default is to use config to identify ref chan
useconfig = 1;
if (~isempty(varargin))
    assign(varargin{:});
end
confRefIDs = [];
confnTrodeIDs = [];
if useconfig
    try   %there is probably a trodesconf file in each raw day folder
        configfile = sprintf('%s%s.trodesconf',rawdaydir,rawdaydir(end-8:end-1));
        config = readTrodesFileConfig(configfile);
    catch % otherwise, there should be one in the parent raw dir
        configfile = dir(sprintf('%s*.trodesconf',rawdaydir));
        config = readTrodesFileConfig([rawdaydir configfile(1).name]);
    end
    if isempty(config)
        error('no config file found')
    end
    confnTrodeIDs = cell2mat(arrayfun(@(x) str2num(x.id),config.nTrodes,'UniformOutput', false));
    if isfield(config.nTrodes,'refNTrodeID')  %newer versions of trodes have the refID listed correctly
        confRefIDs = cell2mat(arrayfun(@(x) str2num(x.refNTrodeID),config.nTrodes,'UniformOutput', false));
    else  %old trodes versions require getting the IDs of the references from the indices listed as 'refNTrode'    
        confRefIndices = cell2mat(arrayfun(@(x) str2num(x.refNTrode),config.nTrodes,'UniformOutput', false));
        confRefIDs = confnTrodeIDs(confRefIndices); 
    end
else
    load([FFanimdir, animalID, 'tetinfo']);
end

cd(eegdir)

eeggndfiles = dir(sprintf('*eeggnd%02d*',sessionNum));
fulleeg = {};
for lfpfileNum = 1:length(eeggndfiles)
    eeg = {};
    eeggnd = load(eeggndfiles(lfpfileNum).name);
    eeggnd = eeggnd.eeggnd;
    dashes = strfind(eeggndfiles(1).name,'-');
    fileparts = strsplit(eeggndfiles(lfpfileNum).name, '-');
    ep = str2num(fileparts{2});
    tet = str2num(fileparts{3}(1:2));
    fprintf('referencing %seeg%02d-%d-%02d.mat\n',animalID,sessionNum,ep,tet)
    %do the actual subtraction of all non-nan values... i.e. retain nan's
    eeg{sessionNum}{ep}{tet}.data = nan(length(eeggnd{sessionNum}{ep}{tet}.data),1);
    inds2use = find(~isnan(eeggnd{sessionNum}{ep}{tet}.data));
    
    %identify ref chan
    reftetID = []; 
    if useconfig
        reftetID = confRefIDs(find(confnTrodeIDs==tet,1));
    else
        try
        reftetID = tetinfo{sessionNum}{ep}{tet}.ref;
        catch
            disp(sprintf('no valid reference tag found for %s d%d e%d nt%d', animalID, sessionNum, ep, tet));
        end
        %         reftetID = eeggnd{sessionNum}{ep}{tet}.refNTrode; %depricated
    end
    if ~isempty(reftetID)
        refeeg = load(sprintf('%seeggnd%02d-%d-%02d.mat',animalID,sessionNum,ep,reftetID));
        eeg{sessionNum}{ep}{tet}.data(inds2use) = ...
            eeggnd{sessionNum}{ep}{tet}.data(inds2use)-...
            refeeg.eeggnd{sessionNum}{ep}{reftetID}.data(inds2use);
        eeg{sessionNum}{ep}{tet}.referenced = 1;
    else
        disp('leaving unreferenced')
        eeg{sessionNum}{ep}{tet}.data(inds2use) = eeggnd{sessionNum}{ep}{tet}.data(inds2use);
        eeg{sessionNum}{ep}{tet}.referenced = 0;
    end
    
    % copy everything else over from eeggnd
    eeg{sessionNum}{ep}{tet}.descript = eeggnd{sessionNum}{ep}{tet}.descript;
    eeg{sessionNum}{ep}{tet}.samprate = eeggnd{sessionNum}{ep}{tet}.samprate; %mattias will implement this as an export arg in the future
    eeg{sessionNum}{ep}{tet}.clockrate = eeggnd{sessionNum}{ep}{tet}.clockrate;
    eeg{sessionNum}{ep}{tet}.starttime = eeggnd{sessionNum}{ep}{tet}.starttime;
    eeg{sessionNum}{ep}{tet}.endtime = eeggnd{sessionNum}{ep}{tet}.endtime;
    eeg{sessionNum}{ep}{tet}.data_voltage_scaled = eeggnd{sessionNum}{ep}{tet}.data_voltage_scaled;
    eeg{sessionNum}{ep}{tet}.nTrode = eeggnd{sessionNum}{ep}{tet}.nTrode;
    eeg{sessionNum}{ep}{tet}.nTrodeChannel = eeggnd{sessionNum}{ep}{tet}.nTrodeChannel;  %1-based
    eeg{sessionNum}{ep}{tet}.low_pass_filter = eeggnd{sessionNum}{ep}{tet}.low_pass_filter;
    eeg{sessionNum}{ep}{tet}.voltage_scaling = eeggnd{sessionNum}{ep}{tet}.voltage_scaling;
    % update referencing status
    eeg{sessionNum}{ep}{tet}.refnTrode = reftetID;
    fulleeg{lfpfileNum} = eeg;
end
tic
fprintf('saving referenced lfp ');
for lfpfileNum = 1:length(eeggndfiles)
    fileparts = strsplit(eeggndfiles(lfpfileNum).name, '-');
    ep = str2num(fileparts{2});
    tet = str2num(fileparts{3}(1:2));
    eeg = fulleeg{lfpfileNum};
    save(sprintf('%s%seeg%02d-%d-%02d.mat',eegdir,animalID,sessionNum,ep,tet),'-v6','eeg');
end
fprintf('took %.03f seconds\n', toc);

