%% TEMPLATE
% **** NEW dayprocess script to adapt to modernized preprocessing pipeline 2/2020 **** 

%% -------------Animal Info-------------------
animal = 'bernard'; 

dates = [20181022, 20181023, 20181024, 20181025, 20181026, 20181027, 20181028, 20181029, 20181030, 20181031, ...
         20181101, 20181102, 20181103, 20181104, 20181105, 20181106, 20181107, 20181108, 20181109, 20181110, ...
         20181111, 20181112, 20181113, 20181114, 20181115, 20181116, 20181117, 20181118, 20181119, ...
         20181120, 20181121, 20181122,20181123, 20181124, 20181125]; %
%sessionNums = 4;
sessionNums = [1,2,3,4,5,6,7,8,9,10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,  33, 34, 35]; %
 
sleepcmperpix = [.055];
runcmperpix = [0.2];

nTrodeareas = []; 
refNTrodes = []; 
nTrodeTags = [...
{[{'suparea'}, {'can1'},{'ntrodes'}, {[1:5,7:15]}]};             
{[{'suparea'}, {'cc'}, {'area'}, {'can1ref'}, {'ntrodes'}, {[6]},{'days'},{[1:35]}]};
{[{'suparea'}, {'can2'}, {'ntrodes'}, {[16:29]}]}; 
{[{'suparea'}, {'cc'}, {'area'}, {'can2ref'}, {'ntrodes'}, {[30]}]};
{[{'area'}, {'ca1'}, {'ntrodes'}, {[7,8,14,19,29]}, {'days'}, {[2:7]}]};   
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1,7,8,9,14,19,22,26,29]},{'days'},{[8]}]};     
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1:5,7:9,13:20,22:29]},{'days'},{[9]}]};     
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1:5,7:11,13:29]},{'days'},{[10:14,18]}]};                    
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1:5,7,10:11,14:29]},{'days'},{[15:17]}]};                    
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1,2,4,5,7:9,11,13:16,18:25,27:29]},{'days'},{[19:20]}]};  
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1,2,4,5,7:11,13:16,18:25,27:29]},{'days'},{[21]}]};  
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1:5,7,8,10,11,13:29]},{'days'},{[22:24]}]};  
{[{'area'}, {'ca1'}, {'ntrodes'}, {[1:5,7:11,13:29]},{'days'},{[25:35]}]};  
{[{'area'}, {'dead'}, {'ntrodes'}, {[12,31,32]}]};

{[{'area'}, {'bad_ca1'}, {'ntrodes'}, {[29]}]}; 
{[{'area'}, {'bad_ca1'}, {'ntrodes'}, {[28]},{'days'},{[9:35]}]}; 

{[{'deadchans'},{[]}, {'ntrodes'},{[1:5,8:11,13,15,17,19:29]}]};  %full tets, all days & at least some days
{[{'deadchans'},{[4]}, {'ntrodes'},{[7,14]}]};  %tritrodes all days
{[{'deadchans'},{[4]}, {'ntrodes'},{[10,16]},{'days'},{[25:35]}]};  
{[{'deadchans'},{[3]}, {'ntrodes'},{[16]},{'days'},{[22:24]}]};  
{[{'deadchans'},{[4]}, {'ntrodes'},{[18]},{'days'},{[22:35]}]};  
{[{'deadchans'},{[2]}, {'ntrodes'},{[18]},{'days'},{[13:21]}]};  
{[{'deadchans'},{[3,4]}, {'ntrodes'},{[6]}]};
];

%List out the epoch types ordered for each day
epochtypesorderedCombined{1} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{2} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{3} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{4} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{5} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{6} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{7} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{8} = {'sleep', 'run', 'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{9} = {'sleep', 'run', 'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{10} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{11} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{12} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{13} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{14} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{15} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{16} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{17} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{18} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{19} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{20} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{21} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{22} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{23} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{24} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{25} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{26} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{27} = {'sleep', 'run','sleep', 'run', 'sleep'};
epochtypesorderedCombined{28} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{29} = {'sleep', 'run', 'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{30} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{31} = {'sleep', 'run', 'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{32} = {'sleep', 'run', 'sleep'};
epochtypesorderedCombined{33} = {'sleep', 'run', 'sleep', 'run', 'sleep'};
epochtypesorderedCombined{34} = {'sleep', 'run', 'run',  'sleep', 'run', 'sleep'};
epochtypesorderedCombined{35} = {'sleep', 'run', 'sleep', 'run', 'sleep'};

% for providing additional tags to populate taskinfo 
 epochTags = []; 
%     {[{'days'}, {1:8}, {'testfield'}, {'testentry'}]}; 
%     ];


%add date,session, and ep to task struct-------------------------
% sessionNums = [1,2,3,4,5,6,7,8,9,10, 11, 12, 13, 14, 15];
% dates = [20190303, 20190304, 20190305, 20190306, 20190307, 20190308, 20190309, ...
%         20190310, 20190311, 20190312, 20190313, 20190314, 20190315, 20190316, 20190317];
% for s = 1:length(sessionNums)
%     load(sprintf('despereauxtask%02d.mat',sessionNums(s)))
%     for e = 1:length(task{sessionNums(s)})
%         task{sessionNums(s)}{e}.session = sessionNums(s);
%         task{sessionNums(s)}{e}.epoch = e;
%         task{sessionNums(s)}{e}.date = dates(s);
%     end
%     save(sprintf('despereauxtask%02d.mat',sessionNums(s)),'task')
% end
% % % 
% % % add delay field to taskinfo run sessions 
% destdir='/mnt/stelmo/anna/despereaux/filterframework/'; animal='despereaux';
% task = loaddatastruct(destdir,animal,'task');
% eps = evaluatefilter(task,'isequal($type,''run'')'); % 
% for d = 1:length(unique(eps(:,1)))
%     e = eps(eps(:,1)==d,:); 
%     AG_addtaskinfo(destdir,animal,d,e(:,2)','t22delay',50);
% end

% fix cluster tags to make .unittype = single or multi------------------------------------
% load('/media/anna/data45/nico/filterframework/nicocellinfo.mat');
% sessionNums = [20];
% for s = sessionNums
%     for e = 1:length(cellinfo{s})
%         for t = 1:length(cellinfo{s}{e})
%             for c = 1:length(cellinfo{s}{e}{t})
%                 if any(strcmp(cellinfo{s}{e}{t}{c}.tags,'mua'))
%                     cellinfo{s}{e}{t}{c}.unittype = 'mua';
%                 else
%                     cellinfo{s}{e}{t}{c}.unittype = 'single';
%                 end
%             end
%         end
%     end
% end
% save('/media/anna/data45/nico/filterframework/nicocellinfo.mat','cellinfo');

%-------------Params------------------
adjusttimestamps = 0; % step 1 ;  %Run this once before processing anything else..
makeFFLFP = 0; % step 2
makereferencedFFLFP = 0 ; useconfig = 1;  %NOTE: if your refs don't change across days, just use the refNtrodes attribute and set this to zero. DR: for D13.. set useconfig = 0.. 
makeFFDIO = 0; % step 4;
makeFFPOS = 0; fit_offset_override = -.0352; % step 5;
makeRippleFilteredLFP = 0; % step 6
addlinpostotask = 0; % step 8. add linpos landmarks to task struct; **NOTE: currently OVERWRITES rather than updating task
updateTaskinfo = 0; % step 7. saves epochtypesorderedcombined info into task struct
updateTetinfo = 0; 
updateCellinfo = 0;  % do not do this unless you have cluster tags to add!
rmDeadMDAchans = 0;  % important for sorting; tetinfo must be updated with dead chan info
makeFFLINPOS = 0; % step 10
extractRipplesKonsensus = 1 ; maxpeakval = 700;% step 11; new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m
extractRipplesJY = 0; epfilter = 'isequal($environment,''sleep'')';  %tell which ep type to use for thresh calculation and subsequent rips detection
makeFFspikes = 0; sorter = 'ms4'; metricperepoch = 1; nTrodestoSort = []; % [] to import all, or can pass in filterstring for tetinfo like 'isequal($area, ''ca1'')'. additional args listed in spikes function
makeFFmarks = 0; marknTrodes = []; marksource = 'trodes'; % 'mountainsort' vs 'trodes'
extractRipples = 0; %old method ripple detection. DONT RUN THIS
extractMUAripples = 0; muasource = 'marks'; muanstd = 3; % could also easily adapt to use spikes as source

% ___________________General Params (probably won't need to change these)___________________
% posSource = 'manual1';
posSource = 'online';
taskcoordinateprogram = [ {'wtrack'} {'getcoord_wtrack'}]; %'wtrack'} ; {'sixarmtrack'} {'getcoord_6armtrack'}]; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
overwriteTrackNodes = 0; % set this to 1 if you want to overwrite the user specified nodes of the wtrack.. i.e. reruns createtaskstruct 
RIPmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
RIPnstd = 2; % > # std ripples (start with 2)
OLDRIPtetrodes = []; % -1 to specify all tetrodes. eventually grab this from tetinfo struct based on area
consensusRIParea = {'ca1'}; %define source regions of ripples.. saved structure will contain this string
useNearestNeighbor = 0; %NN method current DR default.. AG set this to 0 for your data

% ___________________ Debugging ___________________
checkforcorrupteddata = 0;
checknumCommentfiles = 0; %not necessary anymore.. we aren't using the coments files


%% run Trodes_dayprocess with the vars above.. loop over dates/sessionNums
tic
if length(sessionNums) ~= length(dates); error('length of sessionNums must equal length of dates'); end
for i = 1:length(sessionNums);
    date = dates(i);
    sessionNum = sessionNums(i);
    epochtypesordered = epochtypesorderedCombined{sessionNum};
    %cmperpix = repmat([sleepcmperpix runcmperpix],1,ceil(length(epochtypesordered)/2));
    %cmperpix = cmperpix(1:length(epochtypesordered));
    cmperpix(find(strcmp(epochtypesorderedCombined{sessionNum},'sleep'))) = sleepcmperpix;
    cmperpix(find(~strcmp(epochtypesorderedCombined{sessionNum},'sleep'))) = runcmperpix;

 %run dayprocess
    Trodes_dayprocess(animal, date, sessionNum, 'adjusttimestamps',adjusttimestamps, 'makeFFLFP', makeFFLFP, ...
        'rmDeadMDAchans',rmDeadMDAchans, 'makereferencedFFLFP',makereferencedFFLFP, 'useconfig', useconfig, ...
        'makeFFDIO', makeFFDIO, 'makeFFPOS', makeFFPOS,  'fit_offset_override',fit_offset_override, ...
        'makeFFLINPOS', makeFFLINPOS, 'makeFFspikes', makeFFspikes, 'addlinpostotask',addlinpostotask, ...
        'updateTaskinfo', updateTaskinfo, 'updateTetInfo',updateTetinfo,'updateCellinfo',updateCellinfo, ...
        'makeRippleFilteredLFP', makeRippleFilteredLFP, 'extractRipples', extractRipples, ...
        'extractRipplesKonsensus', extractRipplesKonsensus, 'maxpeakval',maxpeakval,'extractRipplesJY',extractRipplesJY, ...
        'epfilter',epfilter, 'sorter',sorter, 'nTrodestoSort', nTrodestoSort, 'metricsperepoch',metricsperepoch', ...
        'makeFFmarks',makeFFmarks, ...
        'marknTrodes',marknTrodes, 'marksource',marksource,'cmperpix', cmperpix, 'posSource', posSource, ...
        'taskcoordinateprogram', taskcoordinateprogram, ...
        'nTrodeareas', nTrodeareas, 'nTrodeTags', nTrodeTags, ...
        'RIPmindur', RIPmindur, 'RIPnstd', RIPnstd, 'OLDRIPtetrodes', OLDRIPtetrodes, ...
        'consensusRIParea', consensusRIParea, 'checkforcorrupteddata', checkforcorrupteddata, ...
        'checknumCommentfiles', checknumCommentfiles, 'useNearestNeighbor',useNearestNeighbor, ...
        'epochtypesordered', epochtypesordered, 'overwriteTrackNodes', overwriteTrackNodes,...
        'refNTrodes', refNTrodes,'extractMUAripples',extractMUAripples,'muasource',muasource,'muanstd',muanstd, 'epochTags',epochTags);
end

timerTrodes_dayprocess = toc;
disp(sprintf('nextcoffeebreak = %0.0f seconds',timerTrodes_dayprocess));

