
function epochs = get_epochs_from_foldernames(sourcedir)
% return array of epochs scraped from a sourcedir foldernames
% this strictly follows the standard FLab fname convention
epochid = 3;

fs = dir(sourcedir);
files = arrayfun(@(x) regexp(x.name, ...
    '\d{8}_[a-zA-Z0-9]{3}_\d{2}\.mda', 'match'),...
    fs, 'un', 0);
files = files(~cellfun('isempty', files));

epochs  = cellfun(@(x) str2num(x{epochid}), ...
    cellfun(@(x) regexp(x, '[_.]', 'split'),...
    files));
end
