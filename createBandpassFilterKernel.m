function createBandpassFilterKernel(freqbandname, outputfile, varargin)

%DR March 2017
%Here is where the settings for all filters are specified
%freqbandname is a string and will be used to find a default freq range if
%not specified, and to name the output .mat wavelet

%options: samprate, freqband

%% set params
samprate = [];
switch freqbandname %defaults
    case 'ripple'
        freqband = [140 250]; 
    case 'theta'
        freqband = [5 11];  
    case 'slowgamma'
        freqband = [20 50];  
    case 'lowgamma'
        freqband = [20 50];  
    case 'highgamma'
        freqband = [65 90]; 
    case 'fastgamma'
        freqband = [65 140]; 
end
if (~isempty(varargin))
    assign(varargin{:});
end
if isempty(freqband) || length(freqband) ~= 2
    error('bad frequency range')
end
if isempty(samprate)
    samprate = 1500; %default
end
freqfiltname = sprintf('%sfilter_%d_%d_%d',freqbandname, freqband(1), freqband(2), samprate);
filterfile = sprintf('%s/%s', outputfile, freqfiltname);
disp(sprintf('creating %s', freqfiltname)); 

%% create filter
ifilt = designeegfilt(samprate,freqband(1),freqband(2));
eval([sprintf('%s.kernel = ifilt;',freqfiltname)])
eval([sprintf('%s.samprate = samprate;',freqfiltname)])
eval([sprintf('%s.freqband = freqband;',freqfiltname)])

%% save
filterfile = [filterfile '.mat'];
eval(['save(' sprintf('''%s'',''%s''',filterfile,freqfiltname) ')']);

end