function createNoiseFilteredLFP(directoryname,fileprefix,days, varargin)
%function noisedayprocess(rawdirectory,daydirectory,fileprefix,days, varargin)
%
%Applies a noise filter to all epochs for each day and saves the data in
%in the EEG subdirectory of the directoryname folder.
%
%daydirectory - example '/data99/user/animaldatafolder/', a folder
%            containing processed matlab data for the animal
%
%fileprefix -   animal specific prefix for each datafile (e.g. 'fre')
%
%days -         a vector of experiment day numbers
%
%options -
%     'system', 1 or 2
%        specifies old (1) or new/nspike (2) rigs. Default 2.
%
%     'daytetlist', [day tet ; day tet ...]
%        specifies, for each day, the tetrodes for which noise
%        extraction should be done
%
%     'downsample', factor
%        saves only every factor points (eg. 1:factor:end).
%                 Default 10
%
%     'f', matfilename
%        specifies the name of the mat file containing the
%        noise filter to use
%        Note that the filter must be called 'noisefilter'.

daytetlist = [];
f = '';
defaultfilter = 'noisefilter_500_600_1500.mat';

[otherArgs] = procOptions(varargin);

% if the filter was not specified, load the default
if isempty(f)
    try
    eval(['load ', defaultfilter]);
    catch
        disp(sprintf('%s not found, check out designeegfilt.m', defaultfilter))
        return
    end
    noisefilter.kernel = noisefilter_500_600_1500;
    noisefilter.samprate = 1500;
    noisefilter.descript = 'high frequency noise';
else
    eval(['load ', f]);
end
% minint = -32768;
days = days(:)';

for d = 1:length(days)
    day = days(d);
    % create the list of files for this day that we should filter
    if (isempty(daytetlist))
        tmpflist = dir(sprintf('%s/EEG/*eeg%02d-*.mat', directoryname, day));
        flist = cell(size(tmpflist));
        for i = 1:length(tmpflist)
            flist{i} = sprintf('%s/EEG/%s', directoryname, tmpflist(i).name);
        end
    else
        % find the rows associated with this day
        flist = {};
        ind = 1;
        tet = daytetlist(find(daytetlist(:,1) == day),2);
        for t = 1:length(tet);
            currtet = tet(t);
            tmpflist = dir(sprintf('%s/EEG/*eeg%02d-*-%02d.mat', ...
                directoryname, day, currtet));
            nfiles = length(tmpflist);
            for i = 1:length(tmpflist)
                flist{ind} = sprintf('%s/EEG/%s', directoryname, tmpflist(i).name);
                ind = ind +1;
            end
        end
    end
    
    fullnoise = {};
    parfor fnum = 1:length(flist) % (by tetrode)\
        noise = {};
        % get the tetrode number and epoch
        % this is ugly, but it works
        dash = find(flist{fnum} == '-');
        epoch = str2num(flist{fnum}((dash(1)+1):(dash(2)-1)));
        tet = str2num(flist{fnum}((dash(2)+1):(dash(2)+3)));
        %load the eeg file
        eeg = load(flist{fnum});
        eeg = eeg.eeg;
        a = find(isnan(eeg{day}{epoch}{tet}.data));
        samprate = eeg{day}{epoch}{tet}.samprate;
        
        [lo,hi]= findcontiguous(a);  %find contiguous NaNs
        for i = 1:length(lo)
            disp('NaN period detected')
            if lo(i) > 1 && hi(i) < length(eeg{day}{epoch}{tet}.data)
                fill = linspace(eeg{day}{epoch}{tet}.data(lo(i)-1), ...
                    eeg{day}{epoch}{tet}.data(hi(i)+1), hi(i)-lo(i)+1);
                eeg{day}{epoch}{tet}.data(lo(i):hi(i)) = fill;
            end
        end
        
        %Trodes LFP extracted as int16.. change to double here bc filtfilt only supports double
        eeg{day}{epoch}{tet}.data = double(eeg{day}{epoch}{tet}.data); %this may also be happening inside some versions of filtereeg2
        fprintf('Noise (500-600Hz) filtering day-%d ep-%d tet-%d\n',day, epoch,tet)
        noise{day}{epoch}{tet} = filtereeg2(eeg{day}{epoch}{tet}, noisefilter, 'int16', 1);
        % replace the filtered invalid entries with nan
        for i = 1:length(lo)
            if lo(i) > 1 && hi(i) < length(noise{day}{epoch}{tet}.data)
                noise{day}{epoch}{tet}.data(lo(i):hi(i)) = nan;
            end
        end
        % save the resulting file
        noisefiles{fnum} = sprintf('%s/EEG/%snoise%02d-%d-%02d.mat', directoryname, fileprefix, day, epoch, tet);
        fullnoise{fnum} = noise;
%         save(noisefile, 'noise');
%         clear noise
    end
        % can't save within parfor
    for fnum = 1:length(flist)
        noise = fullnoise{fnum};
        save(noisefiles{fnum}, 'noise');
    end
end
