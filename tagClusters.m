function tagClusters(FFanimdir, animal, sessionNum, taginput, varargin)

%Demetris Roumis Feb 2017

% add or update tags on cell info struct
% tag input contains an array of cells, with each row  specifying any set
% of tagVariable, tagValue, ntrodes, days in any order.
% leave out or leave empty the days, epochs, ntrodes Tags to include ALL.

% cellTags = [...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]};...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]}];
inherit_tetTags = 1;
if ~isempty(varargin)
    assign(varargin{:});
end

cd(FFanimdir);
fprintf('updating cell info struct.sessionNum %d \n', sessionNum);

try 
    load([FFanimdir, animal, 'cellinfo']);
    if isempty(cellinfo{sessionNum})
        error('cellinfo exists but is empty for day %d \n', sessionNum)
    end
catch
    fprintf('cellinfo structure does not exist or is empty for day %d \n', sessionNum);
    fprintf('..running createcellinfostruct')
    if exist('cellinfo', 'var')
        createcellinfostruct(FFanimdir, animal, 1); % append
    else
        createcellinfostruct(FFanimdir, animal, 0);
    end
    load([FFanimdir, animal, 'cellinfo']);
end

cellInds = cellfetch(cellinfo,' ');
DENC = [{'days'},{'epochs'},{'ntrodes'},{'clusters'}];

if inherit_tetTags
    if ~isempty(cellInds.index)
        DETC = cellInds.index; % all cells
        andef = animaldef(animal);
        for i =1:size(DETC,1)
            tetinfo = loaddatastruct(andef{2}, animal, 'tetinfo', DETC(i,1));
            ntinfo = tetinfo{DETC(i,1)}{DETC(i,2)}{DETC(i,3)};
            % can you append to a struct like this?
            cellinfo{DETC(i,1)}{DETC(i,2)}{DETC(i,3)}{DETC(i,4)} = ...
                [cellinfo{DETC(i,1)}{DETC(i,2)}{DETC(i,3)}{DETC(i,4)} ntinfo];
        end
    end
end

for rowtag = 1:length(taginput(:,1))
    days = []; epochs = []; ntrodes = [];
    rowtaginput = taginput{rowtag}(1,:); %get current rowset of tags
    vars = {rowtaginput{1:2:end}};
    vals = {rowtaginput{2:2:end}};
    DENCvarsInds= ismember(vars, DENC); %search for Day Epoch Ntrodes Clusrter vars
    DENCvars = vars(DENCvarsInds);
    DENCvals = vals(DENCvarsInds);
    tagvars = vars(~DENCvarsInds); %collect tags (excluding day,epoch,ntrodes,clusters)
    tagvals = vals(~DENCvarsInds);
    for i = 1:length(DENCvars)
        %assign in day epoch ntrode vars to workspace
        eval(sprintf('%s=%s;',DENCvars{i},mat2str(DENCvals{i})));
    end
    
    %set day,epoch,ntrode filters to ALL if empty
    if isempty(days)
        days = unique(cellInds.index(:,1));
    end
    if isempty(epochs)
        epochs = unique(cellInds.index(:,2));
    end
    if isempty(ntrodes)
        ntrodes = unique(cellInds.index(:,3));
    end
    
    %filter filterdays, sessionNum, then epochs, then ntrodes.
    Tdays = cellInds.index(ismember(cellInds.index(:,1),sessionNum),:);
    Tdays = Tdays(ismember(Tdays(:,1),days),:);
    if isempty(Tdays) %if this tag set excludes the current sessionNum (day)
        continue %move on to the next tag set. this is kinda wonky
    end
    Tepochs = Tdays(ismember(Tdays(:,2),epochs),:);
    Tntrodes = Tepochs(ismember(Tepochs(:,3),ntrodes),:);    
    
    
    
    if ~isempty(cellInds.index)
        Tc = cellInds.index(ismember(cellInds.index(:,1:3),...
            Tntrodes, 'rows'),:);
        for i =1:size(Tc,1)
            for tagn = 1:length(tagvars) %add tag var and val to this day,ep,nt,clust
                eval(sprintf('cellinfo{Tc(i,1)}{Tc(i,2)}{Tc(i,3)}{Tc(i,4)}.%s = %s;', ...
                    tagvars{tagn}, mat2str(tagvals{tagn})));
            end
        end
    end
end
save([FFanimdir, animal,'cellinfo'], 'cellinfo')
end
