function createtaskinfostruct(FFanimdir,animID,varargin)
fprintf('createtaskinfostruct\n')
% This function creates a taskinfo file in the animal's data directory.
append = 1;
if ~isempty(varargin)
    assign(varargin{:})
end
taskinfo = {};
if append
    try
        taskinfo = loaddatastruct(FFanimdir, animID, 'task');
    catch
    end
end
datafiles = dir([FFanimdir, animID, 'pos*']);
for i = 1:length(datafiles)
    load([FFanimdir, datafiles(i).name]);
    allinfo = cellfetch(pos, '', 'alltags', 1);
    idx = allinfo.index;
    for j = 1:size(allinfo.index,1)
        info = allinfo.values{j,1};
        taskinfo{idx(j,1)}{idx(j,2)}.cmperpixel = info.cmperpixel;
    end
end
% % for each day, save out a task info for legacy compatibility
% fulltaskinfo = taskinfo;
% for iday = 1:length(fulltaskinfo)
%     taskinfo = cell(1,length(fulltaskinfo));
%     taskinfo{iday} = fulltaskinfo{iday};
save(sprintf('%s/%staskinfo.mat', FFanimdir, animID), 'taskinfo')
% end