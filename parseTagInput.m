function [variables, expression] = parseTagInput(taginput)

pattern = '\$[\w\d_.]+([^\w\d_]|$)';
variables = regexp(taginput,pattern,'match');
if isempty(variables)
    error(['Not a valid filter:   ',taginput]);
end
for i = 1:length(variables) %get rid of the '$'
    variables{i} = regexprep(variables{i},'[^\w\d_.]','');
end
% expression = regexprep(taginput,'\$','structVar.'); %replace all '$' with 'structVar.'
    
