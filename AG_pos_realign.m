function [newTimes, newTimesInds] = AG_pos_realign(srcdir, destdir, animalID, sessionNum, epochnum, epochname, videotimestamps)

%uses dio pulses and cam module frame numbers (if available) to redistribute pos timestamps 
% AKG July2016

disp(sprintf('----REALIGNING s%d ep%d--------------------',sessionNum, epochnum))
% 1. load camera hardware frame counts, if possible
cd(sprintf('%s%s',srcdir,epochname))
try 
    camlog = readTrodesExtractedDataFile(sprintf('%spos_cameraHWFrameCount.dat',epochname(1:end-3)));
    %unwrap (sometimes framecounts hit a ceiling and reset to 1)
    camlog.fields.data = double(camlog.fields.data);
    wrapz = diff(camlog.fields.data) < 0;    
    if any(wrapz)
    disp('unwrapping')
    wrapval = camlog.fields.data(find(diff(wrapz) < 0,1));
    if ~isempty(wrapval)
        cumcammult = cumsum([false; wrapz]);
        camlog = cumcammult*wrapval+camlog.fields.data;
    end
    end
catch
    disp('no CameraHWframecount file found')
    camlog = [];
end

% 2. load DIOs (from ff folder, not binaries)
cd(destdir)
try
    load(sprintf('%s%sDIO%02d.mat', destdir,animalID,sessionNum))
    for d = 1:length(DIO{sessionNum}{epochnum})
        %identify cam chan by rate; should be ~30hz on avg (btwn 28-32)
        % only look at times of state=1 (pulse onset)
        warning('off', 'MATLAB:mode:EmptyInput')
        if isExcluded(sum(DIO{sessionNum}{epochnum}{d}.values==1),[length(videotimestamps)-300, length(videotimestamps)+300]) %if there are aprox the right num events to span entire ep
            if isExcluded(1/mode(diff(DIO{sessionNum}{epochnum}{d}.times(DIO{sessionNum}{epochnum}{d}.values==1))),[28, 32]) 
                ratecheck(d) = 1;
            else
                ratecheck(d) = 0;
            end
        else
            ratecheck(d) = 0;
        end
    end
    if sum(ratecheck) == 0
        dios = [];
        disp('no camerasync chan found in dios')
        % add a check here to just use the max if rate doesn't work
    elseif sum(ratecheck) > 1
        disp(sprintf('multiple potential camerasync chans found. using ch %d',find(ratecheck>0,1)))
        % only use pulse onset times (value = 1)
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.values==1);
    else 
        %only use pulse onset times (value =1 )
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0)}.values==1);
        disp(sprintf('using camerasync chan %d for timestamp realigmnent',find(ratecheck>0)))
    end        
catch
    dios = [];
    disp('no dio files found for this session')
end

% Realignment Strategy
% uses .5 sec gap in frame acquisition at recording start as landmark to align first pulse with correct frame.
% if camlogs available, use them to remove dios corresponding to dropped frames and realign based on order starting after the gap
% if camlogs unavailable, use LAST 1000 timestamps/dios to calculate a standard delay. if delay-matched dio found, 
%   reassign timestamp. Deal with remaining laggy times by redistributing.

if isempty(dios)
    disp('no dios: just correct for videotimestamp pileups and dropped frames, if possible.. not written yet. exiting with original timestamps')
    %check for repeat and remove
    if any(diff(videotimestamps)==0)
        repeats = logical([0; diff(videotimestamps)==0]);
        if sessionNum==25 & epochnum==6 & strmatch(animalID,'emile')
            repeats(54832)=1;
        end
        newTimes=videotimestamps(~repeats);
        newTimesInds = find(~repeats);
    else
        newTimes = videotimestamps;
        newTimesInds = [1:length(videotimestamps)];
    end
    return;
    
else 
    
%     %first, check for any bursts of spurious dios; get rid of them NO LONGER NECESSARY as of fixDIOcorruption.m
%     diodiff = diff(dios)<.0005; % generally have an interval btwn .4-.05ms. anything <1ms shouldnt be real
%     burststarts = 1+find(diff(diodiff)==1);
%     burstends = 1+find(diff(diodiff)==-1);
%     disp(sprintf('%d bursts of bad dios; %d being removed in total',length(burststarts),sum(diodiff)))
%     for b = 1:length(burststarts)
%         lostdios = floor((dios(burstends(b)+1)-dios(burststarts(b)-1))/.04);
%         if lostdios>=1 %interp to replace
%             dios(burststarts(b):burstends(b)) = NaN;
%             replaceind = floor(linspace(burststarts(b)-1,burstends(b)+1,lostdios+2));
%             dios(replaceind(2:end-1)) = interp1([burststarts(b)-1,burstends(b)+1],[dios(burststarts(b)-1),dios(burstends(b)+1)],replaceind(2:end-1));
%         else
%             dios(burststarts(b):burstends(b)) = NaN;
%         end
%     end
%     dios = dios(~isnan(dios)); %get rid of nans
    
    % next, initialize and determine whether landmark gap present
    newTimeStamps = zeros(length(videotimestamps),1);
    %initial recording starttime should be a field in dios, but currently isnt. for now, get it from Binary2FF_LFP output
    try
        load(sprintf('EEG/%seeggnd%02d-%d-01.mat',animalID,sessionNum,epochnum))
        starttime = double(eeggnd{sessionNum}{epochnum}{1}.starttime);
        endtime = double(eeggnd{sessionNum}{epochnum}{1}.endtime);
    catch
        error('need to process LFP first to get starttime landmark')
    end
    gapfound = 1;
    findPulseGap = 1+find(diff(dios(1:100))>.4,1);
    if ~isempty(findPulseGap)
        if findPulseGap == 1
            disp('First pulse after gap is first dio');
            pulseIndAfterGap = findPulseGap;
            gapfound = 0;
        else
            disp(['First pulse after gap is: ', num2str(findPulseGap)]);
            pulseIndAfterGap = findPulseGap;
        end
    else
        pulseIndAfterGap = 1;
        disp('no gap in dios found; using first dio instead')
        gapfound = 0;
    end
    findFrameGap = 1+find(diff(videotimestamps(1:100))>.4,1);
    if ~isempty(findFrameGap)
        if findFrameGap == 1
            disp('First frame after gap is first frame');
            frameIndAfterGap = findFrameGap;
            gapfound = 0;
        else
            disp(['First frame after gap is: ' num2str(findFrameGap)]);
            frameIndAfterGap = findFrameGap;
        end
    else
        frameIndAfterGap = 1;
        disp('no gap in frames found; using first frame instead')
        gapfound = 0;
    end
    % next, deal with any frames before the gap/ assign initial timestamp(s)
    if gapfound
        %newTimeStamps(frameIndAfterGap) = dios(pulseIndAfterGap);
        %dios(pulseIndAfterGap) = NaN;
        if frameIndAfterGap<=pulseIndAfterGap %can just assign directly
            newTimeStamps(1:frameIndAfterGap-1) = dios(pulseIndAfterGap-frameIndAfterGap+1:pulseIndAfterGap-1);
        else %if there aren't enough dios, interpolate
            newTimeStamps(1:frameIndAfterGap-1) = interp1([1 frameIndAfterGap],[starttime,dios(pulseIndAfterGap)],[1:frameIndAfterGap-1]);
        end
        %dios(1:pulseIndAfterGap-1) = NaN;
    elseif pulseIndAfterGap~=1 %gap in dios only; assign first frame to first dio after gap
        newTimeStamps(1) = dios(pulseIndAfterGap);
        %dios(1:pulseIndAfterGap) = NaN;
    else
        frameIndAfterGap = 1; % even if there was a gap in videots, ignore it
%         if videotimestamps(1)<dios(1)  %can't use a dio if it's in the future
%             newTimeStamps(1) = videotimestamps(1);
%         else
%             newTimeStamps(1) = dios(1);
%             dios(1) = NaN;
%         end
    end
    
    
    indshift = pulseIndAfterGap-frameIndAfterGap;  %for translating dio indices to videotimestamp indices
    
    %  if camlogs available, use them to get rid of specific dios
    if ~isempty(camlog)
        disp('using dios and camlog')
        disp(sprintf('HWframes: %d, videots: %d, dios: %d',length(camlog),length(videotimestamps),length(dios)))
        disp(sprintf('Videogap: %f, diosgap: %f',videotimestamps(frameIndAfterGap)-starttime,dios(pulseIndAfterGap)-starttime))
        framecountdiff = diff(camlog);
        % identify location and # of dropped frames
        dropped_idx = find(framecountdiff>1 & framecountdiff<30000);  %deal with camcount wraparound
        num_dropped = framecountdiff(dropped_idx);
        
        %Exclude DIOs corresponding to dropped frames
        for d = 1:length(dropped_idx)
            dios = dios([1:dropped_idx(d),dropped_idx(d)+num_dropped(d):end]);
        end
        
        % search for any repeated videotimestamp regions; interp them
        reptimes = diff(videotimestamps) == 0;
        if sum(reptimes)>0 %if there are any timestamp repeats            
            % Get indices of each range of repeat regions
            repstarts = 1+find(diff(reptimes)==1); % ind of first repeat
            repends = 2+find(diff(reptimes)==-1); % add two: 1 to equalize length, 1 to get the ind AFTER repeat region  
            for b = 1:length(repstarts)
                %if there are approx the right # of dios corresponding to the repeat frames, interp
                if abs(sum(isExcluded(dios, [videotimestamps(repstarts(b)),videotimestamps(repends(b))]))-(repends(b)-repstarts(b)))<5
                    disp(sprintf('%d repeats, interping',repends(b)-repstarts(b)))
                    videotimestamps(repstarts(b)+1:repends(b)-1) = interp1([repstarts(b),repends(b)],[videotimestamps(repstarts(b)),videotimestamps(repends(b))],[repstarts(b)+1:repends(b)-1]);
                else   %if we're missing those dios also, remove
                    disp(sprintf('%d repeats without dios, removing',repends(b)-repstarts(b)))
                    videotimestamps(repstarts(b):repends(b)-1)=NaN;
                end
            end
            newTimesInds = find(videotimestamps(~isnan(videotimestamps)));
            newTimeStamps = newTimeStamps(~isnan(videotimestamps));
            videotimestamps = videotimestamps(~isnan(videotimestamps));
            
        else
            newTimesInds=[1:length(videotimestamps)];  %initialize with full vector
        end
        
        %reassign timestamps based on order from beginning
        mismatch = length(newTimeStamps(frameIndAfterGap:end))-length(dios(pulseIndAfterGap:end));
        if mismatch==0 %lengths match, just assign 1:1
            newTimeStamps(frameIndAfterGap:end) = dios(pulseIndAfterGap:end);
            newTimesInds = intersect(newTimesInds,[1:length(newTimeStamps)]);
        elseif mismatch>0 % more videotimestamps than dios; drop last few 
            if mismatch>5
                disp('not enough dios; look into this....press any key to continue with unadjusted timestamps')
                pause
                newTimeStamps = videotimestamps;
                
            else
                disp('not enough dios; dropping last few videotimestamps')
                newTimeStamps=[newTimeStamps(1:frameIndAfterGap-1);dios(pulseIndAfterGap:end)];
                newTimesInds = intersect(newTimesInds,[1:length(newTimeStamps)]);
            end
        else % too many dios, just disregard extra ones at end
            newTimeStamps(frameIndAfterGap:end) = dios(pulseIndAfterGap:pulseIndAfterGap+(length(newTimeStamps)-frameIndAfterGap));
            newTimesInds = intersect(newTimesInds,[1:length(newTimeStamps)]);
        end
        
    else
        disp('using dios only')
        
%         %first, check for any bursts of spurious dios; get rid of them
%         diodiff = diff(dios)<.0005; % generally have an interval btwn .4-.05ms. anything <1ms shouldnt be real
%         burststarts = 1+find(diff(diodiff)==1);
%         burstends = 1+find(diff(diodiff)==-1);
%         disp(sprintf('%d bursts of bad dios; %d being removed in total',length(burststarts),sum(diodiff)))
%         for b = 1:length(burststarts)
%             lostdios = floor((dios(burstends(b)+1)-dios(burststarts(b)-1))/.04);
%             if lostdios>=1 %interp to replace
%                 dios(burststarts(b):burstends(b)) = NaN;
%                 replaceind = floor(linspace(burststarts(b)-1,burstends(b)+1,lostdios+2));
%                 dios(replaceind(2:end-1)) = interp1([burststarts(b)-1,burstends(b)+1],[dios(burststarts(b)-1),dios(burstends(b)+1)],replaceind(2:end-1));
%             else
%                 dios(burststarts(b):burstends(b)) = NaN;
%             end
%         end
%         dios = dios(~isnan(dios)); %get rid of nans
        
        % then, calculate standard delay (btwn videotimestamp and dio) based on first 1000 dios/timestamps
        %dios=dios(dios<videotimestamps(end)); %if any dios occur after final timestamp, get rid of them
        delay = mean(videotimestamps(frameIndAfterGap:frameIndAfterGap+1000)-dios(pulseIndAfterGap:pulseIndAfterGap+1000));
        delaystd = std(videotimestamps(frameIndAfterGap:frameIndAfterGap+1000)-dios(pulseIndAfterGap:pulseIndAfterGap+1000));
        disp(sprintf('delay is %04f, std is %04f',delay,delaystd))
        if (delay <=0 | delaystd>=delay )
            error('delay doesnt make sense')
        end
        %calc standard inverval btwn frames (use to tell if timestamps are regularly spaced vs piled up)
        intervals = diff(videotimestamps);
        Fs = mean(intervals(1:1000));
        Fsstd = std(intervals(1:1000));
        intervals = [intervals;Fs]; %fix length
        
        % if there's a perfect dio match during periods of clean acquisition, assign them
        for frameind = frameIndAfterGap:length(videotimestamps)-1 %start from end and work backwards
            if (abs([intervals(frameind) intervals(frameind+1)]-Fs) < Fsstd) %if intervals are regular
                diomatch_idx = find((dios>(videotimestamps(frameind)-delay-delaystd) & dios<(videotimestamps(frameind)-delay)));
                if ~isempty(diomatch_idx) %match found! remove it from available pool(prevent repeats) and update newTimeStamps
                    newTimeStamps(frameind) = dios(diomatch_idx);
                    dios(diomatch_idx) = NaN;
                end
            end
        end
        
        %find any uncorrected timestamps and figure out what they should be based on neighbors
        if newTimeStamps(end)==0 %deal with end condition
            if ~isnan(dios(end))
                newTimeStamps(end) = dios(end);
                dios(end) = NaN;
            else
                newTimeStamps(end) = endtime;
            end
        end
        if newTimeStamps(frameIndAfterGap)==0 %deal with start condition
            if ~isnan(dios(pulseIndAfterGap))  %this is most likely
                newTimeStamps(frameIndAfterGap) = dios(pulseIndAfterGap);
                dios(pulseIndAfterGap) = NaN;
            elseif frameIndAfterGap==1  %this will be slightly off, but it shouldn't conflict
                newTimeStamps(frameIndAfterGap) = starttime;
            else
                newTimeStamps(frameIndAfterGap) = newTimeStamps(frameIndAfterGap-1)+delay;
            end
        end
        preprob = find(diff(newTimeStamps==0)==1);  %define borders of problem areas
        postprob = find(diff(newTimeStamps==0)==-1)+1;
        if length(preprob)~=length(postprob)
            error('imbalanced problems')
        end
        for p = 1:length(preprob)
            diosInRange = find(dios>newTimeStamps(preprob(p)) & dios< newTimeStamps(postprob(p)));
            if (postprob(p)-preprob(p)-1)==length(diosInRange)
                newTimeStamps(preprob(p)+1:postprob(p)-1) = dios(diosInRange);
                dios(diosInRange) = NaN;
            else % don't use dios, just interpolate between neighbors
                if preprob(p)<frameIndAfterGap  %special initial condition
                    newTimeStamps(preprob(p)+1:postprob(p)-1) = interp1([preprob(p) postprob(p)],[dios(pulseIndAfterGap) newTimeStamps(postprob(p))],[preprob(p)+1:postprob(p)-1]);
                else
                    newTimeStamps(preprob(p)+1:postprob(p)-1) = interp1([preprob(p) postprob(p)],[newTimeStamps(preprob(p)) newTimeStamps(postprob(p))],[preprob(p)+1:postprob(p)-1]);
                end
            end
        end  
        newTimesInds = [1:length(newTimeStamps)];
    end
end    
%assess

if ~isempty(camlog)
    disp(sprintf('dropped frames:%d',sum(num_dropped)))
else
    disp(sprintf('dios unused:%d',sum(~isnan(dios))))
end
        
% sanity check1: no original videotimestamp should ever precede corrected timestamps
lengthmismatch = length(newTimeStamps(frameIndAfterGap:end))-length(videotimestamps(frameIndAfterGap:end));
if lengthmismatch==0
    disp(sprintf('average lag: %04f',mean(videotimestamps-newTimeStamps)))
    if any((videotimestamps(frameIndAfterGap:end)-newTimeStamps(frameIndAfterGap:end))<-.005)
    disp('no original videotimestamp should ever precede corrected timestamps, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
    end
elseif lengthmismatch<0
    disp(sprintf('average lag: %04f',mean(videotimestamps(frameIndAfterGap:length(newTimeStamps))-newTimeStamps(frameIndAfterGap:end))))
    if any((videotimestamps(frameIndAfterGap:length(newTimeStamps))-newTimeStamps(frameIndAfterGap:end))<-.005)
        disp(sprintf('%d found with delayed dio timestamps',sum((videotimestamps(frameIndAfterGap:length(newTimeStamps))-newTimeStamps(frameIndAfterGap:end))<-.005)))
        warning('some original videotimestamp precede corrected timestamps');
    pause
    %newTimeStamps = videotimestamps;
    end
else
    error('more timestamps than videoframes...something went wrong')
end
% sanity check2: no duplicate timestamps should ever be found
if any(diff(newTimeStamps)<=0)
    error('duplicate or inverted timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 
% sanity check3: no nan timestamps should happen
if any(isnan(newTimeStamps))
    error('nan timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 
% sanity check4: no zero timestamps should happen
if (epochnum==1 & any(newTimeStamps(2:end)==0)) |(epochnum>1 & any(newTimeStamps==0))
    error('zero timestamps found, press any key to continue with unaligned timestamps or ctrl-c to abort');
    pause
    newTimeStamps = videotimestamps;
end 

%testing
    figure
    plot(1:3,repmat(starttime,1,3),'*')
    hold on
    plot([dios(1:1000),videotimestamps(1:1000),newTimeStamps(1:1000)]','.-') 
    title('check beginning')
    figure
    plot([dios(end-1000:end),videotimestamps(end-1000:end),newTimeStamps(end-1000:end)]','.-')
    title('check end')

    newTimes = newTimeStamps;
    
end 
