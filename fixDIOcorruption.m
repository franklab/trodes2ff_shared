animal = 'emile';
days = [13:14];
%days = [15:31];
cd('/media/anna/whirlwindtemp2/emile/filterframework')

for d = days
    load(sprintf('%sDIO%02d.mat',animal,d))  
    disp(['-----------fixing day ' num2str(d) '--------'])
    for e = 1:length(DIO{d})
        figure
        subplot(2,1,1)
        hold on
        % run through all chans to identify consensus periods of bad data (all start and end at slightly different times)
        for c = 1:length(DIO{d}{e})
            diodiff{c} = diff(DIO{d}{e}{c}.times)<.0005; % generally have an interval btwn .4-.05ms. anything <1ms shouldnt be real
            burststarts{c} = DIO{d}{e}{c}.times(1+find(diff(diodiff{c})==1));
            burstends{c} = DIO{d}{e}{c}.times(1+find(diff(diodiff{c})==-1));
            if ~isempty(diodiff{c})
                if diodiff{c}(end)==1
                burstends{c} = [burstends{c};DIO{d}{e}{c}.times(end)];
                end
            end
            plot(DIO{d}{e}{c}.times, c+DIO{d}{e}{c}.values)
            plot(burststarts{c},c*ones(1,length(burststarts{c})),'k.')
            plot(burstends{c},c*ones(1,length(burstends{c})),'k.')
        end
        axis tight
        title(sprintf('session%d ep%d',d,e))
        allstarts = cell2mat(burststarts');
        allends = cell2mat(burstends');
        
        if ~isempty(allstarts)
            [n,edges] = histcounts(allstarts,[min(allstarts)-.002:.001:max(allstarts)+.001]);
            constarts = edges(n>13);         
            [n,edges] = histcounts(allends,[min(allends):.001:max(allends)+.002]);
            conends = edges(1+find(n>=16)); %1+ to get right edge of bin (max rather than min)
            %check that starts =ends
            if length(constarts)~=length(conends)
                error('landmark mismatch')
                pause
            else
                disp([num2str(length(constarts)) ' corruption events'])
            end
            % for each corruption event, run through each channel and remove any dios that fall in corruption window
            subplot(2,1,2)
            hold on
            for c = 1:length(DIO{d}{e})
                if length(DIO{d}{e}{c}.times)>5 && isExcluded(1/mode(diff(DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==1))),[28, 32]);
                    disp('this is a camsync! reconstructing lost syncs')
                    ups = DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==1);
                    downs = DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==0);
                    for event = 1:length(constarts)
                        startup = lookup(constarts(event)-.000001,ups,-1); % identify closest smaller-than up timestamp; .000001 ensures that it must be <, not =
                        startup = lookup(ups(startup),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                        endup = lookup(conends(event)+.00001,ups,1);  %closest larger-than timestamp in ups; .00001 ensures that it must be >, not =
                        endup = lookup(ups(endup),DIO{d}{e}{c}.times); %translate back to full times inds
                        lostups = round((DIO{d}{e}{c}.times(endup)-DIO{d}{e}{c}.times(startup))/mode(diff(ups)))-1;  %should be close to a multiple of Fs
                        
                        startdown = lookup(constarts(event)-.000001,downs,-1); % identify closest smaller-than down timestamp
                        startdown = lookup(downs(startdown),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                        enddown = lookup(conends(event)+.000001,downs,1);  %closest larger-than timestamp in downs
                        enddown = lookup(downs(enddown),DIO{d}{e}{c}.times); %translate back to full times inds
                        lostdowns = round((DIO{d}{e}{c}.times(enddown)-DIO{d}{e}{c}.times(startdown))/mode(diff(downs)))-1;
                        
                        %nan out the intervening garbage
                        DIO{d}{e}{c}.times(max([startup startdown])+1:min([endup enddown])-1) = NaN;
                        DIO{d}{e}{c}.values(max([startup startdown])+1:min([endup enddown])-1) = 5; %uint8 cant represent nan, so use 5 as marker
                        
                        %interp to replace any lost ups or downs
                        if lostups>=1                             
                            replaceind = floor(linspace(startup,endup,lostups+2));
                            DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startup endup], [DIO{d}{e}{c}.times(startup),DIO{d}{e}{c}.times(endup)],replaceind(2:end-1));
                            DIO{d}{e}{c}.values(replaceind(2:end-1)) = 1;
                        end
                        if lostdowns>=1
                            replaceind = floor(linspace(startdown,enddown,lostdowns+2));
                            DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startdown enddown], [DIO{d}{e}{c}.times(startdown),DIO{d}{e}{c}.times(enddown)],replaceind(2:end-1));
                            DIO{d}{e}{c}.values(replaceind(2:end-1)) = 0;
                        end
                    end
                    %get rid of nans and 5's
                    DIO{d}{e}{c}.times = DIO{d}{e}{c}.times(~isnan(DIO{d}{e}{c}.times));
                    DIO{d}{e}{c}.values = DIO{d}{e}{c}.values(DIO{d}{e}{c}.values~=5);
                else % this is not a camsync, so we have no way of recoverning any real triggers during the corrupted times :(
                    invalid = isExcluded(DIO{d}{e}{c}.times,[constarts', conends']);
                    DIO{d}{e}{c}.times = DIO{d}{e}{c}.times(~invalid);
                    DIO{d}{e}{c}.values = DIO{d}{e}{c}.values(~invalid);
                end
                plot(DIO{d}{e}{c}.times, c+DIO{d}{e}{c}.values)
            end
            axis tight
        end
    end
    save(sprintf('%sDIO%02d.mat',animal,d),'DIO') 
    disp(['day ' num2str(d) ' DIOs saved'])
end

        