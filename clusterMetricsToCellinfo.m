function cellinfo = clusterMetricsToCellinfo(destdir, animalID, sessionNum, epoch,...
    ntrode, metrics_file, numspikes, clusterlabels, epochlength,...
    varargin)
% reads in mv2 file
% for each cell, stores each metric in a field in cellinfo
% info about all clusters is saved, even the rejected ones

% process varargin if present and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end
% Check for existing cell info struct
try
    load([destdir, animalID, 'cellinfo']);
catch
    disp('cellinfo structure does not exist..running createcellinfostruct')
    createcellinfostruct(destdir, animalID);
    load([destdir, animalID, 'cellinfo']);
end

% 2 sources of info:
% - curated_metrics.json, updated post merging with create_curated_metrics.mat (or if no merging, cluster_metrics.json)
% - mv2 manually saved from mountain viewer
%cd(sorted_results_dir)

% what do we need from the mv2 in ms3? maybe nothing?
%mv2file = dir('*.mv2');
%mv2 = loadjson(mv2file.name);

%jsonfilename = dir(metrics_file);
try
    clustmetrics = loadjson(metrics_file);
catch
    error('No cluster metrics file found')
end

% % parse mv2  only useful for pairmetrics; not used as of ms3
% % clusternames (ie x0x3... is  a byproduct of load json. only pay attention to the last digit(s) following the first '3' which may be separated with a _
% clusternames = fieldnames(mv2.cluster_attributes);
% clusters = regexprep(clusternames,'_',''); % remove _
% clusters = regexp(clusters,'(?<=x0x3)\d+','match'); %remove x0x3 prefix
% clusters = cellfun(@(x) cell2mat(x),clusters,'UniformOutput',0); %get out of nested cell array
% clusters = cell2mat(cellfun(@(x) str2num(x),clusters,'UniformOutput',0)); % more out of nested cell and convert to double
% if ~isempty(mv2.cluster_pair_attributes)
%         pairfields = fieldnames(mv2.cluster_pair_attributes);
%         pairs = regexp(pairfields,'_','split');
%         pairs = cell2mat(cellfun(@(x) [str2num(x{2}) str2num(x{3})],pairs,'UniformOutput',0));
% else
%         pairs = [0 0]; %no pairs merged or none listed as overlapping
% end

for ic = 1:length(clusterlabels)
    % identify which cluster metrics match label (need to match based on label bc not all eps have spikes from all clusters
    clustind = find(clusterlabels(ic)==cellfun(@(x) x.label,clustmetrics.clusters));
    if ~isempty(clustind)
        
        
        cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)} = clustmetrics.clusters{clustind}.metrics;
        cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.label = clusterlabels(ic);
        %cellinfo{sessionNum}{epoch}{ntrode}{ic}.num_events_total = cellinfo{sessionNum}{epoch}{ntrode}{ic}.num_events;  %save total #events for whole day
        %cellinfo{sessionNum}{epoch}{ntrode}{ic}.numspikes = numspikes{ic};    % replace num_events with accurate epoch-specific count
        try
            assert(cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.num_events==numspikes{ic});
        catch
            warning('numspikes derived by counting from mda does not equal that from .json')
        end
        % disambiguate the firing_rate calculation
        cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.firingrate_first2lastspike = cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.firing_rate;
        cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.firingrate_per_epoch = numspikes{ic}/epochlength{ic};
        rmfield(cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)},'firing_rate');
        
        if isfield(clustmetrics.clusters{clustind},'tags') || isempty(clustmetrics.clusters{clustind}.tags)     
            % add tags (noise, mua, artifact, etc)
            cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.tags = clustmetrics.clusters{clustind}.tags;
            % filter framework can't parse a list of tags, so use logic here to define accepted/not for later filtering
            if ( ismember('mua',clustmetrics.clusters{clustind}.tags) | ismember('rejected',clustmetrics.clusters{clustind}.tags) ...
                    | ismember('noise',clustmetrics.clusters{clustind}.tags) |ismember('artifact',clustmetrics.clusters{clustind}.tags) )
                cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.accepted = 0;
            else  %only other options are accepted or untagged
                cellinfo{sessionNum}{epoch}{ntrode}{clusterlabels(ic)}.accepted = 1;
            end
        end
    end
    %mv2ind = find(clusters==clustmetrics.clusters{ic}.label);
    %if isfield(mv2.cluster_attributes.(clusternames{mv2ind}),'tags')
    %        cellinfo{sessionNum}{epoch}{ntrode}{ic}.tags = mv2.cluster_attributes.(clusternames{mv2ind}).tags;
    %end
    %add merge info
    %if any(pairs(:,1)==clustmetrics.clusters{ic}.label)
    %    cellinfo{sessionNum}{epoch}{ntrode}{ic}.mergepartners = pairs(pairs(:,1)==clustmetrics.clusters{ic}.label,2);
    %end
end


%store data in cellinfo and save
save([destdir, animalID, 'cellinfo.mat'],'cellinfo')

end


