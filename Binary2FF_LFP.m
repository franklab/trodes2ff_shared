function Binary2FF_LFP(animalID, srcdir, destdir, sessionNum, varargin)

%Demetris Roumis July 2016
%This function saves LFP data in the Filter Framework v1 format.
%It is assumed that there is a directory of binary files in the form of:
%<animalname>/preprocessed/<date (%YYYY%MM%DD)>/<date>_<animal>_<epoch>.LFP/...
%...<date>_<animal>_<epoch>.LFP_nt<ntrode>ch<channel>.dat
%ex: /mnt/data19/droumis/D10/preprocessed/20160512/20160512_D10_01.LFP/20160512_D10_01.LFP_nt4ch1.dat

%animalID -- a string identifying the animal's id (appended to the beginning of the files).. ex: 'D10'
%sessionNum -- day.. ex: '4'
%srcdir -- preprocessed binary directory.. ex: /mnt/data19/droumis/D10/preprocessed/20160512/
%destdir -- the directory where the processed files should be saved for the animal...
%ex: /mnt/data19/droumis/D10/filterframework/EEG/

%options:
%define the nTrodeareas and the refNTrodes

nTrodeareas = []; %depricated
refNTrodes= []; %depricated

% process varargin and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end

cd(srcdir);
lfpepochDirs = dir('*.LFP');
lfpepochDirs = arrayfun(@(x) x.name,lfpepochDirs,'UniformOutput', false);
lfpepochDirs = sort(lfpepochDirs); %this should be safe if the file naming is consistent with '<date>_<animal>_<2digitepoch>.LFP'
disp('Processing LFP files...');
lfpepoch_IDs = getepochIDs(lfpepochDirs);
for ilfpepoch = 1:length(lfpepochDirs)
    cd(srcdir); %necessary for every loop after the first
    cd(lfpepochDirs{ilfpepoch}); %go into current epoch lfp dir
    lfpepochID = lfpepoch_IDs(ilfpepoch);
    fprintf('reading epoch %d lfp from trodes extracted\n', lfpepochID);
    %     lfpepochID = str2num(lfpepochID{3}(1:2)); %strip off epoch ID num
    lfpntrodefiles = dir('*.LFP*');
    
    timestampsfile = dir('*.timestamps.adj*');
    if isempty(timestampsfile)
        disp('no adjusted timestamps file detected.. hit any key to continue with unadjusted timestamps or ctrl-c to abort')
        pause
        timestampsfile = dir('*.timestamps*');
    end
    timestampsData = readTrodesExtractedDataFile(timestampsfile.name);
    % cast times to double, to avoid rounding error when converting samples
    % to seconds
    timestampsData.fields.data = double(timestampsData.fields.data);
    
    tmpData = [];
    for lfpntrodeNum = 1:length(lfpntrodefiles) %load all ntrodes at once to be able to sort by ntrodeID
        tmpData{lfpntrodeNum} = readTrodesExtractedDataFile(lfpntrodefiles(lfpntrodeNum).name);
    end
    tmpid = cell2mat(cellfun(@(x) x.ntrode_id,tmpData,'UniformOutput', false)); %get the reorder indices
    [sortedIDs, tmpid]  = sort(tmpid);
    tmpData = tmpData(tmpid);
    for lfpntrodeNum = 1:length(sortedIDs)
        lfpntrodeID = sortedIDs(lfpntrodeNum);
        fprintf('creating nt%d lfp\n',lfpntrodeID);
        eeggnd = [];
        sampstep = tmpData{lfpntrodeNum}.clockrate / 1500; %get the integer sample step size. replace 1500 with the export arg once mattias implements
        timestamps_normalized = timestampsData.fields.data / sampstep;
        timestamps_normalized_Ids = round((timestamps_normalized - timestamps_normalized(1))+1);%
        currdata = nan(max(timestamps_normalized_Ids),1); % nanitialize (copywrite demetris 2016)
        currdata(timestamps_normalized_Ids) = tmpData{lfpntrodeNum}.fields.data;
        currdata = double(currdata).*tmpData{lfpntrodeNum}.voltage_scaling; %get data into double type and voltage
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.data = currdata;
        %         eeggnd{sessionNum}{lfpepoch}{lfpntrode}.data = double(tmpData{lfpntrode}.fields.data).*tmpData{lfpntrode}.voltage_scaling; %get data into double type and voltage
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.descript = tmpData{lfpntrodeNum}.description;
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.samprate = 1500; %mattias will implement this as an export arg in the future
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.clockrate = tmpData{lfpntrodeNum}.clockrate;
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.starttime = timestampsData.fields.data(1)/tmpData{lfpntrodeNum}.clockrate; %convert to seconds
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.endtime = timestampsData.fields.data(end)/tmpData{lfpntrodeNum}.clockrate; %convert to seconds
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.data_voltage_scaled = 1;
        if ~isempty(nTrodeareas) && ~isempty(nTrodeareas)
            disp('use of ntrodesareas and refntrodes is depricated.. specify reference in tetinfostruct')
            %             %look for area and area ref ntrodes in the tetsareas input
            %             ntrodeindcell = cellfun(@(x) x==lfpntrodeName, nTrodeareas(:,2), 'UniformOutput', false);
            %             ntrodeind = cell2mat(cellfun(@(x) any(x), ntrodeindcell, 'UniformOutput', false));
            %             if any(ntrodeind)
            %                 nTrodeArea = nTrodeareas(ntrodeind,1);
            %                 eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.area = nTrodeArea;
            %                 refntrodeindcell = cell2mat(cellfun(@(x) strcmp(x,(nTrodeareas{ntrodeind,1})), refNTrodes(:,1), 'UniformOutput', false));
            %                 if any(refntrodeindcell)
            %                     refNTrode = refNTrodes(refntrodeindcell,2);
            %                     eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.refNTrode = refNTrode{1};
            %                 end
            %             else %if it's not in the list of ntrodes, list itself as its reference
            %                 eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.area = 'ref';
            %                 eeggnd{sessionNum}{lfpepoch}{lfpntrodeName}.refNTrode = lfpntrodeName;
            %             end
        else
            eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.area = '';
            eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.refNTrode = [];
        end
        %         eeg{sessionNum}{lfpepoch}{lfpntrode}.trodes_datatype = tmpData{lfpntrode}.fields.type; %int16 in trodes
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.nTrode = tmpData{lfpntrodeNum}.ntrode_id;
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.nTrodeChannel = tmpData{lfpntrodeNum}.ntrode_channel;  %1-based
        if strcmp(tmpData{lfpntrodeNum}.reference, 'on');  % we export unref'd data, so this should never be the case
            eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.referenced = 1;
        else
            eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.referenced = 0;
        end
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.low_pass_filter = tmpData{lfpntrodeNum}.low_pass_filter;
        eeggnd{sessionNum}{lfpepochID}{lfpntrodeID}.voltage_scaling = tmpData{lfpntrodeNum}.voltage_scaling;
        %eeggnd{sessionNum}{lfpepoch}{lfpntrode}.refNtrode_chan = tmpData{lfpNtrode}.somethingTBD coming soon
        %one lfp file per epoch & ntrode
        if ~exist([destdir '/EEG/'],'dir'),
            mkdir([destdir '/EEG/']);
        end
        save([destdir '/EEG/' animalID,'eeggnd',num2str(sprintf('%02d',sessionNum)),'-',num2str(lfpepochID),'-',num2str(sprintf('%02d',lfpntrodeID)),'.mat'],'-v6','eeggnd');
    end
end
end
% FFv1filetime = toc;
% disp([sprintf('%.02f',FFv1filetime) ' seconds to make LFP files'])

function lfpepoch_IDs = getepochIDs(lfpepochDirs)
lfpepoch_IDs = [];
for ilfpep = 1:length(lfpepochDirs)
    ilfpepochID = strsplit(lfpepochDirs{ilfpep}, '_');
    ilfpepochID = str2num(ilfpepochID{3}(1:2)); %strip off epoch ID num
    lfpepoch_IDs(ilfpep,1) = ilfpepochID;
end
end