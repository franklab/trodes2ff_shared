function spikewidth = calc_mdaspikewidth(varargin)

% Calculate spikewidth from mean waveforms exported by MountainSort.
% Assumes that a file waveforms.mda exists in <date>.mountain/output/<pipeline>--nt<tetnum>, 
%   and that we are currently within that directory.
% Modeled after mex function parmcalc, this function finds the spikewidth from the channel with the highest peak.
% Spikewidth is defined as the number of samples between the max peak and min trough.

% MEANT TO BE CALLED BY MDA2SPIKES

% Possible argin: detection sign used for mountainsort (detect_sign in
% pipeline): 0 both, 1 positive, -1 negative.
% Default 0 to accommodate all spikes.
detect = 0;

if (~isempty(varargin))
    assign(varargin{:});
end

outputdir = pwd;

%Load waveforms exported by Mountainview
waveforms = sprintf('%s/waveforms.mda',outputdir);
if ~exist(fullfile(cd, 'waveforms.mda'), 'file')
    spikewidth = [];
    return
else
    W = readmda(waveforms); %rows are channels, columns are samples, third dimension is each cell
    if detect==1
        [tmppeaks,i] = max(W,[],2); % i is the column index of the peak of each channel
    elseif detect==-1
        [tmppeaks,i] = min(W,[],2);
        tmppeaks = abs(tmppeaks);
    else
        [tmppeaks,i] = max(abs(W),[],2); % max absolute value to accommodate both pos and neg spikes
    end
    
    [maxpeak,j] =max(tmppeaks,[],1); % j is the row index of the peak across channel peaks
    
    % use j to index i, find the index of the peak of each waveform across channels
    j = reshape(j,1,size(j,3));
    i = reshape(i,4,size(i,3));
    peak_vals =  zeros(1,size(i,2));
    spikewidth =  zeros(1,size(i,2));
    for q = 1:size(i,2)
        peak_vals(q) =  W(j(q),i(j(q),q),q); % the original peak, signed
        peak_ind = i(j(q),q); % column of the peak on the max channel
        if peak_vals(q) > 0 % if the peak is positive, we're looking for a negative trough
            % find the minimum
            [trough,k] = min(W(j(q),:,q));
        elseif peakvals(q) < 0 % if the peak is negative, we're looking for a positive trough
            % find the maximum
            [trough,k] = max(W(j(q),:,q));
        end
        spikewidth(q) =  abs(k-peak_ind); %in number of samples
    end
    
end