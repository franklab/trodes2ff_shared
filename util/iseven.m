function out = iseven(x)
% Returns 1 or 'true' if the input is an even integer.  Returns 0 if odd.
out = floor(x/2)==x/2;
end