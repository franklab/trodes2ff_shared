function Xuw = unwrap_inc(X, wrapval)
% UNWRAP_INC unwrap an increasing series that wraps at a set value (e.g. a
% counter)

% Tom Davidson, UCSF, May 2016 <tjd@alum.mit.edu>

if ~isvector(X) || ~isnumeric(X)
  error('Can only handle numeric vectors.');
end

% find negative diffs
Xd = diff(X);
wrapi = Xd<0;

% add wrap value at each wrap point
Xd(wrapi) = Xd(wrapi)+wrapval;
Xuw = cumsum([X(1) Xd]);
