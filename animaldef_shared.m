function animalinfo = animaldef(animalname)
% ANIMALDEF (SHARED COPY).
% To use this as your animaldef file, create a symlink somewhere on your 
% Matlab path called animaldef.m :
%
%   cd /path/to/my/matlab/code
%   ln -s /full/path/to/animaldef_shared.m animaldef.m
DRff = '/data2/demetris/';
DRv = '/data2/demetris/';
switch lower(animalname)

    %Anna's animals
    case 'algernon'
        animalinfo = {'algernon','/data45/algernon/filterframework/','algernon','/data45/algernon/raw/','/data45/algernon/preprocessing/'};
    case 'surprise'
        animalinfo = {'surprise','/data45/surprise/filterframework/','surprise','/data45/surprise/raw/','/data45/surprise/preprocessing/'};
        
        %Demetris' animals
    case 'demetris' %{filter_output, results_output, figures}
        DRanalysis = '/opt/DR_swapdata11/analysis'; 
        animalinfo = {'DR', [DRanalysis '/filter_output/'], [DRanalysis '/results_output/'], [DRanalysis '/figures/']};
    case 'jz4' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'JZ4';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata9/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'jz3' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'JZ3';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata7/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'jz2' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'JZ2';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata8/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'jz1' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'JZ1';
        ffdir = DRff; %DRworking;
        ppdir = DRv; %'/opt/data20/'; %%
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'd13' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'D13';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata6/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'd12' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'D12';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata5/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'd10' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'D10';
        ffdir = DRff;
        ppdir = '/opt/DR_swapdata4/';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'd09' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'D09';
        ffdir = 'does not exist';
        ppdir = 'does not exist';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'd05' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'D05';
        ffdir = 'does not exist';
        ppdir = 'does not exist';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 't15' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        ffdir = 'does not exist';
        ppdir = 'does not exist';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};
    case 'ch3' %{animal_name, filterframework, animal_ID, raw, preprocessing}
        anid = 'CH3';
        ffdir = 'does not exist';
        ppdir = 'does not exist';
        rawdir = DRv;
        animalinfo = {anid, [ffdir anid '/filterframework/'], anid, [rawdir anid '/raw/'], [ppdir anid '/preprocessing/']};

    %Mari's animals
    case 'apollo'
        animalinfo = {'Apollo','/opt/data40/mari/Apo/','apo'};
    case 'brody'
        animalinfo = {'Brody','/opt/data40/mari/Bro/','bro'};
    
    % Annabelle's animals
    case 'arnold'
        animalinfo = {'Arnold', '/datatmp/kkay/Arn/', 'arn'};
    case 'barack'
        animalinfo = {'Barack', '/data12/kkay/Bar/', 'bar'};
    case 'calvin'
        animalinfo = {'Calvin', '/data12/kkay/Cal/', 'cal'};
    case 'dwight'
        animalinfo = {'Dwight', '/data12/kkay/Dwi/', 'dwi'};
        
    % Kenny's animals
    case 'chapati'
        animalinfo = {'Chapati','/opt/data40/mari/Cha/','cha'};
    case 'dave'
        animalinfo = {'Dave','/data12/kkay/Dav/','dav'};
    case 'government'
        animalinfo = {'Government','/opt/data40/mari/Gov/','gov'};
        
    % Kenny & Mari's animals    
    case 'egypt'
        animalinfo = {'Egypt','/opt/data40/mari/Egy/','egy'};
    case 'higgs'
        animalinfo = {'Higgs','/opt/data40/mari/Hig/','hig'};
        

    case 'frank'
        animalinfo = {'Frank', '/data12/kkay/Fra/', 'fra'};
    case 'bond'
        animalinfo = {'Bond', '/data12/kkay/Bon/', 'bon'};
        
    % Maggie's animals
    case 'corriander'
        animalinfo = {'Corriander','/data12/kkay/Cor/','Cor'};
        
    %Jai's animals
    case 'r1'
        animalinfo = {'R1','/opt/data40/mari/FromJai/R1_/','R1'};
        

    % Mattias' animals

    case 'miles'
        animalinfo = {'Miles', '/data/mkarlsso/Mil/', 'mil'};
    case 'nine'
        animalinfo = {'Nine', '/data/mkarlsso/Nin/', 'nin'};
    case 'ten'
        animalinfo = {'Ten', '/data/mkarlsso/Ten/', 'ten'};
    case 'dudley'
        animalinfo = {'Dudley', '/data/mkarlsso/Dud/', 'dud'};
    case 'alex'
        animalinfo = {'Alex', '/data/mkarlsso/Ale/', 'ale'};
    case 'conley'
        animalinfo = {'Conley', '/data/mkarlsso/Con/', 'con'};

    case 'five'
        animalinfo = {'Five', '/data13/mcarr/Fiv/', 'Fiv'};

    % Ana's animals
    case 'm01'
        animalinfo = {'m01', '/data/ana/M01/', 'm01'};
    case 'm02'
        animalinfo = {'m02', '/data/ana/M02/', 'm02'};
    otherwise
        error(['Animal ',animalname, ' not defined.']);
end
