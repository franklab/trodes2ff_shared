function Binary2FF_MARKS(animalID, srcdir, destdir, sessionNum, datestring, varargin)

% AKG 2018
% This function converts the trodes export function export_spikes into marks (peak amp on each chan) that can be used for clusterless decoding  output of MountainSort to SPIKES data in the Frank Lab Filter Framework format.
% one file per ntrode 
% uses tetinfo structure with deadchan information to skip marks on dead chans
% if ntrodes=[], sort all indicated as ca1 in tetinfo struct (days with no ca1 ntrodes will not import any marks)
% if ntrodes=-1, process all detected ntrodes automatically 

% TODO: make it accept input from mountainsort's waveform output too

% VARARGIN -- optional input in the form of: 'option', value, 'option', value
overwrite =0;
marksource='trodes';
ntrodes=[];
% process varargin if present and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end

%% Check for existing marks
cd(destdir);
if ~isempty(dir([animalID 'marks' num2str(sprintf('%02d', sessionNum)) '.mat'])) && ~overwrite
    disp('marks file detected... will try to append to existing structure')
    load([animalID 'marks' num2str(sprintf('%02d', sessionNum)) '.mat']) 
elseif ~isempty(dir([animalID 'spikes' num2str(sprintf('%02d', sessionNum)) '.mat'])) && overwrite
    disp('marks file detected... will overwrite')
else
    disp('no marks file detected... proceeding to create one from scratch')
end
load([animalID 'tetinfo.mat']) % load tetinfo for ca1 label and deadchan specification
%%  import marks generated by trodes extracter
if strcmp(marksource,'trodes')
    
    % Collect ntrodes,epochs, and timestamps to use
    cd(srcdir);
    disp('Processing marks binaries...');
    epochDirs = dir('*.spikes');
    epochDirs = arrayfun(@(x) x.name,epochDirs,'UniformOutput', false);
    epochDirs = sort(epochDirs); % file naming should be consistent with '<date>_<animal>_<2digitepoch>.DIO'
    epoch_IDs = [];
    for iep = 1:length(epochDirs)
        iepochID = strsplit(epochDirs{iep}, '_');
        iepochID = str2num(iepochID{3}(1:2)); %strip off epoch ID num
        epoch_IDs(iep,1) = iepochID;
    end
    tic
    for iep = 1:length(epochDirs)
        epoch = epoch_IDs(iep);
        cd(srcdir); %necessary for every loop after the first
        cd(epochDirs{iep}); %go into current epoch DIO dir
        ntrodefiles = dir('*.adj.dat');%find the adjusted DIO files
        if ~isempty(dir('*.adj.adj.dat'))
            disp('multiple adjusted DIO files for same channel exist || adj.adj.dat || fix before proceeding')
            return
        end
        tmpData = [];
        for tmpchans = 1:length(ntrodefiles) %load all ntrodes at once to be able to sort by ntrodeID
            tmpData{tmpchans} = readTrodesExtractedDataFile(ntrodefiles(tmpchans).name);
        end
        tmpid = cell2mat(cellfun(@(x) x.ntrode_id,tmpData,'UniformOutput', false)); %get the id's (not in numeric order)
        tmpData(tmpid) = tmpData;  %sort into numerical order
        tmpid = sort(tmpid);  %sort tmpid into numerical order too
        if ntrodes==-1
            ntrodes_ep=tmpid; % if ntrode list is empty, process all files
        elseif isempty(ntrodes)
            ntrodes_ep = evaluatefilter(tetinfo{sessionNum}{epoch},'isequal($area, ''ca1'')'); 
        else
            ntrodes_ep = ntrodes; % use specified ntrode set
        end
        if isempty(ntrodes_ep)
            disp(sprintf('no mark tets specified for session %d ep %d',sessionNum, epoch))
            continue
        end
        for t = 1:length(ntrodes_ep);
            tet=ntrodes_ep(t);
            tetind = find(tmpid==tet); %in case tmpid is not a complete list
            if isempty(tetind)
                disp(sprintf('tet %d spike binary file not found..skipping', tet))
                continue
            end
            marks{sessionNum}{epoch}{tet}.descript = tmpData{tetind}.description;
            marks{sessionNum}{epoch}{tet}.byte_order = tmpData{tetind}.byte_order;
            marks{sessionNum}{epoch}{tet}.ntrode_id = tmpData{tetind}.ntrode_id;
            marks{sessionNum}{epoch}{tet}.original_file = tmpData{tetind}.original_file;
            marks{sessionNum}{epoch}{tet}.num_channels = tmpData{tetind}.num_channels;
            marks{sessionNum}{epoch}{tet}.voltage_scaling = tmpData{tetind}.voltage_scaling;
            marks{sessionNum}{epoch}{tet}.threshold = tmpData{tetind}.threshold;
            marks{sessionNum}{epoch}{tet}.spike_invert = tmpData{tetind}.spike_invert;
            marks{sessionNum}{epoch}{tet}.reference = tmpData{tetind}.reference;
            try
                marks{sessionNum}{epoch}{tet}.reference_ntrode = tmpData{tetind}.referencentrode;
                marks{sessionNum}{epoch}{tet}.reference_channel = tmpData{tetind}.referencechannel;
            catch
                error('referencing is off!!')
                marks{sessionNum}{epoch}{tet}.reference_ntrode = [];
                marks{sessionNum}{epoch}{tet}.reference_channel = [];
            end
            marks{sessionNum}{epoch}{tet}.filter = tmpData{tetind}.filter;
            marks{sessionNum}{epoch}{tet}.lowpassfilter = tmpData{tetind}.lowpassfilter;
            marks{sessionNum}{epoch}{tet}.highpassfilter = tmpData{tetind}.highpassfilter;
            marks{sessionNum}{epoch}{tet}.clockrate = tmpData{tetind}.clockrate;
            marks{sessionNum}{epoch}{tet}.times = double(tmpData{tetind}.fields(1).data)/tmpData{tetind}.clockrate;
            % process marks data - only keep the max for each channel, not the full waveform. max is always aligned to clip sample 14
            %if there are none or only 1 marks, skip
            if ~isa(tmpData{tet}.fields(2).data,'int16') | size(tmpData{tet}.fields(2).data,1)<2
                disp(['no events detected for tet ' num2str(tet)])
                marks{sessionNum}{epoch}{tet}.marks = [];
                continue
            else
                try
                    goodchans = find(~ismember([1:4],tetinfo{sessionNum}{epoch}{tet}.deadchans));
                catch
                    goodchans = [1:4];
                end
                for c = 1:length(goodchans)
                    %stack marks data
                    %tmpmarks(:,c) = tmpData{tetind}.fields(goodchans(c)+1).data(:,14);
                    clips(:,:,c) = tmpData{tetind}.fields(goodchans(c)+1).data;
                end
                %identify the chan with the peak val;, use that timeslice for the other marks too.
                % 1. find the max val of the clip for each channel
                [clipmax,clipmaxind] = max(clips,[],2);
                clipmaxind = int16(squeeze(clipmaxind));
                % 2. Determine the maximum peak value across all chans and which channel it was on
                [chanmax,chanmaxind] = max(squeeze(clipmax),[],2);
                % 3. Set up the matrics that will be turned into the logical mask
                allchans = repmat([1:length(goodchans)],[length(chanmax),1]);
                % 4. apply logical mask of which chan had the max and get the clip ind of the max for that chan
                % 4b. flatten by summing across row (across chans) (0 12 0 0 -> 12)
                chanclipmax=sum(clipmaxind.*int16(allchans==repmat(chanmaxind,1,length(goodchans))),2); %logical mask for chans
                allclips=repmat([1:40],length(chanmax),1);
                % 5. make logical mask for clip ind. stack one for each chan and convert to int16
                cliplogical=int16(repmat(allclips==repmat(chanclipmax,[1,40]),[1,1,length(goodchans)]));
                % 6. appl logical mask for clipsample by multiplying by 0/1 (preserves shape)
                tmpmarks=clips.*(cliplogical);
                % 7. reduce across sample dimension
                tmpmarks = squeeze(sum(tmpmarks,2));
                
                marks{sessionNum}{epoch}{tet}.marks = int16(tmpmarks*tmpData{tetind}.voltage_scaling) ; %apply voltage scaling factor
                clear clips
            end
        end
    end
    if exist('marks') 
        save([destdir animalID,'marks',num2str(sprintf('%02d',sessionNum)),'.mat'],'marks');
    else
        disp(['no marks saved for session ',num2str(sessionNum)])
    end
    FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make marks files'])
    cd(destdir);
%% import marks from mountainsort mda    
elseif strcmp(marksource,'mountainsort')
    voltage_scale_factor = .195;  % from trodes acquisition system
    cd(srcdir)
    mountaindir = sprintf('%s_%s.mountain',datestring,animalID);
    try 
        disp('importing mountainsort marks; only written for multiepoch sort for now')
        cd(mountaindir)
    catch
        disp('cant find .mountain dir')
    end
    tic
    % get list of epochs from the prv json file (doesn.t matter if this ntrode has been sorted or not)
    tmp = dir('nt*');
    cd(tmp(1).name)
    js = loadjson('./raw.mda.prv');
    if ~isfield(js,'files')  % if files is not in the top level of the struct, it should be buried deeper
        js =js.processes{1}.inputs.timeseries;
    end
    epstrings = cellfun(@(x) strsplit(x.name, '_'), js.files, 'un', 0);
    epscell = cellfun(@(x) strsplit(x{3}, '.'), epstrings, 'un', 0);
    epochs = cell2mat(cellfun(@(x) str2num(x{1}), epscell, 'un', 0)');
% else %single ep; =
%     mountaindir = dir(sprintf('%s_%s_*.mountain',datestring,animalID));  % extract ep info from mountain dir names
%     epochs = arrayfun(@(x) x.name,mountaindir,'UniformOutput',false); %will only return the directories that are epoch numbers
%     epochs = regexp(epochs, '_','split');
%     epochs = cell2mat(cellfun(@(x) str2num(x{3}),epochs,'UniformOutput',0));
%     cd(strcat(srcdir, '/output'));
% end

%find the list of ntrodes
cd([srcdir mountaindir])
 if isempty(ntrodes) 
     ntrodedir = dir('*nt*');
     ntrodes = arrayfun(@(x) x.name,ntrodedir,'UniformOutput',0);
     ntrodes = regexp(ntrodes, 'nt','split');
     ntrodes = unique(cell2mat(cellfun(@(x) str2num(x{2}),ntrodes,'UniformOutput',0)));
 end

% Collect the adjtimestamps for each ep and their indices
% for multi epoch, indices are out of the full concatenated session. for single eps, they are just 1:length(ep)
for epoch = 1:length(epochs)
    mdadir = dir(sprintf('%s%s_%s_%02d*.mda',srcdir,datestring,animalID,epochs(epoch)));
    timestampsmda = sprintf('%s%s/%stimestamps.adj.mda',srcdir,mdadir(1).name,mdadir(1).name(1:end-3));
    real_timestamps{epochs(epoch)} = double(readmda(timestampsmda));
    %if firingsAcrossEpochs  %inds should be relative to all eps concatenated
        if epochs(epoch)==1
            real_timeinds{epochs(epoch)} = 1:length(real_timestamps{epochs(epoch)});
        else % end index of prev ep +1: endind of previous+1+length current -1
            startind = real_timeinds{epochs(epoch)-1}(end)+1;
            real_timeinds{epochs(epoch)} = startind:(startind+length(real_timestamps{epochs(epoch)})-1);
        end
    %else    % single eps, so inds are relative to each indiv ind
    %    real_timeinds{epochs(epoch)} = 1:length(real_timestamps{epochs(epoch)});
    %end
end 

% run mark conversion for each epoch and ntrode
for n = 1:length(ntrodes)
    resultspath = sprintf('%s%s/nt%d',srcdir,mountaindir,ntrodes(n));
    cd(resultspath)  % there should only be 1
    if exist('marks.mda','file')
        marksmda=readmda('marks.mda');
        marksmda= int16(squeeze(marksmda))';
        firings = readmda('firings_raw.mda');
        mark_timeinds = firings(2,:); %get timestamps from firings.mda (in samples out of whole day)
        for epoch = 1:length(epochs)
            marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.source = 'mountainsort';
            in_curr_epoch = ismember(mark_timeinds,real_timeinds{epochs(epoch)});
            mark_epochinds = mark_timeinds(in_curr_epoch);
            %Convert cluster spikes to within-epoch indices
            mark_epochinds = lookup(mark_epochinds,real_timeinds{epochs(epoch)});
            %Convert indices to times
            mark_times=real_timestamps{epochs(epoch)}(mark_epochinds)/30000; %in seconds
            % identify and remove any repeat timestamps; this can happen occaisionally as a result of cluster merging
            repeats = diff(mark_times)==0;
            marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.times=mark_times(~repeats)'; %Put times into data
            marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks = marksmda(in_curr_epoch,:);
            marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks = marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks(~repeats,:); % get rid of repeat marks
            % invert waveforms and apply voltage scaling to convert to uV
            marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks = -1*voltage_scale_factor*marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks;
            assert(length(marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.times)==size(marks{sessionNum}{epochs(epoch)}{ntrodes(n)}.marks,1)) 
        end
    else
        warning(sprintf('no marks.mda found for d %d nt %d ep %d!',sessionNum,ntrodes(n),epochs(epoch)))
    end  
end
    save(sprintf('%s%smarks%02d.mat',destdir,animalID,sessionNum),'marks')
    FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make marks files'])
    cd(destdir);
end

end



