function create_marks(animals, dates, days, ntrodes, pipeline, varargin)

%Demetris Roumis Nov 2017

%intended for use with clusterless decoding

useunits = 'tensofmsIntegers';%'samples'; %seconds
samprate = 3e4;
% mark_variables = {'time'};
if ~isempty(varargin)
    assign(varargin{:})
end

for ian = 1:length(animals);
    animal = animals{ian};
    animalinfo = animaldef(animal);
    preproc_filepath = animalinfo{5};
    
    
    for idate = 1:length(dates);
        date = dates(idate);
        clear adjustedtimestamps_cells
        day = days(idate);
        mdadirs = dir(sprintf('%s/%d/*.mda', preproc_filepath, dates(idate)));
        mdadirs = strsplit([mdadirs(:).name], '.mda')';
        disp('loading timestamps')
        for mdadir = 1:length(mdadirs)-1
            timestampfile = dir(sprintf('%s/%d/%s.mda/*.timestamps.adj.mda', preproc_filepath, date, mdadirs{mdadir}));
            adjustedtimestamps_cells{mdadir} = readmda(sprintf('%s/%d/%s.mda/%s', preproc_filepath, date, mdadirs{mdadir},timestampfile.name));
        end
        adjustedtimestamps = cat(2,adjustedtimestamps_cells{:})';
        debugepochboundaries = adjustedtimestamps(diff(adjustedtimestamps)>1)/30000/60;
        disp(['epoch gaps at minutes:' sprintf('%.1f  ', debugepochboundaries)])
        
        
        for introde = ntrodes;
            disp(sprintf('creating mark.mat files for %s %d %d pipe:%s',animal, day, introde, pipeline))
            mark_filepath = sprintf('/%d/%d_%s.mountain/output/%s--nt%d/', date, date, animal, pipeline, introde);
            mark_filename = sprintf('%s%s%s', preproc_filepath, mark_filepath, 'amplitudes_out.mda');
            spiketime_filename = sprintf('%s%s%s', preproc_filepath, mark_filepath, 'firings.mda');
            mrks = readmda(mark_filename)';
            % mrks = mrks.^2;
            spikeTimesInMSSamples = readmda(spiketime_filename)';
            % 2nd row: index into timestamps (1st row is channel) (3rd row is cluster
            % label. 1st row is primary channel.
            spikeTimesInMSSamples = spikeTimesInMSSamples(:,2);
            % times = spikeindices(:,2)/samprate; %in seconds
            spikeTimesInAdjSamples = adjustedtimestamps(spikeTimesInMSSamples);
            % timestamps = timestamps/samprate; %in seconds;
            switch useunits;
                case 'tensofmsIntegers'
%                     #ED's dataframe is expecting tens of ms.. so
                      % convert from 30kHz samples to seconds 
                    spikeTimes_seconds = spikeTimesInAdjSamples/samprate;
%                     then to tens of ms
                    spikeTimes = round(spikeTimes_seconds*1E4, 0);
%                     spiketimes = round(spikeTimes1,0);
                case 'seconds'
%                     disp('times in seconds')
                    spikeTimes = spikeTimesInAdjSamples/samprate;
                case 'milliseconds'
%                     disp('times in milliseconds')
                    spikeTimes = spikeTimesInAdjSamples/samprate*1E3;
                case 'samples'
%                     disp('times in 30kHz samples')
                    spikeTimes = spikeTimesInAdjSamples;
            end
                        
            marks = [spikeTimes mrks];
            %threshmarks = marks(threshmarkInds,:);
            
            num_channels = size(mrks, 2);
            mark_vars = arrayfun(@(x) sprintf('channel_%d_max', x),[1:num_channels], 'uni', 0)';
            mark_variables = cat(1, {'time'}, mark_vars);
            
            filedata.params = marks;
            filedata.paramnames = mark_variables;
            
            % save(sprintf('%s/EEG/%s%s%02d-%02d.mat', animalinfo{2}, animal, 'marks', day, ntrode), 'marks','-v7.3')
            save(sprintf('%s/EEG/%s%s%02d-%02d.mat', animalinfo{2}, animal, 'marks', day, introde), 'filedata')
        end
    end
end
