function stripDeadMDAchans(animalID, preprocdaydir, FFanimdir, sessionNum, date, varargin)
% uses tetinfo struct to identify dead chans;
% opens mda files for that day, removes dead chans, rewrites mda
% if 'keep_orig'=1, the full mda will be resaved with an orig_ prefix; the stripped one will take the normal file name (that MS looks for)

keep_orig=1;

if (~isempty(varargin))
    assign(varargin{:});
end

cd(preprocdaydir)
% identify tetrodes that have dead chans
tetinfo = loaddatastruct(FFanimdir,animalID,'tetinfo',sessionNum);
eptetlist = evaluatefilter(tetinfo{sessionNum},'~isempty($deadchans)') ;

for et = 1:size(eptetlist,1)
    
    deadchans = ismember([1:4],tetinfo{sessionNum}{eptetlist(et,1)}{eptetlist(et,2)}.deadchans);
    
    epdir = dir(sprintf('%d_%s_%02d*.mda',date,animalID,eptetlist(et,1)));

    tetfile = dir(sprintf('%s/%d_%s_%02d*.nt%d.mda',epdir.name, date,animalID,eptetlist(et,1),eptetlist(et,2)));
    tetmda = readmda(sprintf('%s/%s',epdir.name,tetfile.name));
    
    if ~isempty(dir(sprintf('%s/orig_%s',epdir.name,tetfile.name))) | size(tetmda,1)<4 
        fprintf('tetrode %d has already been stripped!\n',eptetlist(et,2))
    else
        disp(sprintf('stripping %d dead channels from day %d ep %d tet %d',sum(deadchans),sessionNum, eptetlist(et,1), eptetlist(et,2)))
        newtetmda = tetmda(~deadchans,:);
        
        %if specified, resave original mda with orig_ prefix
        if keep_orig
            disp('keeping original just in case')
            writemda(tetmda, sprintf('%s/orig_%s',epdir.name,tetfile.name),'int16');
        end
        % save over the original with the stripped version
        writemda(newtetmda, sprintf('%s/%s',epdir.name,tetfile.name),'int16');
    end
     
end



end