function Binarymda2FF_SPIKES(animalID, preprocdir, ffdir, sessionNum, datestring, varargin)

% Mari Sosa December 2016  AKG 2017  DKR 2017 Shijie 2020
% This function converts the firings_curated.mda output of MountainSort to SPIKES data in the Frank Lab Filter Framework format.
% example use: 
%   Binarymda2FF_SPIKES('/home/jojo/RemoteData2/shijie/animals/Jaq/mountainlab_output/',1,'20190826',{'mdakind','firings_burst_merged.mda'})
% The output cellinfo and spikes are located here if following the example use
%   '/home/jojo/RemoteData2/shijie/animals/Jaq/filterframework'

%opts: a cell of optional inputs in the form of: {'option', value, 'option',value}
%   some options are: 
%                -overwrite, bool, default=0, overwrite previous results or just append
%                -mdakind, str, default='firings_curated.mda', which
%                   firng mda to use to get spikes. backup is firings_raw.mda
%                -sorter = 'ms4' which assumes:
                    %   1) a directory of binary files at:
                    %  [<username>/<animalname>/mountainlab_output/datestring/<ntxx>/, for xx in 1:electrode number]  
                    %       This directory should contain (at minimum) the raw.mda.prv, params.json, firings_curated.mda, and metrics_curated.json
                    %       firings_curated gets translated into spikes datastruct and metrics gets stored in cellinfo struct
                    %       currently, curation is done automatically as part of the pipeline. Manual adjustments in mountainview will need
                    %       to be saved into the firings_curated.mda and curated metrics will need to be recalculated
                    %   2) json files of metrics for each epoch at
                    %  <username>/<animalname>/preprocessing/datestring/<date>_<animalname>.mountain/<tetrodedir>/ 
%              - ntrodes : if empty, import all. if string, gets passed to the tetinfo filter to select tets in a specific area
%              - metricsperepoch: if 1, expects metrics to be calculated on a per-epoch basis rather than having a single set of 
%                   metrics for the whole day 
%              -remove_noise_clusters: if 1, uses cluster tags to remove noise clusters from spikes struct. currently leaves clusterinfo in cellinfo struct

%       
% function call to mda2spikes: 
    % Requires supporting functions: readmda.m, varargin2struct.m, loadjson.m, jsonopt.m, mergestruct.m, struct2jdata.m
    % Outputs: A single tetrode's spikes, containing cells with structs for each cluster on the tetrode.
    %          Clusters remain assigned to their cluster ID# as output by mountainsort.
    % outputs will be stored in separated by epoch for ease of later filtering, altho the clustering may have been done across epochs.
   
%% overwrite default values
% VARARGIN -- process optional input in the form of: 'option', value, 'option', value
overwrite =0;
sorter='ms4';
ntrodes=[];
metricsperepoch = 1;
mdakind = 'firings_curated';
remove_noise_clusters = 1;
% process varargin if present and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end


%% Check for existing spikes
cd(ffdir);
if ~isempty(dir([animalID 'spikes' num2str(sprintf('%02d', sessionNum)) '.mat'])) && ~overwrite
    disp('spikes file detected... will overwrite if session exsited before and append if not')
    load([animalID 'spikes' num2str(sprintf('%02d', sessionNum)) '.mat']) 
elseif ~isempty(dir([animalID 'spikes' num2str(sprintf('%02d', sessionNum)) '.mat'])) && overwrite
    disp('spikes file detected... will overwrite')
else
    disp('no spikes file detected... proceeding to create one from scratch')
end

if strcmp(sorter,'ms4')
    mountaindir = sprintf('%smountainlab_output/%s/',ffdir(1:end-16),datestring);
else
    mountaindir = sprintf('%s%s/%s_%s.mountain',preprocdir,datestring,datestring,animalID);  % extract ep info from mountain dir names
end

cd(mountaindir)
%% Collect epochs to use
if strcmp(sorter,'ms4')
    cd(sprintf('%snt1/metrics/',mountaindir));   % currently assumes that you have an nt1 & that all nts are sorted on the same eps
    metrics_dir_1=dir('*nt*');
    epochs = arrayfun(@(x) x.name,metrics_dir_1,'UniformOutput',false); %will only return the directories that are epoch numbers
    epochs = regexp(epochs, '_','split');
    epochs = cell2mat(cellfun(@(x) str2num(x{4}(6:end-5)),epochs,'UniformOutput',0));

else
    cd(mountaindir)
    % get list of epochs from the prv json file (doesn.t matter if this ntrode has been sorted or not)
    tmp = dir('nt*');
    cd(tmp(1).name)
    js = loadjson('./raw.mda.prv');
    if ~isfield(js,'files')  % if files is not in the top level of the struct, it should be buried deeper
        js =js.processes{1}.inputs.timeseries;
    end
    epstrings = cellfun(@(x) strsplit(x.name, '_'), js.files, 'un', 0);
    epscell = cellfun(@(x) strsplit(x{3}, '.'), epstrings, 'un', 0);
    epochs = cell2mat(cellfun(@(x) str2num(x{1}), epscell, 'un', 0)');
end

%% collect ntrodes to use

if isempty(ntrodes)
    ntrodedir = dir('*nt*');
    ntrodes = arrayfun(@(x) x.name,ntrodedir,'UniformOutput',0);
    ntrodes = unique(cell2mat(cellfun(@(x) str2double(regexp(x, '\d*','match')),ntrodes,'UniformOutput',0)));
    
elseif isstr(ntrodes)
    load([ffdir animalID 'tetinfo.mat']) % load tetinfo for ca1 label and deadchan specification
    disp(sprintf('collecting all %s ntrodes',ntrodes))
    ntrodes = evaluatefilter(tetinfo{sessionNum}{epochs(1)},ntrodes);
end
 
%% Collect the adjtimestamps for each ep and their indices
% for multi epoch, indices are out of the full concatenated session. for single eps, they are just 1:length(ep)
cd(preprocdir)
for epoch = 1:length(epochs)
    mdadir = dir(fullfile(preprocdir,sprintf('%s_%s_%02d*.mda',datestring,animalID,epochs(epoch))));
    timestampsmda=dir(fullfile(mdadir.name, '*timestamps.adj.mda'));
    timestampspath = [preprocdir, mdadir.name,'/' timestampsmda.name];
    real_timestamps{epochs(epoch)} = double(readmda(timestampspath));
    %inds should be relative to all eps concatenated
    if epochs(epoch)==1
        real_timeinds{epochs(epoch)} = 1:length(real_timestamps{epochs(epoch)});
    else % end index of prev ep +1: endind of previous+1+length current -1
        startind = real_timeinds{epochs(epoch)-1}(end)+1;
        real_timeinds{epochs(epoch)} = startind:(startind+length(real_timestamps{epochs(epoch)})-1);
    end
end 
%%
disp('Processing spikes...');
% run spike conversion for each epoch and ntrode
for n = 1:length(ntrodes)
    
    % get 1)paramsjson, 2)firingsmda
    paramsjson = sprintf('%s/nt%d/params.json',mountaindir,ntrodes(n));
    firingsmda = sprintf('%s/nt%d/%s.mda',mountaindir,ntrodes(n),mdakind);
    firingsmda_backup = sprintf('%s/nt%d/firings_raw.mda',mountaindir,ntrodes(n));
    if exist(firingsmda,'file')
        fprintf('using curated firing for day %d nt %d \n',sessionNum,ntrodes(n))
    elseif exist(firingsmda_backup,'file')
        fprintf('no curation found; USING RAW FIRING for %d nt %d \n',sessionNum,ntrodes(n))
        firingsmda = firingsmda_backup;
    else
        warning(sprintf('no firings_curated or firings.mda found for d %d nt %d!',sessionNum,ntrodes(n)))
    end
    if ~metricsperepoch
        metricspath = sprintf('%snt%d/metrics_processed.json',mountaindir,ntrodes(n));
    end
    
    for epoch = 1:length(epochs)
        
        % 3)get resultspath, which is actually metrics per epoch.json file      
        if metricsperepoch
            metricspath = sprintf('%snt%d/metrics/metrics_curated_nt%02d_epoch%d.json',mountaindir,ntrodes(n),ntrodes(n),epoch);
        end
        %cd(resultspath)       
        spikes{sessionNum}{epochs(epoch)}{ntrodes(n)} = mda2spikes(firingsmda,paramsjson,real_timestamps,real_timeinds,epochs(epoch));
        
        if ~isempty(spikes{sessionNum}{epochs(epoch)}{ntrodes(n)})
            notempty = ~cellfun(@isempty,spikes{sessionNum}{epochs(epoch)}{ntrodes(n)});
            numspikes = cellfun(@(x) length(x.data),spikes{sessionNum}{epochs(epoch)}{ntrodes(n)}(notempty), 'UniformOutput', false);
            epochlength = cellfun(@(x) diff(x.timerange),spikes{sessionNum}{epochs(epoch)}{ntrodes(n)}(notempty), 'UniformOutput', false);
            clusterlabels = cellfun(@(x) x.label,spikes{sessionNum}{epochs(epoch)}{ntrodes(n)}(notempty));
            
            % save metrics to cellinfo struct - if multi, just gets repeated across epochs
            cellinfo = clusterMetricsToCellinfo(ffdir, animalID, sessionNum, epochs(epoch),ntrodes(n), metricspath,numspikes, clusterlabels, epochlength);
        
            % remove clusters that are labeled noise from spikes (leave in cellinfo?)
            if remove_noise_clusters
                notempty = ~cellfun(@isempty ,cellinfo{sessionNum}{epochs(epoch)}{ntrodes(n)});
                untagged = zeros(1,length(notempty));              
                untagged(notempty) = cellfun(@(x) isempty(x.tags),cellinfo{sessionNum}{epochs(epoch)}{ntrodes(n)}(notempty));
                noisetags = zeros(length(notempty),1);
                noisetags(notempty & ~untagged) = cellfun(@(x) strcmp(x.tags,'noise'),cellinfo{sessionNum}{epochs(epoch)}{ntrodes(n)}(notempty & ~untagged));
                noiseclusters = find(noisetags);
                for nc = 1:length(noiseclusters)
                    spikes{sessionNum}{epochs(epoch)}{ntrodes(n)}{noiseclusters(nc)} = [];
                end
            end
        else
            disp(sprintf('no clusters to add to cellinfo for ep %d',epochs(epoch)))
        end  
    end
   
end
save(sprintf('%s%sspikes%02d.mat',ffdir,animalID,sessionNum),'spikes','-v7.3')
disp(sprintf('%sspikes%02d.mat saved',animalID,sessionNum))
end