% version 2 removes all periods of bad data, even if they arent common to a majority of channels

animal = 'test';
days = [4];   %25 done
%days = [15:31];
cd('/media/anna/whirlwindtemp2/test/filterframework')

for d = days
    load(sprintf('%sDIO%02d.mat',animal,d))  
    disp(['-----------fixing day ' num2str(d) '--------'])
    for e = 1:length(DIO{d})
        figure
        
        % run through all chans to identify any periods of bad data (intervals <1ms)
        for c = 1:length(DIO{d}{e})
           
            diodiff{c} = diff(DIO{d}{e}{c}.times)<.0005; % generally have an interval btwn .4-.05ms. anything <1ms shouldnt be real
            burststarts{c} = DIO{d}{e}{c}.times(1+find(diff(diodiff{c})==1));
            burstends{c} = DIO{d}{e}{c}.times(1+find(diff(diodiff{c})==-1));
            if ~isempty(diodiff{c})
                if diodiff{c}(end)==1
                    burstends{c} = [burstends{c};DIO{d}{e}{c}.times(end)];
                end
                if diodiff{c}(1)==1
                    burststarts{c} = [DIO{d}{e}{c}.times(1);burststarts{c}];
                end
            end
            subplot(2,1,1)
            hold on
            plot(DIO{d}{e}{c}.times, c+DIO{d}{e}{c}.values)
            plot(burststarts{c},c*ones(1,length(burststarts{c})),'k.')
            plot(burstends{c},c*ones(1,length(burstends{c})),'k.')
        end
        axis tight
        title(sprintf('session%d ep%d',d,e))
        allstarts = cell2mat(burststarts');
        allends = cell2mat(burstends');
        
        
        for c = 1:length(DIO{d}{e})
            if length(DIO{d}{e}{c}.times)>5 && isExcluded(1/mode(diff(DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==1))),[28, 32]);
                disp('this is a camsync! reconstructing lost syncs')
                ups = DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==1);
                downs = DIO{d}{e}{c}.times(DIO{d}{e}{c}.values==0);
                if ~isempty(burststarts{c})   %deal with flicker noise
                    for event = 1:length(burststarts{c})
                        startup = lookup(burststarts{c}(event)-.000001,ups,-1); % identify closest smaller-than up timestamp; .000001 ensures that it must be <, not =
                        startup = lookup(ups(startup),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                        endup = lookup(burstends{c}(event)+.00001,ups,1);  %closest larger-than timestamp in ups; .00001 ensures that it must be >, not =
                        endup = lookup(ups(endup),DIO{d}{e}{c}.times); %translate back to full times inds
                        lostups = round((DIO{d}{e}{c}.times(endup)-DIO{d}{e}{c}.times(startup))/mode(diff(ups)))-1;  %should be close to a multiple of Fs
                        
                        startdown = lookup(burststarts{c}(event)-.000001,downs,-1); % identify closest smaller-than down timestamp
                        startdown = lookup(downs(startdown),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                        enddown = lookup(burstends{c}(event)+.000001,downs,1);  %closest larger-than timestamp in downs
                        enddown = lookup(downs(enddown),DIO{d}{e}{c}.times); %translate back to full times inds
                        lostdowns = round((DIO{d}{e}{c}.times(enddown)-DIO{d}{e}{c}.times(startdown))/mode(diff(downs)))-1;
                        
                        %nan out the intervening garbage
                        DIO{d}{e}{c}.times(max([startup startdown])+1:min([endup enddown])-1) = NaN;
                        DIO{d}{e}{c}.values(max([startup startdown])+1:min([endup enddown])-1) = 5; %uint8 cant represent nan, so use 5 as marker
                        
                        %interp to replace any lost ups or downs
                        if lostups>=1                             
                            replaceind = floor(linspace(startup,endup,lostups+2));
                            DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startup endup], [DIO{d}{e}{c}.times(startup),DIO{d}{e}{c}.times(endup)],replaceind(2:end-1));
                            DIO{d}{e}{c}.values(replaceind(2:end-1)) = 1;
                        end
                        if lostdowns>=1
                            replaceind = floor(linspace(startdown,enddown,lostdowns+2));
                            DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startdown enddown], [DIO{d}{e}{c}.times(startdown),DIO{d}{e}{c}.times(enddown)],replaceind(2:end-1));
                            DIO{d}{e}{c}.values(replaceind(2:end-1)) = 0;
                        end
                    end
                end
                % deal with any repeating dios (constnatly up)  and rebuild
                reps = (diff(double(DIO{d}{e}{c}.values))==0 & ~isnan(DIO{d}{e}{c}.times(2:end))); 
                repstarts = DIO{d}{e}{c}.times(1+find(diff(reps)==1));
                repends = DIO{d}{e}{c}.times(1+find(diff(reps)==-1));
                    
                if ~isempty(repstarts)
                        subplot(2,1,1)
                        plot(repstarts,c*ones(1,length(repstarts)),'b.')
                        disp(sprintf('s%de%d: %d bursts of repeats, %d total',d,e,length(repstarts),sum(reps)))                        
                        for event = 1:length(repstarts)
                            startup = lookup(repstarts(event)-.000001,ups,-1); % identify closest smaller-than up timestamp; .000001 ensures that it must be <, not =
                            startup = lookup(ups(startup),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                            endup = lookup(repends(event)+.00001,ups,1);  %closest larger-than timestamp in ups; .00001 ensures that it must be >, not =
                            endup = lookup(ups(endup),DIO{d}{e}{c}.times); %translate back to full times inds
                            lostups = round((DIO{d}{e}{c}.times(endup)-DIO{d}{e}{c}.times(startup))/mode(diff(ups)))-1;  %should be close to a multiple of Fs
                        
                            startdown = lookup(repstarts(event)-.000001,downs,-1); % identify closest smaller-than down timestamp
                            startdown = lookup(downs(startdown),DIO{d}{e}{c}.times); %translate back to ind relative to full times vector
                            enddown = lookup(repends(event)+.000001,downs,1);  %closest larger-than timestamp in downs
                            enddown = lookup(downs(enddown),DIO{d}{e}{c}.times); %translate back to full times inds
                            lostdowns = round((DIO{d}{e}{c}.times(enddown)-DIO{d}{e}{c}.times(startdown))/mode(diff(downs)))-1;
                        
                            %nan out the intervening garbage
                            DIO{d}{e}{c}.times(max([startup startdown])+1:min([endup enddown])-1) = NaN;
                            DIO{d}{e}{c}.values(max([startup startdown])+1:min([endup enddown])-1) = 5; %uint8 cant represent nan, so use 5 as marker
                        
                            %interp to replace any lost ups or downs
                            if lostups>=1
                                replaceind = floor(linspace(startup,endup,lostups+2));
                                DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startup endup], [DIO{d}{e}{c}.times(startup),DIO{d}{e}{c}.times(endup)],replaceind(2:end-1));
                                DIO{d}{e}{c}.values(replaceind(2:end-1)) = 1;
                            end
                            if lostdowns>=1
                                replaceind = floor(linspace(startdown,enddown,lostdowns+2));
                                DIO{d}{e}{c}.times(replaceind(2:end-1)) = interp1([startdown enddown], [DIO{d}{e}{c}.times(startdown),DIO{d}{e}{c}.times(enddown)],replaceind(2:end-1));
                                DIO{d}{e}{c}.values(replaceind(2:end-1)) = 0;
                            end
                        end  
                end
                %get rid of nans and 5's
                DIO{d}{e}{c}.times = DIO{d}{e}{c}.times(~isnan(DIO{d}{e}{c}.times));
                DIO{d}{e}{c}.values = DIO{d}{e}{c}.values(DIO{d}{e}{c}.values~=5);  
                
                if abs(length(DIO{d}{e}{c}.values==1)-length(DIO{d}{e}{c}.values==1))>5
                    error('mismatched dio lengths')
                end
                %if (d==14 & e==4)  %reconstruct the big weird pag in the beginning
                    
            else % this is not a camsync, so we have no way of recoverning any real triggers during the corrupted times :(
                    if ~isempty(burststarts{c}) 
                        invalid = isExcluded(DIO{d}{e}{c}.times,[burststarts{c}, burstends{c}]);
                        DIO{d}{e}{c}.times = DIO{d}{e}{c}.times(~invalid);
                        DIO{d}{e}{c}.values = DIO{d}{e}{c}.values(~invalid);
                    end 
            end
            subplot(2,1,2)
            hold on
            plot(DIO{d}{e}{c}.times, c+DIO{d}{e}{c}.values)
            axis tight
        end
    end
   save(sprintf('%sDIO%02d.mat',animal,d),'DIO') 
   disp(['day ' num2str(d) ' DIOs saved'])
end

        