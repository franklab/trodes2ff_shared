function Binary2FF_POS(animalID, srcdir, destdir, sessionNum, date, varargin)

%Demetris Roumis July 2016
%This function saves RAWPOS and POS data in the Filter Framework v1 format.
%It is assumed that there is a directory of binary files in the form of:
%  <username>/<animalname>/extracted/<date (%YYYY%MM%DD)>/<extracted binary files>

%animalID -- a string identifying the animal's id (appended to the beginning of the files).. ex: 'D10'
%sessionNum -- day.. ex: '4'
%srcdir -- preprocessed binary directory.. ex: /mnt/data19/droumis/D10/preprocessed/20160512/
%destdir -- the directory where the processed files should be saved for the animal...
%ex: /mnt/data19/droumis/D10/filterframework/

%OPTIONS -- optional input in the form of: 'option', value, 'option', value
%           optional arguments are:
%           DIODEPOS -- 0 uses back diode for pos, 1 uses front diode for
%               pos, and values in between use the proportionate distance
%               between the two diodes. (default 0.5, ie, average of diodes)
%           CMPERPIX -- size of each pixel in centimeters (must be specified)
%           POSFILT -- filter to use for pos smoothing when computing
%               velocity (default gaussian(30*0.5, 60))
%           REVERSEX -- mirror image pos info along x axis (default 0)
%           REVERSEY -- mirror image pos info along y axis (default 0)
%           SOURCE -- which pos source to use? can be
%                        'online','manual1',etc.  default online

% set default values for the optional arguments for POS processing
diodepos = 0.5; %centroid
diodeweights = [.5 .5];
cmperpix = NaN;
posignoretimesidx = [];
%posfilt = gaussian(30*0.5, 60);  %from nspike old cameras 
posfilt = gaussian(2, 15); %for simple vel calculation; half second gaussian. improved for trodes
%reversex = 0;   % can pass into posinterp to reverse x or y NOT IMPLEMENTED FOR TRODES
%reversey = 0;
maxdevpoints = 30;
maxv = 300;
posSource = 'online';
posSource_alternate = 'offline';
useNearestNeighbor = 0; %DR pos realign
fit_offset_override = 0;
% process varargin and overwrite default values
if (~isempty(varargin))
    assign(varargin{:});
end

%% Look for existing rawpos and pos
cd(destdir);
if ~isempty(dir([animalID 'rawpos' num2str(sprintf('%02d', sessionNum)) '.mat']))
    disp('rawpos file detected.. will try to append to existing structure')
    load([animalID 'rawpos' num2str(sprintf('%02d', sessionNum)) '.mat']) %loads as pos var
else
    disp('no rawpos file detected.. proceeding to create one from scratch')
end
if ~isempty(dir([animalID 'pos' num2str(sprintf('%02d', sessionNum)) '.mat']))
    disp('pos file detected.. will try to append to existing structure')
    load([animalID 'pos' num2str(sprintf('%02d', sessionNum)) '.mat']) %loads as pos var
else
    disp('no pos file detected.. proceeding to create one from scratch')
end
%% Create rawpos file
cd(srcdir);
rawposepochDirs = dir('*.pos');
rawposepochDirs = arrayfun(@(x) x.name,rawposepochDirs,'UniformOutput', false);
rawposepochDirs = sort(rawposepochDirs); %this should be safe if the file naming is consistent with '<date>_<animal>_<2digitepoch>.pos'
disp(sprintf('Processing %d binary pos files...', length(rawposepochDirs)));
tic
epoch_IDs = getepochIDs(rawposepochDirs);

n_epochs = numel(rawposepochDirs);

% initialize ignore time list 
if numel(posignoretimesidx)< n_epochs
    posignoretimesidx{max(epoch_IDs)} = [];
end

for iep = 1:n_epochs
    epoch = epoch_IDs(iep);
    cd(srcdir); %necessary for every loop after the first
    cd(rawposepochDirs{iep}); %go into current epoch pos dir
    posString = sprintf('*.pos_%s.dat',posSource);
    rawposntrodefiles = dir(posString);
    % make the online vs offline posSource a priority rather than choice
    if isempty(rawposntrodefiles)
        disp(sprintf('couldn''t find %s pos tracking, taking a look for %s instead', posSource, posSource_alternate))
        posString = sprintf('*.pos_%s.dat',posSource_alternate);
        rawposntrodefiles = dir(posString);
        tmpData = [];
        tmpData = readTrodesExtractedDataFile(rawposntrodefiles.name);
%     
%     if isempty(rawposntrodefiles) %if there's no position tracking file with the posSource suffix, look in the raw folder
%         animalinfo = animaldef(animalID);
%         rawdir = animalinfo{4};
%         disp(sprintf('couldn''t find %s pos tracking in preprocessing folder... taking a look in the raw dir instead', posSource))
%         rawposDateDir = sprintf('%s%d/', rawdir, date);
%         posString = sprintf('%s%d_%s_%02d*.pos_%s*', rawposDateDir, date, animalID, epoch, posSource);
%         rawposntrodefiles = dir(posString);
%         if isempty(rawposntrodefiles)
%             disp(sprintf(':''( couldn''t find %s pos tracking in the raw dir either...', posSource))
%             return
%         else
%             preproccopy = 'y';
% %             preproccopy = input(sprintf('raw success!.. Try to put a copy of %s in the preprocessed folder (y/n)?', rawposntrodefiles.name),'s');
%             %this got annoying real fast.. default is now to make a copy
%             if strcmp(preproccopy,'y')
%                 system(sprintf('scp %s%s %s%s/%s',rawposDateDir, rawposntrodefiles.name, srcdir,rawposepochDirs{iep}, rawposntrodefiles.name));
%             end
%         tmpData = [];
%         tmpData = readTrodesExtractedDataFile(sprintf('%s%s',rawposDateDir, rawposntrodefiles.name));    
%         end
%         
    else
        tmpData = [];
        tmpData = readTrodesExtractedDataFile(rawposntrodefiles.name);
    end
   
    % TODO:: add fisheye distortion correction to pos tracking
    
    
    % combine the timestamps x1 y1 x2 y2 into a double array, in this order
    fieldsstruct = arrayfun(@(x) x.name,tmpData.fields,'UniformOutput', false); %get order of datatypes in array
    fieldslength = arrayfun(@(x) length(x.data), tmpData.fields);
    endind = min(fieldslength);  %  if not all the fields have the same length, cut off the extra entry (ie if write operation got cutoff within a line) 
    postrackingtimestamps = (double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'time'))).data(1:endind)))./tmpData.clockrate; %put timestamps into seconds
    x1posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'xloc'))).data(1:endind));
    y1posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'yloc'))).data(1:endind));
    x2posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'xloc2'))).data(1:endind));
    y2posData = double(tmpData.fields(find(strcmp([fieldsstruct(:)], 'yloc2'))).data(1:endind));
    rawposDatatmp = [x1posData y1posData x2posData y2posData]; %collect the pos tracking data

    %sanity check: plot
%     figure 
%     plot(x1posData,y1posData,'g')
%     hold on
%     plot(x2posData,y2posData,'r')
%     title(sprintf('Sanity Check day%d ep%d green-frontLED red-backLED', sessionNum, epoch))
    
    %collect all our timestamp data sources
    %cd(srcdir);
    %cd(rawposepochDirs{epoch});
    if isempty(dir('*.pos_timestamps.dat*')) && isempty(dir('*.pos_timestamps.adj*'))
        disp('no pos timestamps files detected, press any key to continue with position tracking timestamps or ctrl-c to abort');
        pause
        postimestamps = postrackingtimestamps;
    else
        if ~isempty(dir('*.pos_timestamps.dat*'));
            postimefile = dir('*.pos_timestamps.dat');
            postimestampsUNadj = readCameraModuleTimeStamps(postimefile.name);
        else
            disp('UNadj timestamps file not detected, press any key to continue with position tracking timestamps or ctrl-c to abort');
            pause
            postimestamps = postrackingtimestamps;
        end
        if ~isempty(dir('*.pos_timestamps.adj*'));
            postimefileADJUSTED = dir('*.pos_timestamps.adj*'); %there can be only one
            postimestamps = readCameraModuleTimeStamps(postimefileADJUSTED.name);
        else
            disp('ADJ timestamps file not detected, press any key to continue with unadjusted timestamps or ctrl-c to abort');
            pause
            postimestamps = postimestampsUNadj;
        end
    end
    
    % realign timestamps based on camlogs, if available
    % newTimesInds reflects which of the original times should be used (some might have been excluded for repeats or for length mismatch with dios)
    disp('using ''pos_realign_camlog'' method of realignment')
    [newTimes,newTimesInds] = pos_realign_camlog(srcdir, destdir, animalID, sessionNum,...
        epoch, rawposepochDirs{iep}, postimestamps,[],fit_offset_override, posignoretimesidx{epoch});
    
    % update newTimesInds if tracking is missing some timestamps
    if length(postimestamps)>length(postrackingtimestamps)
           disp(sprintf('missing %d samples from tracking',length(postimestamps)-length(postrackingtimestamps)))
           [~,validtrackinginds,validadjinds] = intersect(postrackingtimestamps, postimestampsUNadj(newTimesInds)); %find which valid timestamps were tracked
           newTimes = newTimes(validadjinds);
           rawposDatatmp = rawposDatatmp(validtrackinginds,:); %get valid adj timestamps
           rawposDatatmp = [newTimes rawposDatatmp]; %add corrected timestamps to pos tracking data
    else    
        rawposDatatmp = rawposDatatmp(newTimesInds,:); % plase pos tracking data into new struct based on valid inds
        rawposDatatmp = [newTimes rawposDatatmp]; %add timestamps to pos tracking data
    end

    rawpos{sessionNum}{epoch}.data = rawposDatatmp; 
    rawpos{sessionNum}{epoch}.fields = ['timestamp x1 y1 x2 y2'];
    rawpos{sessionNum}{epoch}.descript = { };
    rawpos{sessionNum}{epoch}.threshold = tmpData.threshold;
    rawpos{sessionNum}{epoch}.dark = tmpData.dark;
    rawpos{sessionNum}{epoch}.clockrate = tmpData.clockrate;
    clear rawposDatatmp
    % -------------create pos files--------------------------------
    if isfinite(cmperpix) & (length(cmperpix) < n_epochs) %if all epochs don't have their own cmperpix
        cmperpix = repmat(cmperpix(1),1,length(rawposepochDirs)); %if only 1 value for cmperpix, copy to length of epochs
    end    
    if ~isfinite(cmperpix)
        error('cmperpix must be specified.. cmperpixer.m can help');
    end
        
    if ~isempty(rawpos{sessionNum}{epoch}.data)
        disp('smoothing pos, calculating velocity etc...')
        % convert to cm, interpolate over toofast points, calculate centerpoint and get head dir
        postmp = rawpos{sessionNum}{epoch};
        postmp.data= [postmp.data(:,1), postmp.data(:,2:5)*cmperpix(iep)];
        pos{sessionNum}{epoch} = posinterp(postmp, 'diode', diodepos,...
            'maxv', 300, 'maxdevpoints', 30);
        pos{sessionNum}{epoch}.cmperpixel = cmperpix(iep);
        % smooth interped position, calculate and save velocity to the pos struct:: pos = [time x y dir vel]
        pos{sessionNum}{epoch} = addvelocity(pos{sessionNum}{epoch}, posfilt); 
        % Loess smoothing of position and velocity calculation:: smoothpos = [time X-Loess Y-Loess Dir-Loess Vel-Loess xvel yvel xdiff ydiff]
        Loesssmoothpos = sj_estimate_position(rawpos{sessionNum}{epoch},'centimeters_per_pixel',cmperpix(iep),'front_back_marker_weights',diodeweights); 
        mappostimeind = lookup(Loesssmoothpos{1}.data(:,1),pos{sessionNum}{epoch}.data(:,1)); %map pos times to Loess pos indicies..aka find closest time
        pos{sessionNum}{epoch}.data(mappostimeind,[1 6 7 8 9]) = Loesssmoothpos{1}.data(mappostimeind,[1 2 3 4 5]);
        % FINAL output: pos = [time x y dir vel-Gauss X-Loess Y-Loess Dir-Loess Vel-Gauss-Loess]
        %------------------------%
        %DR - FYI::: SJ didn't use  smoothed vel from estimate_position.m (Loess) ; instead used sj_addnfiltvelocity (Gaussian with chopped tail)
        %pos{sessionNum}{epoch}.data(mappostimeind,9) = sj_addnfiltvelocity(Loesssmoothpos{1}.data(:,2:3), Loesssmoothpos{1}.data(:,1)); %gaussian filter       
        %AKG - actually it's barely different at all, and not worth the extra step
        %------------------------%
       pos{sessionNum}{epoch}.fields = 'time x-interp y-interp dir-interp vel-gauss x-loess y-loess dir-loess vel-loess'; 
        
    else
        disp(sprintf('pos and rawpos not saved for Day%d epoch%d', sessionNum, epoch))
        pos{sessionNum}{epoch}.data = [];
        pos{sessionNum}{epoch}.cmperpixel = cmperpix(iep);
    end
end
save([destdir animalID,'rawpos',num2str(sprintf('%02d',sessionNum)),'.mat'],'-v6','rawpos'); %one rawpos file per day
save([destdir animalID,'pos',num2str(sprintf('%02d',sessionNum)),'.mat'],'-v6','pos'); %one pos file per day
FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make POS files'])

end

function epoch_IDs = getepochIDs(epochDirs)
epoch_IDs = [];
for iep = 1:length(epochDirs)
    iepochID = strsplit(epochDirs{iep}, '_');
    iepochID = str2num(iepochID{3}(1:2)); %strip off epoch ID num
    epoch_IDs(iep,1) = iepochID;
end
end
