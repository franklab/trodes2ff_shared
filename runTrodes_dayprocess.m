

%Make your own copy of this script in your own directory, set your params, then run the script

animal = ''; %e.g. 'D10'
dates = []; % 8 digit date(s) YYYYMMDD e.g. [20160512, 20160513]
sessionNums = []; % e.g. [1, 2] eventually just grab this from the task info struct or comments files
adjusttimestamps = 0; %Run this once before processing anything else..
makeFFLFP = 0;
makereferencedFFLFP = 0; useconfig = 1;  % get ref info from config (currently isn't saved in extracted data)
makeFFDIO = 0;
makeFFPOS = 0;
makeFFLINPOS = 0;
makeFFspikes = 0;
%    % for spike only:
%    mda_kind='firings_burst_merged.mda'; % which mda to use to get spikes
%    mountainlab_output_folder=sprintf('/home/jojo/RemoteData2/shijie/animals/%s/mountainlab_output/',animal);
updatetaskstruct = 0; %create and/or update taskinfo
updatetetcellstructs = 0; % create and/or update tetinfo and cellinfo structs
makeRippleFilteredLFP = 0;
extractRipples = 0; %old method ripple detection
extractRipplesKonsensus = 0; % new consensus method..KK has other options that alter smooth,square,mean order... see KK's extraction.m
% ___________________ tmpPARAMS ___________________
cmperpix = []; % e.g. [0.1] eventually just grab this from .trodescomments
posSource = '';
taskcoordinateprogram = 'sj_getcoord_wtrack'; %'sj_getcoord_wtrack' for W track..'doc' getcoord_wtrack for click order
dayepochlist = []; %[day epoch; day epoch;...] e.g. [4 2] .. eventually just grab this from the .trodescomments
epochs2tag = []; %e.g. [6 8]
epochtags = {''};% {property1, value1, property2, value2, …} e.g. {'environment', 'wtrack', 'type', 'run'} or {'environment', 'openfield', 'type', 'run'};
tetsareas = []; % [ {area1} {[tetrodes]} ; {area2} {tetrodes} ... ] e.g. [{'ca1'} {[1, 16:30]} ; {'mec'} {[3:8 11:14]} ; {'por'} {[10 15]} ; {'v2l'} {[9]}; {'ref'} {[2 18]}];
RIPmindur = 0.015; % time (in seconds) which the signal must remain above threshold to be counted as as ripple (start with 0.015)
RIPnstd = 2; % > # std ripples (start with 2)
OLDRIPtetrodes = []; % -1 to specify all tetrodes. e.g. [1 16 17 19:30] .. eventually grab this from tetinfo struct based on area
consensusRIParea = ''; %define source region of ripples.. saved structure will contain this string e.g. 'por'
% ___________________ Debugging ___________________
checkforcorrupteddata = 1;
checknumCommentfiles = 0; %not sure if this is necessary anymore.. not using comments files for anything..yet

%% run the vars above.. loop over dates/sessionNums if multiple
tic
for i = 1:length(dates);
    date = dates(i);
    sessionNum = sessionNums(i);
    %run dayprocess
    Trodes_dayprocess(animal, date, sessionNum, 'adjusttimestamps',adjusttimestamps, 'makeFFLFP', makeFFLFP,...
        'makereferencedFFLFP',makereferencedFFLFP, 'useconfig', useconfig, 'makeFFDIO', makeFFDIO, 'makeFFPOS', makeFFPOS, 'makeFFLINPOS', makeFFLINPOS, ...
        'makeFFspikes', makeFFspikes, 'mda_kind',mda_kind,'mountainlab_output_folder',mountainlab_output_folder,...
        'updatetaskstruct', updatetaskstruct, 'updatetetcellstructs', updatetetcellstructs, ...
        'makeRippleFilteredLFP', makeRippleFilteredLFP, 'extractRipples', extractRipples, 'extractRipplesKonsensus', extractRipplesKonsensus, ...
        'cmperpix', cmperpix, 'posSource', posSource, 'taskcoordinateprogram', taskcoordinateprogram, 'dayepochlist', dayepochlist, 'epochs2tag', epochs2tag, ...
        'epochtags', epochtags, 'tetsareas', tetsareas, 'RIPmindur', RIPmindur, 'RIPnstd', RIPnstd, 'OLDRIPtetrodes', OLDRIPtetrodes, ...
        'consensusRIParea', consensusRIParea, 'checkforcorrupteddata', checkforcorrupteddata, 'checknumCommentfiles', checknumCommentfiles);
end
timerTrodes_dayprocess = toc;
disp(sprintf('nextcoffeebreak = %0.0f seconds',timerTrodes_dayprocess));    %get this to email or text me when finished