function extractNoiseConsensus(animaldir, animalprefix, eventname, noiseeventeegname, ripeventeegname, days, tetfilters, min_suprathresh_duration, nstd, varargin)

% extractEventConsensus(FFanimdir, animalID, sprintf('%sripples',consensusRIParea{riparea}), 'ripple',sessionNum,conRIPtetfilters,RIPmindur,RIPnstd)

%function extractconsensus(directoryname, fileprefix, eventname, day, tetrode, min_suprathresh_duration, nstd, varargin)

%       Whereas extractripples + getriptimes extracts events from single tetrodes
%       and then subsequently constructs nrip vector, extractconsensus combines
%       event-band power across all valid detecting tetrodes then detects threshold SD events.

%       Follows Csicsvari--Buzsaki-1999 except instead of summing squared amplitudes
%       in "1.6 ms" windows, here keeps the original sampling rate (1500 Hz) and instead
%       Gaussian kernel smooths the combined power

% Aftewards, saves <animalprefix><event>con<day>.mat files in
% animdirect under {day}{epoch}

%
%fileprefix	- folder name where the day's data is stored
%
%day		- the day to process
%
%min_suprathresh_duration
%		- the time (in seconds) which the signal
%       must remain above threshold to be counted as as ripple; this guards
%       against short spikes in signal (e.g. noise) as being counted as
%       ripples. Set min_suprathreshold_duration to some small value, like
%       0.015 s.
%
%nstd		- the number of standard dev that ripple must be from mean to
%			be detextractconsensusected. Start with 2.
%
%options	'stdev', stdev   sets the size of the standard deviation used to
%				allow direct comparison across epochs
%       	'baseline', b   sets the size of the baseline used to
%				allow direct comparison across epochs
%           'maxpeakval, m	- ripples with maximal peaks above this value
%				are exluded.  Use this avoid detecting noise
%				events. Default 1000
%           'samethreshperday' - 0 or 1, default 0,
%               0 calculates baseline and threshold per epoch for each
%               tetrode
%               1 uses same threshold for all sessions on the same day on
%               the same tetrode.  Calculates this threshold based on the
%               baseline and stdev for all sessions (run and sleep) for
%               the entire day.    AS added 1-12-10
%
% Outputs:
%ripples 	- structue with various fields, including the following which
%			describe each ripple.
%	starttime - time of beginning of ripple
%	endtime	  - time of end of ripple
%	maxthresh - the largest threshold in stdev units at which this ripple
%			would still be detected.

disp(sprintf('%s Event Konsensus processing %s',eventname,animalprefix))
consvariablename = [eventname 'kons'];
noisestdev = 0;
noisebaseline = 0;
automaxval = 0;
maxpeakval = 2000;
samethreshperday = 0;
min_separation = 0;
NOISE_excludetime = 0;
save_powertrace = 1;
% define the standard deviation for the Gaussian smoother which we
% apply before thresholding (this reduces sensitivity to spurious
% flucutations in the ripple envelope)
smoothing_width = 0.004; % 4 ms

if (~isempty(varargin))
    assign(varargin{:});
end

tetinfo = loaddatastruct(animaldir, animalprefix, 'tetinfo');
task = loaddatastruct(animaldir, animalprefix, 'task');

for d = days
    for ep = 1:length(task{d})
        if ep > length(tetinfo{d}) || ~isfield(task{d}{ep},'type') || strcmp(task{d}{ep}.type,'failed sleep')
            disp('task struct has one more epoch than in tetinfo (or aberrant .type) - disregarding this epoch')
            continue
        end
        for TF = 1:length(tetfilters)
            tetfilter = tetfilters{TF};
            % retrieve valid tetrodes to iterate through
            tetlist =  evaluatefilter(tetinfo{d}{ep},tetfilter)';
            fprintf(' %d\n',tetlist);
            if isempty(tetlist)
                disp(sprintf('no valid detecting tetrodes, d %d e %d',d,ep))
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = tetfilter;
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
                ev.peak_value = [];
                ev.peak_index = [];
                ev.total_area = [];
                ev.area_midpoint_index = [];
                ev.total_energy = [];
                ev.energy_midpoint_index = [];
                ev.absolute_maxthresh = [];
                eventscons{d}{ep}{TF} = ev;
                clear ev;
                continue
            end
            if length(tetlist) < 3
                fprintf('day %d ep %d: less than 3 tets!\n',d,ep)
            end
            
            % load eeg data
            eventeeg = loadeegstruct(animaldir, animalprefix, noiseeventeegname, d, ep, tetlist);
            ripeeg = loadeegstruct(animaldir, animalprefix, ripeventeegname, d, ep, tetlist);
%             eeg = loadeegstruct(animaldir, animalprefix, 'eeg', d, ep, tetlist);
            %DR aug 2016 tmp change start time to double
            if ~isa(eventeeg{d}{ep}{tetlist(1)}.starttime, 'double')
                eventeeg{d}{ep}{tetlist(1)}.starttime = double(eventeeg{d}{ep}{tetlist(1)}.starttime);
            end
            % obtain times vector from one of the tetrodes as a reference
            %DR Jan 2017... get the eeg times from an eeg file.
            eegtimesvec_ref = geteegtimes(eventeeg{d}{ep}{tetlist(1)});
            %             eegtimesvec_ref =
            epoch_starttime = eegtimesvec_ref(1);
            epoch_endtime = eegtimesvec_ref(end);
            numsamples_refeeg = length(eventeeg{d}{ep}{tetlist(1)}.data(:,1));
            if numsamples_refeeg ~= length(eegtimesvec_ref)
                keyboard
            end
            
            % obtain the squared amplitudes from each tetrode
            noisesqamps = nan(length(tetlist),length(eegtimesvec_ref));  % initialize
            ripsqamps = nan(length(tetlist),length(eegtimesvec_ref));  % initialize
            for tt = 1:length(tetlist)
                tet = tetlist(tt);
                noisedata = double(eventeeg{d}{ep}{tet}.data(:,1))';
                ripdata = double(ripeeg{d}{ep}{tet}.data(:,1))';
                if length(noisedata) == numsamples_refeeg
                    noisesqamps(tt,:) = noisedata.^2;
                    ripsqamps(tt,:) = ripdata.^2;
                else
                    % if mismatch in # of samples for this tetrode (different DSPs often start at slightly different times),
                    % 'interpolate' (really just quick way of appending NaNs on the deficient end)
                    eegtimesvec = geteegtimes(eventeeg{d}{ep}{tet});
                    noiseinterpdata = interp1(eegtimesvec,noisedata,eegtimesvec_ref,'nearest')';
                    noisesqamps(tt,:) = noiseinterpdata.^2;
                    ripinterpdata = interp1(eegtimesvec,ripdata,eegtimesvec_ref,'nearest')';
                    ripsqamps(tt,:) = ripinterpdata.^2;
                end
            end
            
            % basic values
            samprate = eventeeg{d}{ep}{tetlist(1)}.samprate;
            mindur = round(min_suprathresh_duration * samprate);
            
            % individually smooth the squared amplitudes with a kernel,
            % then take square root
            kernel = gaussian(smoothing_width*samprate, ceil(8*smoothing_width*samprate));
            noise_rms_amps = nan(size(noisesqamps));
            rip_rms_amps = nan(size(noisesqamps));
            for tt2 = 1:length(tetlist)
                noise_rms_amps(tt2,:) = realsqrt(smoothvect(noisesqamps(tt2,:), kernel));
                rip_rms_amps(tt2,:) = realsqrt(smoothvect(ripsqamps(tt2,:), kernel));
            end
            
            % Now take the mean across sites
            noisepowertrace = nanmean(noise_rms_amps,1);
            rippowertrace = nanmean(rip_rms_amps,1);
            % Z score and take subtract ripz from noisez
            noiseZ = zscore(noisepowertrace);
            ripZ = zscore(rippowertrace);
            diffZ = noiseZ - ripZ;
            % calculate mean and std of powertrace, to use in extractevents
            stdev = std(diffZ);
            baseline = mean(diffZ);
            thresh = baseline + nstd * stdev;

            if any(~isreal([baseline stdev]))
                error('these should be real valued..')
            end
            
            % extract the events if this is a valid trace
            if any(find(diffZ<baseline))
                fprintf('day %d epoch %d %s \n',d,ep, consvariablename)
                
                %another odd error where extractevents hangs if the
                %baseline is below zero. in which case bump everything up
                %by the diff of baseline, 0. which makes baseline 0
                if baseline < 0
                    diffZ = diffZ + (0-baseline);
                    thresh = thresh + (0-baseline);
                    baseline = 0;
                end
                
                % some odd error within the extractevents .mex makes it lock up
                % sometimes if the powertrace is not doubled.. therefore down below I just
                % take the first half of the output
                tmpevent = extractevents([diffZ'; diffZ'], thresh, baseline, min_separation, mindur, 0)';
                if mod(size(tmpevent,1),2)==0
                    lastind = size(tmpevent,1)/2;
                else
                    lastind = (size(tmpevent,1)-1)/2;
                end
                tmpevent = tmpevent(1:lastind,:);

                % create an excluded buffer zone around detect noise events
                bufftmpevent = [tmpevent(:,1)-NOISE_excludetime*1500 tmpevent(:,2)+NOISE_excludetime*1500 tmpevent(:,3:end)];
                % cap at the ends
                for ib = 1:length(bufftmpevent(:,1))
                     if bufftmpevent(ib,1) < 1
                        bufftmpevent(ib,1) = 1;
                    end
                    if bufftmpevent(ib,2) > length(diffZ)
                        bufftmpevent(ib,2) = length(diffZ);
                    end
                end
                
                startind = bufftmpevent(:,1);
                endind = bufftmpevent(:,2);
                % Assign the fields
                ev.eventname = eventname;
                ev.nstd = nstd;
                ev.min_suprathresh_duration = min_suprathresh_duration;
                ev.tetfilter = tetfilter;
                ev.tetlist = tetlist;
                ev.starttime = epoch_starttime + startind / samprate;
                ev.endtime = epoch_starttime + endind / samprate;
                ev.maxthresh = (bufftmpevent(:,9) - baseline) / stdev; %z scored
                ev.peak_value = bufftmpevent(:,3);
                ev.peak_index = bufftmpevent(:,4);
                ev.total_area = bufftmpevent(:,5);
                ev.area_midpoint_index = bufftmpevent(:,6);
                ev.total_energy = bufftmpevent(:,7);
                ev.energy_midpoint_index = bufftmpevent(:,8);
                ev.absolute_maxthresh = bufftmpevent(:,9); %absolute
            else
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = '';
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
                ev.peak_value = [];
                ev.peak_index = [];
                ev.total_area = [];
                ev.area_midpoint_index = [];
                ev.total_energy = [];
                ev.energy_midpoint_index = [];
                ev.absolute_maxthresh = [];
            end
            if any(find(diffZ<baseline))==0
                warning(['No below baseline values in data.Fields left blank, day ' num2str(d) 'epoch ' num2str(ep)])
            end
            ev.timerange = [epoch_starttime epoch_endtime];
            ev.samprate = samprate;
            ev.baseline = baseline;
            ev.std = stdev;
            if save_powertrace
                ev.eegtimesvec_ref = eegtimesvec_ref;
                ev.powertrace = diffZ;
            end
            clear eventeeg
            eventskons{d}{ep}{TF} = ev;
            clear ev; 
        end
    end
    
    if exist('eventskons','var')
        eval([consvariablename ' = eventskons;']);
        save(sprintf('%s/%s%skons%02d.mat', animaldir, animalprefix, eventname,d),consvariablename);
        cd(animaldir)
        clear eventskons;
    else
        fprintf('consensus not saved for %s %s, day %d\n',animalprefix,eventname,d)
    end
    
end

end
