function [out, newTimesStampsInd] = pos_realign(srcdir, destdir, animalID, sessionNum, epochnum, epochname, postimestamps)

%uses dio pulses and cam module frame numbers (if available) to redistribute pos timestamps
% AG,DR Aug2016
%-DR--UPDATE--- 9/3/16.. SEEMS LIKE REPEATS IN postimestamps CORRESPOND TO DROPPED PACKETS OF NEURAL DATA.. SO DELEAT REPEATS INSTEAD

disp(sprintf('----------------------REALIGNING s%d ep%d-----using DR_pos_realign_NN-Method------------',sessionNum, epochnum))
% 1. load camera hardware frame counts, if possible
cd(sprintf('%s%s',srcdir,epochname))
camlog = [];
camlogfile = sprintf('%spos_cameraHWFrameCount.dat',epochname(1:end-3));
if exist(camlogfile, 'file'),
    camlog = readTrodesExtractedDataFile(camlogfile);
end

if ~isempty(camlog)
    CamlogDiscrepency = 0;
    camlog.fields.data = double(camlog.fields.data);
        %sometimes the first count is wayy off (like [13227, 1, 2, 3..]). if this happens, set it to 1 minus the second hwcount.
    if diff(camlog.fields.data(1:2)) < 0
        camlog.fields.data(1) = camlog.fields.data(2)-1;
    end
    %    get rid of wrap around so I can use the camlog values
    wrapz = diff(camlog.fields.data) < 0;
    wrapval = camlog.fields.data(find(diff(wrapz) < 0,1));
    if ~isempty(wrapval)
        cumcammult = cumsum([false; wrapz]);
        camlog.fields.data = cumcammult*wrapval+camlog.fields.data;
    end
    if length(camlog.fields.data) ~= length(postimestamps)
        CamlogDiscrepency = length(camlog.fields.data) - length(postimestamps);
    end
    disp(sprintf('CamlogDiscrepency = %d',CamlogDiscrepency))
else
    disp('no CameraHWframecount file found')
    camlog.fields.data = [];
end

% 2. load DIOs (from ff folder, not binaries)
cd(destdir)
try
    load(sprintf('%s%sDIO%02d.mat', destdir,animalID,sessionNum)); %loads DIO struct
catch
end
if ~isempty(DIO)
    for d = 1:length(DIO{sessionNum}{epochnum})
        %identify cam chan by rate; should be ~30hz on avg (btwn 26-34)
        % only look at times of state=1 (pulse onset)
        warning('off', 'MATLAB:mode:EmptyInput')
        if length(DIO{sessionNum}{epochnum}{d}.values)>1000  %if there are enough events..
            diovals = DIO{sessionNum}{epochnum}{d}.values;
            diotimes = DIO{sessionNum}{epochnum}{d}.times;
            if isExcluded(1/mode(diff(diotimes(diovals==1))),[26, 34]);
                ratecheck(d) = 1;
            end
        else
            ratecheck(d) = 0;
        end
    end
    if sum(ratecheck) == 0
        dios = [];
        disp('no camerasync chan found in dios')
    elseif sum(ratecheck) > 1
        disp(sprintf('multiple potential camerasync chans found. using ch %d',find(ratecheck>0,1)))
        % only use pulse onset times (value = 1)
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0,1)}.values==1);
    else
        %only use pulse onset times (value =1 )
        dios = DIO{sessionNum}{epochnum}{find(ratecheck>0)}.times(DIO{sessionNum}{epochnum}{find(ratecheck>0)}.values==1);
        disp(sprintf('using camerasync chan %d for timestamp realigmnent',find(ratecheck>0)))
    end
else
    dios = [];
    disp('no dio files found for this session')
end

% --------------DR Realignment Strategy----------------------
% Perform backwards nearest neighbor lookup from each of the tracked postimestamps onto the diotimestamps

% (1) --------- Error condition 1 --- Repeats in postimestamps
% IF length(camlog) == length(postimestamps)..
% interpole across repeat region with the values of corresponding camlog.. this will handle dropped frames
% ELSEIF length(camlog) ~= length(postimestamps) OR isempty(camlog)
%    interpolate across repeat region with number of repeated frames.. this assumes there are no dropped frames

% (4) --------- Error condition 2
% If there are no dios or camlog, I think the best we can do is just trust the videostamps and interp across any repeat regions
% based on the number of frames repeated and the neighboring timestamps // known datasets with this condition also have a variable frame rate :(

if isempty(dios)
    disp('currently no correction strategy without camera strobee; trusting the postimestamps')
    out = postimestamps;
    error('this is broken')
    
else
    %% -----------------------------the business-----------------------------------
    newTimeStamps = zeros(length(postimestamps),1);
    pos2diosInd = lookup(postimestamps, dios); %find the  nearest neighbor of each video timestamps in dios. this handles dropped videoframes as well.
    newTimeStamps = dios(pos2diosInd);
    repeatpostimestamps = find(diff(postimestamps) <= 0);
    repeatdios = find(diff(dios) <= 0);
    repeatcamlogs = find(diff(camlog.fields.data) <= 0);
    repeatnewTimeStamps = find(diff(newTimeStamps) <= 0);
    disp(sprintf('repeatpostimestamps = %d',length(repeatpostimestamps)))
    disp(sprintf('repeatdios  = %d',length(repeatdios)))
    disp(sprintf('repeatcamlogs = %d',length(repeatcamlogs)))
    disp(sprintf('repeatnewTimeStamps = %d',length(repeatnewTimeStamps)))
    if ~isempty(repeatnewTimeStamps) %if there are any newtimestamp repeats
        [BrepInds, ErepInds] = getrepInds(newTimeStamps);
        [newTimeStamps, camlog] = removerepeats(newTimeStamps, camlog, BrepInds, ErepInds);
    else
        %no repeats.. assigned a direct NNlookup from dios
    end
    goodtimes = ~isnan(newTimeStamps);
    newTimeStamps = newTimeStamps(goodtimes);
    goodcamtimes = ~isnan(camlog.fields.data);
    camlog.fields.data = camlog.fields.data(goodcamtimes);
    disp(sprintf('%.02f percent of postimestamps removed bc repeats',(sum(~goodtimes)/length(newTimeStamps))*100))
    if (sum(~goodtimes)/length(newTimeStamps))*100 > 5; % 5 percent threshold picked arbitrarily for debugging
        disp('more than 5percent postimestamps removed.. are you sure you want to continue? Take a look at the data')
        % plotcondensedDIOs
        if 0
        figure
        plot(1:3,repmat(dios(1),1,3),'*')
        hold on
        plot([dios(1:length(newTimeStamps))-dios(1:length(newTimeStamps)),postimestamps(1:length(newTimeStamps))-dios(1:length(newTimeStamps)),newTimeStamps-dios(1:length(newTimeStamps))]','.-')
        end
        if 0
            %load eeg stuff
            load(sprintf('EEG/%seeg%02d-%d-01.mat',animalID,sessionNum,epochnum))
            epdate = epochname(1:8);
            epbasename = sprintf('%s_%s_%02d', epdate, animalID, epochnum);
            LFPtimestampsadj = readTrodesExtractedDataFile(sprintf('%s%s.LFP/%s.timestamps.adj.dat',srcdir, epbasename, epbasename));
            LFPtimestampsadj = double(LFPtimestampsadj.fields.data)/30000; %put into seconds
            disp(sprintf('EEG/%seeg%02d-%d-01.mat',animalID,sessionNum,epochnum))
            disp(sprintf('%s%s.LFP/%s.timestamps.adj.dat',srcdir, epbasename, epbasename))
            try
                eeg{sessionNum}{epochnum}{1}.data = [eeg{sessionNum}{epochnum}{1}.data LFPtimestampsadj]; %put timestamps next to eeg values
            catch
                disp('DEBUGGING CHECKS:::')
                LFPtime2datadiscepency = length(eeg{sessionNum}{epochnum}{1}.data) - length(LFPtimestampsadj);
                disp(sprintf('LFPtime2datadiscepency = %.03f seconds', abs(LFPtime2datadiscepency/30000)))
                LFPtimestampsNotAdj = readTrodesExtractedDataFile(sprintf('%s%s.LFP/%s.timestamps.dat',srcdir, epbasename, epbasename));
                LFP_adj2nonadjtimediscepency = length(LFPtimestampsadj) - length(LFPtimestampsNotAdj.fields.data);
                disp(sprintf('LFP_adj2nonadjtimediscepency = %.03f seconds', abs(LFP_adj2nonadjtimediscepency/30000)))
                repeatLFPtimestampsNotAdj = find(diff(LFPtimestampsNotAdj.fields.data) <= 0);
                disp(sprintf('repeatLFPtimestampsNotAdj = %d', length(repeatLFPtimestampsNotAdj)))
                disp('paused. ctrl c to abort')
                pause
            end
            eegWtime = eeg{sessionNum}{epochnum}{1}.data;
            %find LFP index of repeat regions
            startrepval = postimestamps(BrepInds);
            endrepval =  postimestamps(ErepInds+1);
            lfprepInds = lookup([startrepval endrepval],eegWtime(:,2));
            for i = 1:length(repeatnewTimeStamps)
                figure
                starting = repeatnewTimeStamps(i) - 100;
                ending = repeatnewTimeStamps(i) + 100;
                plot([dios(starting:ending),postimestamps(starting:ending),newTimeStamps(starting:ending)]','.-')
                title(sprintf('check segment %d of %d', i, length(repeatnewTimeStamps)))
                pause
                %plot LFPrepeats
                eegx = eegWtime(lfprepInds(i,1)*1500:lfprepInds(i,2)*1500,2);
                eegy = eegWtime(lfprepInds(i,1)*1500:lfprepInds(i,2)*1500,1);
                plot(eegx,eegy)
                %plot postimestamp diff of repeat region
                figure(2)
                xdiffrep = postimestamps(BrepInds-3*1500:ErepInds+3*1500);
                diffrep = diff(xdiffrep);
                plot(xdiffrep(1:end-1),diffrep)
                %         plot(diffrep)
            end
        end
    end
    %     check = find((postimestamps-newTimeStamps)<0);  %sometimes the interpolation causes backwards correction
    %     if ~isempty(check)
    %         newTimeStamps(check) = postimestamps(check);
    %     end
    
    
%     dioslost = find(diff(dios)>.04);
%     dioslosttime = diff([dios(dioslost) dios(dioslost+1)],1,2); %DR.. get time lost from dropped dios
    
    %     disp(sprintf('putative dropped frames/extra dios: %d',length(dios(pulseIndAfterGap:end))-length(newTimeStamps(frameIndAfterGap:end))))
%     disp(sprintf('any dios lost: %d',length(dioslost)))
%     disp(sprintf('time from dios lost: %.02f seconds',dioslosttime))
    %     disp(sprintf('average lag: %04f',mean(postimestamps-newTimeStamps)))
    if length(camlog.fields.data) == length(postimestamps)
        disp(sprintf('dropped frames:%d',sum(diff(camlog.fields.data)>1)))
    else
        disp(sprintf('camlog unused... might need to start using this to account for potential delay drift that would mess up nearest neighbor'))
    end
end


% sanity check2: no duplicate or reverse order timestamps should ever be found
if any(diff(newTimeStamps)<=0)
    duplinds = find(diff(newTimeStamps) <= 0);
    disp('duplicate or inverted indices at')
    disp(duplinds)
    error('duplicate or inverted timestamps found, abort');
end
% sanity check3: no nan timestamps should happen
if any(isnan(newTimeStamps))
    error('nan timestamps found, abort');
end
% DR not true anymore since i'm using the pulses before the gap.. if there's a postimestamp before gap as well, it may be zero
% % sanity check4: no zero timestamps should happen
% if any(newTimeStamps==0)
%     error('zero timestamps found, abort');
% end

out = newTimeStamps;
newTimesStampsInd = goodtimes;
end

function plotNNshift
    %distribution of the nearest neighbor shift from postimestamps to dios
    figure
    pos2diosval = postimestamps(goodtimes) - dios(pos2diosInd(goodtimes));
    hist(pos2diosval,100)
end

function plotend
        figure
        plot([dios(end-500:end),postimestamps(end-500:end),newTimeStamps(end-500:end)]','.-')
        title('check end')
end

function plotbeginning
        figure
        plot([dios(1:500),postimestamps(1:500),newTimeStamps(1:500)]','.-')
        title('check beginning')
end

function [BrepInds, ErepInds] = getrepInds(newTimeStamps)
        % Get indices of each range of repeat regions
        X = diff(newTimeStamps) > 0; %logical indices of non-repeats and non-inversions
        Brep = find([true; X]); % begin of each group
        Erep = find([X;true]); % end of each group
        Lrep = 1+Erep-Brep; % length from the beginning of each repeat series until the next non-repeat, inclusive
        %         Lrep = Erep-Brep; % length from the beginning of each repeat series until the next non-repeat, EXclusive
        Y = Lrep>1; %All the groups with more than one element
        BrepInds = Brep(Y); % beginning indices of each group
        ErepInds = Erep(Y); % end indices of each group
        LrepInds = Lrep(Y); % length of repeats for each group
end

function [newTimeStamps, camlog] = removerepeats(newTimeStamps, camlog, BrepInds, ErepInds);
         for rep = 1:length(BrepInds); %for each group of repeats
            newTimeStamps(BrepInds(rep)+1:ErepInds(rep)) = nan;
            if length(camlog.fields.data) == length(newTimeStamps) %  if camlogs available and can be matched to postimestamps, use camlog count to base the interpolation.. this will handle dropped frames during repeat periods
                camlog.fields.data(BrepInds(rep)+1:ErepInds(rep)) = nan; %deal with mismatching camlogs before this line..
            end
            %             startrepval = newTimeStamps(BrepInds(rep));
            %             endrepval =  newTimeStamps(ErepInds(rep)+1); % equivalent to newTimeStamps(BrepInds(rep)+LrepInds(rep))
            %             if length(camlog.fields.data) == length(newTimeStamps) %  if camlogs available and can be matched to postimestamps, use camlog count to base the interpolation.. this will handle dropped frames during repeat periods
            %                 interpedtimestamps = interp1([BrepInds(rep) ErepInds(rep)+1], [startrepval endrepval], [camlog.fields.data(BrepInds(rep):ErepInds(rep))]);
            %                 newTimeStamps(BrepInds(rep):ErepInds(rep)) = interpedtimestamps;
            %             else
            %                 % repeats but no camlog OR no dropped frames within repeat region -- interpolate using number of repeated frames
            %                 interpedtimestamps = interp1([BrepInds(rep) ErepInds(rep)+1], [startrepval endrepval], [BrepInds(rep):ErepInds(rep)]');
            %                 newTimeStamps(BrepInds(rep):ErepInds(rep)) = interpedtimestamps;
            %                 %need to create a fix for the condition of repeats at the end of the postimestamps.. i think that will currently break this bc there's no endrepeats+1
            %             end
        end
end









