function extractMUAevents(animaldir, animalprefix, day, tetfilter, min_dur, nstd, muasource, varargin)

% Follows Davidson et al 2009 detection of candidate replay events using mua trace
% MUA trace can be generated from either spikes.mat or marks.mat (specify 'marls' or 'spikes' as muasource)
% combines mark/spike times across tetrodes, generates a smoothed histogram (1ms bins, 15ms SD gaussian)
% uses extractevents c++ function to detect fluctuations in the mua trace exceeding nstd for mindur

% saves output to <anim>muaripples<session>.mat; {d}{e}.fields

%
%fileprefix	- folder name where the day's data is stored
%
%day		- the day to process
%
%min_suprathresh_duration
%		- the time (in seconds) which the signal
%       must remain above threshold to be counted as as ripple; this guards
%       against short spikes in signal (e.g. noise) as being counted as
%       ripples. Set min_suprathreshold_duration to some small value, like
%       0.015 s.
%
%nstd		- the number of standard dev that ripple must be from mean to
%			be detextractconsensusected. Start with 2.
%
%
% Outputs:
%muaripples 	- structue with various fields, including the following which
%			describe each ripple.
%	starttime - time of beginning of ripple
%	endtime	  - time of end of ripple
%	maxthresh - the largest threshold in stdev units at which this ripple
%			would still be detected.

disp(sprintf('MUA Event detecting %s d%d',animalprefix,day))

stdev = 0;
baseline = 0;
timebin = .001;  % this generates a 1ms-bin histogram of spike counts 
samprate = 1/timebin;  % see above; NOT LFP Fs
samethreshperday = 0;
excludeEvents = '';
min_separation = 0.015;
min_dur = 0.015;
smoothingwidth = .015; % 15ms kernel width
kernel = gaussian(smoothingwidth/timebin, 100);  

if (~isempty(varargin))
    assign(varargin{:});
end

tetinfo = loaddatastruct(animaldir, animalprefix, 'tetinfo');

for d = day
    for ep = 1:length(tetinfo{d})
        
        % retrieve valid tetrodes to iterate through
        tetlist =  evaluatefilter(tetinfo{d}{ep},tetfilter)';
        if isempty(tetlist)
            disp(sprintf('no valid detecting tetrodes, d %d e %d',d,ep))
            ev.eventname = '';
            ev.nstd = [];
            ev.min_suprathresh_duration = [];
            ev.tetfilter = tetfilter;
            ev.tetlist = [];
            ev.starttime = [];
            ev.endtime = [];
            ev.maxthresh = [];
            ev.peak_value = [];
            ev.peak_index = [];
            ev.total_area = [];
            ev.area_midpoint_index = [];
            ev.total_energy = [];
            ev.energy_midpoint_index = [];
            ev.absolute_maxthresh = [];
            muaripples{d}{ep} = ev;
            clear ev;
            continue
        end
        if length(tetlist) < 3
            warning(sprintf('day %d ep %d: less than 3 tets!',d,ep))
        end
        
        % load source data
        switch muasource
            %                case 'spikes'
            %                   datasource = loaddatastruct(animaldir, animalprefix, 'spikes', d);
            %                   NOT WRITTEN YET
            
            case 'marks'
                datasource = loaddatastruct(animaldir, animalprefix, 'marks', d);
                spiketimes = cellfun(@(x) x.times,datasource{d}{ep}(tetlist),'UniformOutput',0);
                % concatenate times from all tetrodes, then reorder
                spiketimes = sort(vertcat(spiketimes{:}));
            otherwise
                error('unexpected mua source')
        end
        
        % define 1ms timebins from an eeg trace (this enables identification of any nan periods)
        eeg = loadeegstruct(animaldir, animalprefix, 'eeg', d, ep, tetlist(1));
        eegtimes = geteegtimes(eeg{d}{ep}{tetlist(1)});
        
        % generate histogram
        edges = eegtimes(1):timebin:eegtimes(end);
        spikecounts = histcounts(spiketimes,edges);
        
        % smooth the resultant histogram
        smoothed = smoothvect(spikecounts, kernel);
        
        % identify any nan times
        naninds = find(isnan(eeg{day}{ep}{tetlist(1)}.data));
        [lo,hi]= findcontiguous(naninds);  %find contiguous NaNs
        lo_times = eegtimes(lo);
        hi_times = eegtimes(hi);
        if ~isempty(lo)
            warning('nans found! naning out the smoothwin around them')
            lo_times = lo_times-smoothingwidth;
            hi_times = hi_times+smoothingwidth; % get rid of times where smoothing edge effects will be
            nonans = ~logical(list2vec([lo_times hi_times],edges(1:end-1)));
        else
            nonans = logical(ones(length(edges)-1,1));
        end
        
        % only want  to calculate mean and sd of immobile times
        immobilefilter = {'ag_get2dstate', '($immobility == 1)','immobility_velocity',4,'immobility_buffer',0};
        immoperiods = kk_evaluatetimefilter(animaldir,animalprefix, {immobilefilter}, [d ep]);
        immobins = logical(list2vec(immoperiods{d}{ep},edges(1:end-1)));
        
        validbins = immobins & nonans;
        baseline = mean(smoothed(validbins));
        stdev = std(smoothed(validbins));
        thresh = baseline + nstd * stdev;
        
        %if there are any nans, nan out the smoothing edge effects they will have had
        smoothed(~nonans) = nan;
        
        %bug within the extractevents .mex makes it lock up sometimes if the trace is not doubled..
        % so just double it and take the first half of the output (from kk)
        tmpevent = extractevents([smoothed'; smoothed'], thresh, baseline, min_separation, min_dur, 0)';
        if mod(size(tmpevent,1),2)==0
            lastind = size(tmpevent,1)/2;
        else
            lastind = (size(tmpevent,1)-1)/2;
        end
        tmpevent=tmpevent(1:lastind,:);
        
        % only save events that occur during immobility
        startind = tmpevent(:,1);
        endind = tmpevent(:,2);
        immoevents = isExcluded(edges(1) + startind / samprate, immoperiods{d}{ep}) & isExcluded(edges(1) + endind / samprate, immoperiods{d}{ep});
        
        % Assign the fields
        ev.eventname = 'muaripples';
        ev.nstd = nstd;
        ev.min_suprathresh_duration = min_dur;
        ev.tetfilter = tetfilter;
        ev.tetlist = tetlist;
        ev.starttime = edges(1) + startind(immoevents) / samprate;
        ev.endtime = edges(1) + endind(immoevents) / samprate;
        ev.maxthresh = (tmpevent(immoevents,9) - baseline) / stdev; %z scored
        ev.peak_value = tmpevent(immoevents,3);
        ev.peak_index = tmpevent(immoevents,4);
        ev.total_area = tmpevent(immoevents,5);
        ev.area_midpoint_index = tmpevent(immoevents,6);
        ev.total_energy = tmpevent(immoevents,7);
        ev.energy_midpoint_index = tmpevent(immoevents,8);
        ev.absolute_maxthresh = tmpevent(immoevents,9); %absolute
        % add additional params
        ev.timerange = [edges(1) edges(end)];
        ev.samprate = samprate;
        ev.baseline = baseline;
        ev.std = stdev;
        ev.binedges = edges;
        ev.muatrace = smoothed;
        
        % sanity checking - plot
        if 0
            figure; hold on;
            plot(edges(immobins),-1*ones(sum(immobins),1),'.')
            plot(edges(1:end-1),spikecounts)
            plot(edges(1:end-1),smoothed)
            plot(edges(~nonans),-2*ones(sum(~nonans),1),'rx')
            plot([edges(1) edges(end)],[baseline baseline],'k')
            plot([edges(1) edges(end)],[thresh thresh],'k:')
            plot(ev.starttime,-.5*ones(length(ev.starttime),1),'g.')
            plot(ev.endtime,-.5*ones(length(ev.starttime),1),'r.')
            
        end
        
        % store events
        clear eventeeg
        muaripples{d}{ep} = ev;
        clear ev;
    end
    
end


if exist('muaripples','var')
    save(sprintf('%s/%smuaripples%02d.mat', animaldir, animalprefix,d),'muaripples');
    cd(animaldir)
    clear muaripples;
else
    disp(sprintf('muaripples not saved for %s day %d',animalprefix,d))
end

end


