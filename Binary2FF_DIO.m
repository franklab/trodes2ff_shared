function Binary2FF_DIO(animalID, srcdir, destdir, sessionNum)

%Demetris Roumis July 2016
%This function  saves DIO data in the Filter Framework v1 format.
%It is assumed that there is a directory of binary DIO files in the form of:
%<animalname>/preprocessed/<date (%YYYY%MM%DD)>/<date>_<animal>_<epoch>.DIO/...
%...<date>_<animal>_<epoch>.DIO_D<in/out>.dat
%ex: /mnt/data19/droumis/D10/preprocessed/20160516/20160516_D10_02.DIO/20160516_D10_02.dio_Din7.dat

%animalID -- a string identifying the animal's id (appended to the beginning of the files).. ex: 'D10'
%sessionNum -- day.. ex: '4'
%srcdir -- preprocessed binary directory.. ex: /mnt/data19/droumis/D10/preprocessed/20160512/
%destdir -- the directory where the processed files should be saved for the animal...
%...ex: /mnt/data19/droumis/D10/filterframework/

cd(destdir);
if ~isempty(dir([animalID 'DIO' num2str(sprintf('%02d', sessionNum)) '.mat']))
    disp('DIO file detected.. will try to append to existing structure')
    load([animalID 'DIO' num2str(sprintf('%02d', sessionNum)) '.mat']) %loads as DIO var
else
    disp('no DIO file detected.. proceeding to create one from scratch')
end
cd(srcdir);
DIOepochDirs = dir('*.DIO');
DIOepochDirs = arrayfun(@(x) x.name,DIOepochDirs,'UniformOutput', false);
DIOepochDirs = sort(DIOepochDirs); %this should be safe if the file naming is consistent with '<date>_<animal>_<2digitepoch>.DIO'

disp('Processing DIO files...');
tic
epoch_IDs = getepochIDs(DIOepochDirs);
for iep = 1:length(DIOepochDirs)
    epoch = epoch_IDs(iep);
    cd(srcdir); %necessary for every loop after the first
    cd(DIOepochDirs{iep}); %go into current epoch DIO dir
    DIOntrodefiles = dir('*.adj.dat');%find the adjusted DIO files
    if ~isempty(dir('*.adj.adj.dat'))
        disp('multiple adjusted DIO files for same channel exist || adj.adj.dat || fix before proceeding')
        return
    end
    if isempty(DIOntrodefiles) & ~isempty(dir('*.dat'))
        warning('only unadjusted DIO files found! proceeding with them...')
        DIOntrodefiles = dir('*.dat');
    end
    
    tmpData = [];
    for tmpchans = 1:length(DIOntrodefiles) %load all ntrodes at once to be able to sort by ntrodeID
        tmpData{tmpchans} = readTrodesExtractedDataFile(DIOntrodefiles(tmpchans).name);
    end
    tmpid = cellfun(@(x) x.id,tmpData,'UniformOutput', false); %get the DIO alphanumeric id's
    tmpchanIDs = cellfun(@(x) str2double(regexp(x,'\d+','match')),tmpid); %get the channel IDs numbers
    tmpdirs = cellfun(@(x) x.direction,tmpData,'UniformOutput', false); %get the DIO directions, 'input' or 'output'
    Dinputs = strcmp('input',tmpdirs); % 1 = input; 0 = output
    Doutputs = ~Dinputs;
    [~, DinsortedInd] = sort(tmpchanIDs(Dinputs)); %sort inputs
    [~, DoutsortedInd] = sort(tmpchanIDs(Doutputs)); %sort outputs
    tmpDataIn = tmpData(Dinputs);
    tmpDataIn = tmpDataIn(DinsortedInd); %sort the input data
    tmpDataDout = tmpData(Doutputs);
    tmpDataDout = tmpDataDout(DoutsortedInd); %sort the output data
    tmpData = [tmpDataIn tmpDataDout];
    for DIOchan = 1:length(tmpData);
        DIO{sessionNum}{epoch}{DIOchan}.descript = tmpData{DIOchan}.description;
        DIO{sessionNum}{epoch}{DIOchan}.direction = tmpData{DIOchan}.direction;
        DIO{sessionNum}{epoch}{DIOchan}.original_id = tmpData{DIOchan}.id;
        DIO{sessionNum}{epoch}{DIOchan}.original_file = tmpData{DIOchan}.original_file;
        DIO{sessionNum}{epoch}{DIOchan}.input = Dinputs(DIOchan);
        DIO{sessionNum}{epoch}{DIOchan}.times = double(tmpData{DIOchan}.fields(1).data)/tmpData{DIOchan}.clockrate; %convert to sec
        DIO{sessionNum}{epoch}{DIOchan}.clockrate = tmpData{DIOchan}.clockrate;
        DIO{sessionNum}{epoch}{DIOchan}.values = tmpData{DIOchan}.fields(2).data;
        %should i include pulsetimes, timesincelast,timeuntilnext, pulselength, pulseind? do we have another processing step for DIO?
    end
end
save([destdir animalID,'DIO',num2str(sprintf('%02d',sessionNum)),'.mat'],'-v6','DIO'); %one dio file per day
FFv1filetime = toc; disp([sprintf('%.02f',FFv1filetime) ' seconds to make DIO files'])
cd(destdir);
end

function epoch_IDs = getepochIDs(epochDirs)
epoch_IDs = [];
for iep = 1:length(epochDirs)
    iepochID = strsplit(epochDirs{iep}, '_');
    iepochID = str2num(iepochID{3}(1:2)); %strip off epoch ID num
    epoch_IDs(iep,1) = iepochID;
end
end