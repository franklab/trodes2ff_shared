
function ntrodes = get_ntrodes_from_filenames(sourcedir)
% return array of ntrodes scraped from a sourcedir foldernames
% this strictly follows the standard FLab fname convention
ntrodeid = 4;

fs = dir(sourcedir);
files = arrayfun(@(x) regexp(x.name, ...
    '\d{8}_[a-zA-Z0-9]{3}_\d{2}\.nt\d{1,2}\.mda', 'match'),...
    fs, 'un', 0);
files = files(~cellfun('isempty', files));

ntrodes = unique(sort(cellfun(@(x) str2num(x{1}{ntrodeid}), cellfun(@(x) regexp(x, ...
    '[(nt).]', 'split'), files, 'un', 0))))';
end