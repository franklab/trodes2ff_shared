function tagEpochs(FFanimdir, animalID, day, taginput, varargin)

%Demetris Roumis June 2019

% add or update tags on task info struct - 
% tag input contains an array of cells, with each row  specifying any set
% of tagVariable, tagValue, days, epochs, in any order.
% leave out or leave empty the days, epochs Tags to include ALL.

% cellTags = [...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]};...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]}];

if ~isempty(varargin)
    assign(varargin{:});
end

assign(taginput{1}{:});  % load all the info in taginput 

cd(FFanimdir);
fprintf('taskinfo day %d \n', day);

 
task = loaddatastruct(FFanimdir, animalID, 'task',day);
if isempty(task)
    error('taskinfo does not exist for day %d; create a minimal one before adding additional tags creating it \n', day)
else
    fprintf('taskinfo exists for day %d; adding tags to it \n', day)
end


idx = cellfetch(task,' '); % beware, cellfetch doesn't return empty cells (so populate them with  sleep/run above) 
DE = {'days', 'epochs'};
for rowtag = 1:length(taginput(:,1))
    days = []; epochs = [];
    rowtaginput = taginput{rowtag}(1,:); %get current rowset of tags
    vars = {rowtaginput{1:2:end}};
    vals = {rowtaginput{2:2:end}};
    DEvarsInds= ismember(vars, [{'days'},{'epochs'}]); %search for Day Epoch Ntrodes variables
    DEvars = vars(DEvarsInds);
    DEvals = vals(DEvarsInds);
    tagvars = vars(~DEvarsInds); %collect tags (excluding day,epoch,ntrodes)
    tagvals = vals(~DEvarsInds);
    for i = 1:length(DEvars)
        eval(sprintf('%s=%s;',DEvars{i},mat2str(DEvals{i}))); %assign in day epoch ntrode vars to workspace
    end
    
    %set day,epoch,ntrode filters to ALL if empty
    if isempty(days)
        days = unique(idx.index(:,1));
    end
    if isempty(epochs)
        epochs = unique(idx.index(:,2));
    end

    %filter filterdays, sessionNum,  then epochs
    targetdays = idx.index(ismember(idx.index(:,1),day),:); %include only sessionNum(s)
    targetdays = targetdays(ismember(targetdays(:,1),days),:); %include only in days tag
    if isempty(targetdays) %if this tag set excludes the current sessionNum (day)
        continue %move on to the next tag set
    end
    teps = targetdays(ismember(targetdays(:,2),epochs),:);
    if isempty(teps)
        teps = [repmat(day,length(epochs),1) epochs'];
    end
    for i =1:size(teps,1)
        for tagn = 1:length(tagvars) %add tag var and val to this day,epoch
            eval(sprintf('task{teps(i,1)}{teps(i,2)}.%s = %s;',tagvars{tagn}, ...
                mat2str(tagvals{tagn})));
        end
    end
end


% for each day, save out a task info for legacy compatibility
fulltaskinfo = task;
for iday = 1:length(fulltaskinfo)
    if isempty(fulltaskinfo{iday})
        continue
    end
    task = cell(1,length(fulltaskinfo));
    task{iday} = fulltaskinfo{iday};
    save(sprintf('%s/%stask%02d.mat', FFanimdir, animalID, iday), 'task')
end
