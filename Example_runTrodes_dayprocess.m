
%{ 
--------Critical Notes----------
_________________TASK_INFO_________________________________

__DATE____DAY_TASK_______________
20160508   1  [s w s w s o s]
20160510   2  [s w s w s o s]
20160511   3  [s w s w s o s o _]
20160512   4  [s w s w s o s o s]
20160513   5  [s w s w s o s o s]
20160514   6  [s w s w s o s o s]
20160515   7  [s w s w s o s o s]
20160516   8  [s w s w s o s o s]
20160518   9  [s w s r s o s o s]
20160519  10  [_ w s r s o s o s]
20160520  11  [s w s r s o s o s]
20160521  12  [s w s r s o _ o s]
20160524  13  [s 6 s 6 s o s o s]
20160525  14  [s 6 s L s o s o s]
20160526  15  [s L s 6 s o s o s]
20160527  16  [s 6 s L s o s o s]
20160529  17  [s L s 6 s o s o s]
20160530  18  [s 6 s L s o s o s]
20160531  19  [s L s 6 s o s o s]

run cmperpix: 0.1555
sleep cmperpix: 0.1662
                                                                                                                                                
WELL PORT DIO MAP:

"ROTATED W"   "WTRACK"
3,9 =====|   1,7  2,8  3,9
         |    ||  ||  ||
2,8 =====|    ||  ||  ||
         |    ||  ||  ||
1,7 =====|    ----------

-1 second delay post port-in, then 400ms duration reward
__________________NTRODE_INFO__________________________________________________


__________________Daily Notes__________________________________


%}
clear all
animal = 'D10';
dates = [20160508 20160510 20160511 20160512 20160513 20160514 20160515 ...
    20160516 20160518 20160519 20160520 20160521 20160524 20160525 20160526 ...
    20160527 20160529 20160530 20160531];
days = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19];

sleepcmperpix = [0.1555];
runcmperpix = [0.1662];

%% Pos, Behavior Params
posSource = 'online'; % 'manual1'
taskcoordinateprogram = [ {'wtrack'} {'getcoord_wtrack'}; {'sixarmtrack'} ...
    {'getcoord_6armtrack'}]; 
Winputdios = [1 2 3]; % for w track: [center, well 0, well 2]
Woutputdios = [7 8 9]; % for w track: [center, well 0, well 2]
MWinputdios = [1 2 3 4 5 6]; % for w track: [center, well 0, well 2]
MWoutputdios = [7 8 9 10 11 12]; % for w track: [center, well 0, well 2]
behaveStatePrograms = [ {'wtrack'} {'createBehaveState_wtrack'} {Winputdios} {Woutputdios}; {'sixarmtrack'} {'createrewardinfo_multW'} {MWinputdios} {MWoutputdios}]; %each row: epochtype rewardinfoprogram inputdios outputdiosoverwriteTrackNodes = 0; % set this to 1 if you want to overwrite the user specified nodes of the wtrack.. i.e. reruns createtaskstruct 
overwritebehaveState = 1; %overwrites on an epoch by epoch basis

%% Event Detection Params
excludenoise = 1; % toggles exclusion of lfp voltages above maxpeakval
maxpeakval = 700;
% noisekons 
excludeEvents = ''; % 'noise' to exclude detected noisekons. '' to not use noisekons
NOISEmindur = 1/1500; % must be at least this long. use 1 sample (1/1500) for noise 
NOISEnstd = 5; % ratio of the 500-600Hz to 150-250Hz zpower (5 std to start)
NOISE_excludetime = .1; % s to exclude around a noise event
% ripples
RIPmindur = .015; % s min duration of a ripple
RIPnstd = 2; % > # std ripples (start with 2 or 3)
consensusRIParea = {'ca1'}; %, 'mec', 'por'}; %define source region of ripples.. saved structure will contain this string

%% Spikes, Marks Params
marksource = 'mountainsort';
marknTrodes = [];
overwriteSpikes = 0;
%%

% ntrode tags. necessary tags: valid, ref, suparea, area, subarea.
% leave out or leave empty the days, epochs, ntrodes Tags to include ALL. 
% i.e.: {'days'}, {[]}, {'epochs'}, {[]}, {'ntrodes'}, {[]}
%     {[{'subarea'}, {'supf'}, {'ntrodes'}, {[6 8 9 11:15]}]};
%     {[{'subarea'}, {'deep'}, {'ntrodes'}, {[2:5 10]}]};
ntrodeTags = [...
    {[{'valid'}, {'yes'}, {'ntrodes'}, {[1:30]}]};
    {[{'valid'}, {'no'}, {'ntrodes'}, {[1 19]}]};
    {[{'suparea'}, {'cc'}, {'area'}, {'ref'}, {'subarea'}, {'mec'}, {'ref'}, {'gnd'}, ...
            {'cannula'}, {'mec'}, {'ntrodes'}, {14}]};
    {[{'suparea'}, {'hpc'}, {'area'}, {'ca1'}, {'subarea'}, {'d'}, {'ref'}, {14}, ...
            {'cannula'}, {'ca1'}, {'ntrodes'}, {[1 16:30]}]};
    {[{'suparea'}, {'ctx'}, {'area'}, {'mec'}, {'subarea'}, {''}, {'ref'}, {14}, ...
            {'cannula'}, {'mec'}, {'ntrodes'}, {[2:13 15]}]};
    {[{'subarea'}, {'supf'}, {'ntrodes'}, {[9 11]}]};
    {[{'subarea'}, {'deep'}, {'ntrodes'}, {[3]}]};
    {[{'subarea'}, {'nsupf'}, {'ntrodes'}, {[6 8 15 12 13]}]};
    {[{'subarea'}, {'ndeep'}, {'ntrodes'}, {[2 4 5 7 10]}]};
    {[{'subarea'}, {'d'}, {'rip_detect'}, {false}, {'ntrodes'}, {[1 19]}]};
            {[{'ntrodes'}, {2}, {'ntxy'}, {[4 3]}]};
            {[{'ntrodes'}, {3}, {'ntxy'}, {[3 4]}]};
            {[{'ntrodes'}, {4}, {'ntxy'}, {[4 4]}]};
            {[{'ntrodes'}, {5}, {'ntxy'}, {[3 1]}]};
            {[{'ntrodes'}, {6}, {'ntxy'}, {[2 3]}]};
            {[{'ntrodes'}, {7}, {'ntxy'}, {[3 3]}]};
            {[{'ntrodes'}, {8}, {'ntxy'}, {[2 4]}]};
            {[{'ntrodes'}, {9}, {'ntxy'}, {[2 1]}]};
            {[{'ntrodes'}, {10}, {'ntxy'}, {[3 2]}]};
            {[{'ntrodes'}, {11}, {'ntxy'}, {[1 4]}]};
            {[{'ntrodes'}, {12}, {'ntxy'}, {[2 2]}]};
            {[{'ntrodes'}, {13}, {'ntxy'}, {[1 3]}]};
            {[{'ntrodes'}, {14}, {'ntxy'}, {[1 2]}]};
            {[{'ntrodes'}, {15}, {'ntxy'}, {[1 1]}]};
            {[{'ntrodes'}, {1}, {'ntxy'}, {[4 2]}]};
            {[{'ntrodes'}, {16}, {'ntxy'}, {[1 4]}]};
            {[{'ntrodes'}, {17}, {'ntxy'}, {[1 3]}]};
            {[{'ntrodes'}, {18}, {'ntxy'}, {[2 3]}]};
            {[{'ntrodes'}, {19}, {'ntxy'}, {[1 2]}]};
            {[{'ntrodes'}, {20}, {'ntxy'}, {[2 4]}]};
            {[{'ntrodes'}, {21}, {'ntxy'}, {[2 2]}]};
            {[{'ntrodes'}, {22}, {'ntxy'}, {[3 3]}]};
            {[{'ntrodes'}, {23}, {'ntxy'}, {[1 1]}]};
            {[{'ntrodes'}, {24}, {'ntxy'}, {[3 2]}]};
            {[{'ntrodes'}, {25}, {'ntxy'}, {[2 1]}]};
            {[{'ntrodes'}, {26}, {'ntxy'}, {[3 4]}]};
            {[{'ntrodes'}, {27}, {'ntxy'}, {[4 3]}]};
            {[{'ntrodes'}, {28}, {'ntxy'}, {[4 4]}]};
            {[{'ntrodes'}, {29}, {'ntxy'}, {[3 1]}]};
            {[{'ntrodes'}, {30}, {'ntxy'}, {[4 1]}]}];

epochTags = [...
    {[{'days'}, {1:8}, {'daytype'}, {'wtrack'}]}; 
    {[{'days'},{9:12},{'daytype'},{'wtrackrotated'}]};
    {[{'days'},{13:19},{'daytype'},{'sixarm'}]};
    {[{'days'},{1:12},{'inputdio'},{Winputdios},{'outputdio'},{Woutputdios}]};
    {[{'days'},{6},{'epochs'}, {2}, {'exemplar'},{1}]}];

% Epoch types ordered, cell array per day
% (i.e. for day 1: taskseq{1} = {'sleep', 'wtrack', ...)
taskseq{1} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep'};
taskseq{2} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep'};
taskseq{3} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{4} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{5} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{6} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{7} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{8} = {'sleep', 'wtrack', 'sleep', 'wtrack', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{9} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{10} = {'', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{11} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{12} = {'sleep', 'wtrack', 'sleep', 'wtrackrotated', 'sleep', 'openfield', '', 'openfield', 'sleep'};
taskseq{13} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{14} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_left', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{15} = {'sleep', 'sixarmtrack_left', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{16} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_left', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{17} = {'sleep', 'sixarmtrack_left', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{18} = {'sleep', 'sixarmtrack_right', 'sleep', 'sixarmtrack_left', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};
taskseq{19} = {'sleep', 'sixarmtrack_left', 'sleep', 'sixarmtrack_right', 'sleep', 'openfield', 'sleep', 'openfield', 'sleep'};

%% ------------- Run Processes ------------------
% adjust timestamps
adjusttimestamps = 0;

% LFP (unreferenced)
makeFFLFP = 0;
makereferencedFFLFP = 0; useconfig = 0; % 'useconfig = 0' uses 'ref' tag in tetinfo
makeRippleFilteredLFP = 0; %ripple (140 - 250 Hz) samprate 1500 Hz
makeThetaFilteredLFP = 0; % theta (6-9 Hz) samprate 1500 Hz
makeLowGammaFilteredLFP = 0; % lowgamma (20 - 50 Hz) samprat 1500 Hz 
makeHighGammaFilteredLFP = 0; % highgamma (65 - 95 Hz) samprat 1500 Hz
makeFastGammaFilteredLFP = 0; % fastgamma (65 - 140 Hz) samprat 1500 Hz
makeNoiseFilteredLFP = 0; % 500-600 Hz samprate 1500 Hz

% POS
makeFFPOS = 0;
makeLinearCoords = 0;
makeFFLINPOS = 0;
% DIO 
makeFFDIO = 0;
makeFFbehaveState = 0; % parse DIO into a behave state struct
% spikes
makeFFspikes = 0;

% taskinfo
updateTaskInfo = 0;
% tetinfo
updateTetInfo = 0;
%cellinfo
updateCellInfo = 1;

% Detect events
extractNoiseKonsensus = 0;
extractRipplesKonsensus = 0;

% Debugging 
checkforcorrupteddata = 0;
%% run Trodes_dayprocess with the vars above per day
runDayPreProcess