function adjust_timestamps(animaldir)
% go through all of the epochs of data for specified animal directory and adjust the timestamps in all relevant files to
% ensure that the epochs are sequential in time with 5 minute gaps between

%make adjust_timestamps data types independent of each other's 'timestamps' var
adjust_mda = 1; %unfortunately this isn't optional bc use of first tmie as start for the rest
adjust_lfp = 1;
adjust_time = 0; %no one is using this yet
adjust_dio = 1;
adjust_spikes = 1; 
adjust_pos = 1;

suffix = '.adj';
epoch_interval = 5 * 60 * 30000; % five minute space between epoch
cwd = pwd;

cd(animaldir); %session dir

% get a list of the mda directories
mda_dir_list = dir('*.mda'); % one .mda dir / epoch

toffset = 0;
% go through each of the directories in turn
for enum = 1:length(mda_dir_list) %loop over epochs
    disp(sprintf('adjust_timestamp processing epoch %d', enum));
    tmp = strsplit(mda_dir_list(enum).name, '.');
    % get the basename for this set of directories so we can create the various names we need for different
    % datatypes
    basename{enum} = tmp{1};
    
    
    % STEP 1: get the times from the current epoch's mda format timestamp file, adjust the timestamps and rewrite the file with a suffixd
    if adjust_mda
        if isdir(strcat(basename{enum}, '.mda'))
            cd(strcat(basename{enum}, '.mda')); %session/epoch
            % open the timestamps files
            timestamp_file = dir('*timestamps.mda'); %should only be one unadjusted timestamps file
            if (length(timestamp_file) > 1) %this shouldn't occur anymore...
                warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(timestamp_file), pwd, timestamp_file(end).name));
            end
            disp(sprintf('Processing mda timestamp file %s', timestamp_file(end).name));
            mdatimestamps = readmda(timestamp_file(end).name);
            mdatimestamps = check_for_backwards_timestamps(mdatimestamps);
            % : 
            starttime = mdatimestamps(1);
            mdatimestamps = mdatimestamps - starttime + toffset;
            disp(sprintf('epoch num %d start time: %.02f min', enum, starttime/30000/60))
            endtime = mdatimestamps(end);
            disp(sprintf('MDA--> ep %d; offset %5.02f ; mda start: %5.02f ; adj start %5.02f ; adj end %5.02f (s)', enum, toffset, starttime/30000, mdatimestamps(1)/30000, mdatimestamps(end)/30000))

            %newtsfile = (timestamp_file(end).name,suffix);
            newtsfile = newfilename(timestamp_file(end).name, suffix);
            % write the new adjusted timestamp file
            writemda(mdatimestamps, newtsfile, 'uint32');
            cd ..;
        else
            disp(sprintf('ERROR: no .mda dir detected for epoch #%d ... extract mda before proceeding',enum))
            return
        end
    end
    
    
    % STEP 3: read in and adjust the LFP timestamps
    if adjust_lfp
        lfp_dir = dir(sprintf('%s*.LFP', basename{enum}')); %session / epoch
        if ~isempty(lfp_dir);
            cd(lfp_dir.name);
            % open the timestamp files
            lfptimestamp_file = dir('*timestamps.dat');  %should only be one unadjusted timestamps file
            if ~isempty(lfptimestamp_file);
                if (length(lfptimestamp_file) > 1) %this checks for multiple unadjusted timestamp.dat files and uses the last one
                    warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(lfptimestamp_file), pwd, lfptimestamp_file(end).name));
                end
                adjlfptimestamp_file = dir('*timestamps.adj.dat');
                if ~isempty(adjlfptimestamp_file)
                    disp(sprintf('adjusted LFP timestamps detected, overwriting for epoch#%d',enum))
                else
                    disp(['Processing LFP timestamp file ' lfptimestamp_file(end).name]);
                end
                
                lfpdata = readTrodesExtractedDataFile(lfptimestamp_file(end).name);
                for i = 1:length(lfpdata.fields)
                    if strcmp(lfpdata.fields(i).name,'time') %find the timestamps field
                        lfptimestamps = double(lfpdata.fields(i).data);
                    end
                end
                lfptimestamps = check_for_backwards_timestamps(lfptimestamps);
                
                % adjust the timestamps
                unadj_start = lfptimestamps(1);
                lfptimestamps = lfptimestamps - starttime + toffset;

                disp(sprintf('LFP--> ep %d; offset %5.02f ; lfp start: %5.02f ; adj start %5.02f ; adj end %5.02f (s)',...
                    enum, toffset, unadj_start/30000, lfptimestamps(1)/30000, lfptimestamps(end)/30000))
                
                % write out the new file
                fileID = fopen(lfptimestamp_file(end).name, 'r');
                header = readtrodesexportheader(fileID);
                fclose(fileID);
                newtsfile = newfilename(lfptimestamp_file(end).name, suffix);
                fileID = fopen(newtsfile, 'w');
                % write the header. Note that we're not updating the Time offset field at the moment
                writetrodesexportheader(fileID, header);
                
                % write the data
                fwrite(fileID, lfptimestamps, 'uint32');
                fclose(fileID);
                cd ..
                %         end
            else
                cd ..
                disp(sprintf('no .LFP timestamp file detected for epoch #%d',enum))
            end
        else
            disp(sprintf('no .LFP dir detected for epoch #%d',enum))
        end
    end
    % Step 4: read in and adjust the position timestamps (note that there can be multiple position directories for a
    % given epoch
    if adjust_pos
        pos_dir = dir(sprintf('%s*.pos', basename{enum}')); %session / epoch
        if ~isempty(pos_dir);
            cd(pos_dir.name); %session / epoch
            % open the timestamp files
            postimestamp_file = dir('*timestamps.dat');
            if ~isempty(pos_dir);
                if (length(postimestamp_file) > 1) %this checks for multiple unadjusted timestamp.dat files and uses the last one
                    warning(sprintf('multiple timestamp files (%d) in %s, using %s', length(postimestamp_file), pwd, postimestamp_file(end).name));
                end
                adjpostimestamp_file = dir('*timestamps.adj.dat');
                if ~isempty(adjpostimestamp_file) % this checks for a timestamps.adj.dat file
                    disp(sprintf('adjusted pos timestamps detected, overwriting for epoch#%d',enum))
                else
                    disp(['Processing timestamp file ' postimestamp_file(end).name]);
                end
                % read in the timestamps
                fileID = fopen(postimestamp_file(end).name, 'r');
                header = readtrodesexportheader(fileID);
                fclose(fileID);
                [postimestamps clockRate] = readCameraModuleTimeStamps(postimestamp_file(end).name);
                postimestamps = postimestamps*clockRate; %convert back to uint timestamps
                % adjust the timestamps
                postimestamps = postimestamps - starttime + toffset;
                % write out the new file
                newtsfile = newfilename(postimestamp_file(end).name, suffix);
                fileID = fopen(newtsfile, 'w');
                % write the header. Note that we're not updating the Time offset field at the moment
                writetrodesexportheader(fileID, header);
                % write the data
                fwrite(fileID, postimestamps, 'uint32');
                fclose(fileID);
                cd ..
                %         end
            else
                cd ..
                disp(sprintf('no .pos files detected for epoch #%d',enum))
            end
        else
            disp(sprintf('no .pos dir detected for epoch #%d',enum))
        end
    end
    % STEP 5: read in and adjust the DIO timestamps
    if adjust_dio
        dio_dir =dir(sprintf('%s*.DIO', basename{enum}')); %session / epoch
        if ~isempty(dio_dir);
            %         dispDIOoverwritewarning = 1;
            cd(strcat(basename{enum}, '.DIO'));
            % go through the files one by one
            dio_files = dir('*dio*.dat'); %there is a DIO files for each of the DIO's in each epoch
            %need to fix in future...this should only return the non-adjusted files instead of the hack below
            if ~isempty(dio_files);
                for dio_ind = 1:length(dio_files)
                    % check to see if this is an already adjusted file; if so, report once that it will get over written..
                    % This functions on the unadjusted files, and then saves them over any previous adjusted files
                    if ~isempty(strfind(dio_files(dio_ind).name,'adj')) %this will skip all the already adj files
                        %                 %The warning message is not really necessary..
                        %                 if dispDIOoverwritewarning
                        %                     disp(sprintf('adjusted DIO timestamps detected, overwriting for epoch#%d',enum))
                        %                     dispDIOoverwritewarning = 0; %suppress further overwrite warnings for this epoch
                        %                 end
                        continue
                    else
                        if dio_ind == 1
                            disp(['Processing dio files']);
                        end
                        diodata = readTrodesExtractedDataFile(dio_files(dio_ind).name);
                        for i = 1:length(diodata.fields)
                            if strcmp(diodata.fields(i).name,'time') %find the timestamps field
                                diotimestamps = diodata.fields(i).data;
                            elseif strcmp(diodata.fields(i).name, 'state') %find the state field
                                state = diodata.fields(i).data;
                            end
                        end
                        diotimestamps = diotimestamps - starttime + toffset;  % adjust the timestamps 
                        
                        % %copy out the header
                        fileID = fopen(dio_files(dio_ind).name, 'r');
                        header = readtrodesexportheader(fileID); 
                        fclose(fileID);
                        newdiofile = newfilename(dio_files(dio_ind).name, suffix);
                        fileID = fopen(newdiofile, 'w');
                        writetrodesexportheader(fileID, header);
                        
                        % matlab doesn't allow writes of different datatypes, so we typecast everything to uint8, append and then write
                        tsuint32 = cast(diotimestamps, 'uint32');
                        tstmp = typecast(tsuint32, 'uint8');
                        ntimestamps = length(diotimestamps);
                        tstmp = reshape(tstmp, 4, ntimestamps);
                        if length(tstmp(1,:)) ~= length(state) || any(diotimestamps(:) <0)
                            error('discrepency between state and timestamps or negative timestamps..')
                        else
                            tstmp(5,:) = state;
                        end
                        fwrite(fileID, tstmp, 'uint8');
                        fclose(fileID);
                    end
                end
                cd ..
            else
                cd ..
                disp(sprintf('no .DIO files detected for epoch #%d',enum))
            end
        else
            disp(sprintf('no .DIO dir detected for epoch #%d',enum))
        end
    end
    
    % STEP 6: read in and adjust the .time files (these just list start and end time of each epoch)
    if adjust_time
        time_dir =dir(sprintf('%s*.time', basename{enum}')); %session / epoch
        if ~isempty(time_dir);
            cd(strcat(basename{enum}, '.time'));
            time_file = dir('*time*.dat');
            if ~isempty(time_file);
                disptimeoverwrite = 1;
                % go through the files one by one
                time_file = dir('*time*.dat');
                for time_ind = 1:length(time_file)
                    %check to see if this is an already adjusted file; if so, report, skip it.. it will get over written
                    if ~isempty(strfind(time_file(time_ind).name,'adj')) %this will skip all the already adj files
                        if disptimeoverwrite
                            disp(sprintf('adjusted time file detected, overwriting for epoch#%d',enum))
                            disptimeoverwrite = 0; %suppress further overwrite warnings for this epoch
                        end
                    else
                        if time_ind == 1
                            disp(['Processing time files but beware, this does not currently work']);
                        end
                        fileID = fopen(time_file(time_ind).name, 'r');
                        header = readtrodesexportheader(fileID);
                        offset = ftell(fileID);
                        % read in the timestamps
                        timetimestamps = fread(fileID,'uint32');  
                        % adjust the timestamps
                        timetimestamps = timetimestamps - starttime + toffset;
                        fclose(fileID);
                        % open up the new files
                        newtimefile = newfilename(time_file(time_ind).name, suffix);
                        fileID = fopen(newtimefile, 'w');
                        writetrodesexportheader(fileID, header);
                        % matlab doesn't allow writes of different datatypes, so we typecast everything to uint8, append and then write
                        tsuint32 = cast(timetimestamps, 'uint32');
                        tstmp = typecast(tsuint32, 'uint8');
                        ntimestamps = length(timetimestamps);
                        tstmp = reshape(tstmp, 4, ntimestamps);
                        fwrite(fileID, tstmp, 'uint8');
                        fclose(fileID);
                    end
                end
                cd ..
            else
                cd ..
                disp(sprintf('no .time files detected for epoch #%d',enum))
            end
        else
            disp(sprintf('no .time dir detected for epoch #%d',enum))
        end
    end
    % STEP 7: read in and adjust spike files
    if adjust_spikes
        spikes_dir =dir(sprintf('%s*.spikes', basename{enum}')); %session / epoch
        if ~isempty(spikes_dir);
            cd(strcat(basename{enum}, '.spikes'));
            spikes_file = dir('*spikes*.dat');
            if ~isempty(spikes_file);
                disp('partially debugged AKG Mar2018 mostly good, occaisional fails in identifying correct location of header end')
                dispspikesoverwrite = 1;
                % go through the files one by one
                for spikes_ind = 1:length(spikes_file)
                    %check to see if this is an already adjusted file; if so, report, skip it, and overwrite
                    if ~isempty(strfind(spikes_file(spikes_ind).name,'adj')) %this will skip all the already adj files
                        if dispspikesoverwrite
                            disp(sprintf('adjusted spikes timestamps detected, overwriting for epoch#%d',enum))
                            dispspikesoverwrite = 0; %suppress further overwrite warnings for this epoch
                        end
                    else
                        if spikes_ind == 1
                            disp(['Processing spike files'])
                        end
                        spkdata = readTrodesExtractedDataFile(spikes_file(spikes_ind).name);
                        fileID = fopen(spikes_file(spikes_ind).name, 'r');
                        header = readtrodesexportheader(fileID);
                        offset = ftell(fileID);
                        headerparse = strfind(header,'num_channels');
                        numchanind = find(~cellfun('isempty', headerparse));
                        num_chans = regexp(header{numchanind},'\d*','Match');
                        num_chans = str2num(num_chans{1});
                        %check for non-tetrodes and skip
                        if num_chans ~= 4;
                            disp(sprintf('SKIPPING......%s is not a tetrode',spikes_file(spikes_ind, 1).name))
                            continue
                        end
                        % read in the timestamps and then the status
                        % 40 samples in a waveform; 2bytes per int16 (waveforms); 4 bytes per uint34 (timestamps)
                        ntrodeseek = 40*num_chans;
                        spiketimestamps = fread(fileID, Inf,'uint32', ntrodeseek * 2); % skip 320 bytes to get to the next timestamp, 
                        lastbyte = ftell(fileID);
                        % adjust the timestamps
                        spiketimestamps = spiketimestamps - starttime + toffset;
                        
                        badtimestamps = find(diff(spiketimestamps) <= 0);
                        if badtimestamps 
                            offset=offset-1;
                            fseek(fileID,offset,'bof');
                            spiketimestamps = fread(fileID, Inf,'uint32', ntrodeseek * 2); % skip 40 points for each channel, each an int16
                            badtimestamps = find(diff(spiketimestamps) <= 0);
                            if isempty(badtimestamps)
                                disp('AKG fix; headerend was 1 byte off...') %was remy s23 nt4, s22nt20
                            else 
                                warning(sprintf('BADTIMESTAMPS IN %s, unfixable by headerend reset',spikes_file(spikes_ind, 1).name))
                                fprintf('continue and retain %d bad timestamps?', length(badtimestamps))
                                pause
                            end                           
                        end
                        ntimestamps = length(spiketimestamps);
                        % read in the waveforms
                        fseek(fileID, offset+4, 'bof');
                        spikewf = fread(fileID, Inf,'160*int16', 4);  % skip 4 bytes between each read (the timestamps)
                        fclose(fileID);
                        
                        %check that the file doesn't end with uneven data across channels; truncate if it does.
                        if mod(length(spikewf),160)>0  
                            disp('incomplete data. truncating last few bytes')
                            %remove the last timestamp
                            spiketimestamps = spiketimestamps(1:end-1);
                            ntimestamps = length(spiketimestamps);
                            spikewf = spikewf(1:ntimestamps*160);  %160 because still dealing with int16s, not int8
                        else
                            endfile = Inf;
                        end
                        newspikesfile = newfilename(spikes_file(spikes_ind).name, suffix);
                        % matlab doesn't allow writes of different datatypes, so we typecast everything to uint8, append and then write
                        tsuint32 = cast(spiketimestamps, 'uint32');
                        tstmp = typecast(tsuint32, 'uint8');
                        tstmp = reshape(tstmp, 4, ntimestamps);
                        spikewfint16 = cast(spikewf, 'int16');  
                        spikewftmp = typecast(spikewfint16, 'uint8');
                        spike_extract_failed = 0;
                        try
                            spikewftmp = reshape(spikewftmp, 320, ntimestamps);
                        catch
                            spike_extract_failed = 1;
                            error(sprintf('spikes file (%s) failed reshape, probably is not a tetrode. adjust_timestamps currently requires 4 channels/ntrode, skipping.', spikes_file(spikes_ind).name));
                        end
                        
                        if ~spike_extract_failed
                            % open up the new files
                            fileID = fopen(newspikesfile, 'w');
                            writetrodesexportheader(fileID, header);
                            tstmp(5:324,:) = spikewftmp;
                            fwrite(fileID, tstmp, 'uint8');
                            fclose(fileID);
                        end
                    end
                end
                cd ..
            else
                cd ..
                disp(sprintf('no .spikes files detected for epoch #%d',enum))
            end
        else
            disp(sprintf('no .spikes dir detected for epoch #%d',enum))
        end
    end
    
    
    disp('updating toffset')
    toffsetlog(enum) = toffset;
    toffset = toffset + (mdatimestamps(end)-mdatimestamps(1)) + epoch_interval;
    disp(sprintf('toffset = %d timestamps or %.03f minutes',toffset,toffset/30000/60))
    if toffsetlog(enum) >= toffset || toffsetlog(enum) < 0
        error('time offsets must be monotonically increasing... it''s a trap!! ABORT')
    end
end

end

function timestamps = check_for_backwards_timestamps(timestamps)
backward_idx = find(diff(timestamps) <= 0);
if backward_idx
    fprintf('%d backwards timestamps found timestamps must be monotonically increasing...\n',...
        length(backward_idx))
    if ~ismember(1,backward_idx)
        fprintf('beginning timestamps are not backwards \n')
        fprintf('press any key to continue with synthesizing timestamps or ctrl-c to abort');
        pause
        step = mode(diff(timestamps));
        fprintf('synthesizing timestamps based on a constant step size (mode of diff %d) and length of timestamps\n', ...
            step)
        starttimestamp = timestamps(1);
        timestamps = double(starttimestamp) + ([0:length(timestamps)-1] * double(step));
    else
        error('backwards timestamps at beginning preventing synthesizing timestamps')
    end
end
end

function newname = newfilename(name, suffix)
% put in the suffix before the final .dat or .mda
% parse the filename into
parts = strsplit(name, '.');
newname = parts{1};
for i = 2:(length(parts)-1)
    newname = strcat(newname, '.', parts{i});
end
% add in the suffix
newname = strcat(newname, suffix, '.', parts{end});
end


