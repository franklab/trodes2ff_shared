function [ptNew, ptNew_pi, out] = pos_realign_camlog(...
    srcdir, destdir, animalID, sessionNum, epochnum, epochname, ...
    pt, ...
    camDioCh_override,...
    fit_trodes_offset_override,...
    ignoreSegs_pi,...
    use_dios)

%% INPUTS:
% srcdir, destdir, animalID, sessionNum, epochnum, epochname : Used to
%     locate camlog/DIO input files. No Defaults
% pt: Times to be corrected: Trodes time of each video frame, as reported 
%     by cameraModule. (i.e. contents of *.pos_timestamps.adj.dat)
% camDioCh_override: DIO channel corresponding to timing pulses
%     from camera. Default: [] = automatically detect correct channel.
% fit_trodes_offset_override: (optional) Specify average lag from camera
%     frame acquisition to (fitted) Trodes time observed by cameraModule. Used
%     when there is no usable timing gap at beginning of file (e.g. when camera
%     DIOs are missing). Default: [] = use timing gap.
% ignoreSegs_pi: segments of adjusted time to censor/ignore (specified as
%     indexes into pt). Used for periods of unrecoverable corruption. 
%     Default: [] = don't ignore any segments.
% use_dios: Whether to attempt to use camera DIOs to estimate
%     timing lag. Default = true.

%% OUTPUTS:
% ptNew: corrected times (in seconds since T_day_zero; i.e. 'adjusted')
% ptNew_pi: *logical* index into the input 'pt' corresponding to corrected
%     ptNew output. (I.e. index of which video frames were successfully
%     aligned)
% out: catch-all output struct with useful diagnostics/intermediate results

%% TODO
%  -Refactor to get rid of ugly pt_validi/dio_validi hackery (use logical index?)
%    -Deal with pt_validi strangeness in chunkwise freeze handling
%  -Sanity check fixed framerate using DIOs when synthesizing HWclock
%  -Improve Trodes Freeze handline
%     -Test for clock recovery (latest MCU ~1/2018 does this)
%  -Add back in plots when DIOs not present (just don't plot them)
%  Extra credit:
%  -Add to plots:
%    -chunk fits as line in offset plot
%    -indicate ignored segments/freezes
%  -sanity check for non-linear clock drift (moving average of
%   postimestamps vs. linear fit?; use DIOs for this?).
%     -put bounds on error, a la max_ts_err field in contstructs
%     -fix it with piecewise regressions if necessary

%% LATER
% -validate inputs
% -plot diagnostics with linked lines for first/last 20 frames, overall.
% -log to 'diary', or write logging/display function  
% -Handle no-hwlog case by fitting multiple lines with fixed framerate
%  (will break down if dropped frames are very frequent)

%% MAYBE

%% NOPE?

%% DONE
%  -add diagnostic plots
%  -replace main logic with regression of camHWclock and TrodesTime
%   -delete invalid TrodesTime values (i.e. repeats during freezes)
%   -regress HWclock and Trodes time
%   -correct offset between camHWclock/postime using corrected postime vs.
%    single DIO (single-point alignment--use alignment gap/'pause' at 
%    beginning of file)
%   -piecewise regression if Trodes Freezes (and clock alignment is lost)
%  -synthesize HWclock from HWFrameCount if nec 
%  -TEST in file with multiple freezes (Remy?)
%  -TEST min_chunklen handling
%  -Handle no DIO case (require latency as parameter, possibly obtained 
%   from manual synchronization with a video frame)

%%%%%%
diary on
fprintf('\n----REALIGNING %s s%d ep%d--------------------\n',animalID, sessionNum, epochnum);

%%%%

%% set up parameters (some of these could be inputs)

% expected camera framerate
%cam_framerate = 29.7; % (Hz) % nope--learn from DIOs or postimestamps
ratecheck_framerate_tol = 0.15; % framerate mode +/- 20 percent

trodes_clockrate = 30000; % (Hz)

% Landmark timing 'gap' (pause in video stream) parameters
gap_min_dur = 0.4; % min duration of the gap (s)
gap_max_dur = 1; % max duration of the gap (s)
gap_nsearch = 100; % Only look in the first n DIO/pos entries for a gap

% expected min/max lag from DIO to frame arrival time (postimestamp)
min_latency = 0.010; % hard minimum latency expected from DIO to postimes

% On AVT cameras, HWFrame counts wrap to 0 above this value
AVT_camHWframeCount_wrapval = 65535;

% Min length of chunk before attempting regression
min_chunklen = 20;

% save figure with realignment diagnostics
save_realignment_diagnostics = false;

%% Validate inputs, set up paths

if ~exist('camDioCh_override', 'var')
  camDioCh_override = [];
end

if ~exist('ignoreSegs_pi') || isempty(ignoreSegs_pi)
  ignoreSegs_pi = [];
else
  assert(size(ignoreSegs_pi,2)==2,'''ignoreSegs'' must be m x 2 vector');
  assert(~any(mod(ignoreSegs_pi,1)),'''ignoreSegs'' must be integer-valued (index into postimestamps)');
end  

if ~exist('use_dios') || isempty(use_dios)
  use_dios = true;
end

if ~exist('fit_trodes_offset_override'),
  fit_trodes_offset_override = [];
end

if ~isempty(fit_trodes_offset_override)
  if fit_trodes_offset_override>0,
    error('fit_trodes_offset_override must be negative (camera frames must occur before postimestamps)');
  end
end

pos_basename = epochname(1:end-4);
posdir = [srcdir '/' pos_basename '.pos'];

camlogfile = [posdir '/' pos_basename '.pos_cameraHWFrameCount.dat'];
camsyncfile = [posdir '/' pos_basename '.pos_cameraHWSync.dat'];

DIOfile = [destdir '/' sprintf('%sDIO%02d.mat',animalID, sessionNum)];

assert(isa(pt, 'double'), 'postimestamps should be of type ''double''.');
if ~(issorted(pt)); %issorted wasn't catching repeats
    % neural data and dios stop being written but pt repeats, then resets
    % all pt once initial repeat occurs should be invalidated
    % put warning message later, when we're actually handling this
    unsorted = 1;
else
    unsorted = 0;
end

fprintf('# of postimestamps in original file = %d\n', numel(pt));

% Save a copy of inputs (which may change during alignment;
out.pt = pt;
out.ignoreSegs_pi = ignoreSegs_pi;

%%%%%
% Load and proces input files, if available:

%% Load and process camera hardware info (required)

if exist(camlogfile, 'file')
  
  out.camlog_struct = readTrodesExtractedDataFile(camlogfile);

  camHWframeCount = double(out.camlog_struct.fields(strmatch('frameCount', {out.camlog_struct.fields.name})).data);
  camHWclock = [];
  camlogPt = [];
  
elseif exist(camsyncfile, 'file')

  out.camsync_struct = readTrodesExtractedDataFile(camsyncfile);  
  
  camHWframeCount = double(out.camsync_struct.fields(strmatch('HWframeCount', {out.camsync_struct.fields.name})).data);

  camHWclock = double(out.camsync_struct.fields(strmatch('HWTimestamp', {out.camsync_struct.fields.name})).data);
  camHWclock = camHWclock-camHWclock(1); % convert relative to first frame time
  camHWclock = camHWclock .* 1e-9; % convert from nanoseconds to seconds

  % get camlogPt in seconds (NB: unadjusted)
  camlogPt = double(out.camsync_struct.fields(strmatch('PosTimestamp', {out.camsync_struct.fields.name})).data);
  camlogPt = camlogPt./out.camsync_struct.clockrate;
  
  % the camlog postimestamps should be same as the loaded
  % postimestamps (except for offset during adjustment)
  assert(numel(camlogPt)==numel(pt), 'postimestamp input should be same size as camlog file');
  assert(all(diff(camlogPt-pt) < 1e-6), '(adjusted) postimestamp inputs should have fixed offset from camlog file postimestamps');
  
  %  AG edit -- if trodes got restarted, adjusted time could be before OR after unadjusted time; should always use adjust time instead
  %if camlogPt(1)-pt(1) > 1e-6 
    % replace camlogPt with pt, since camlogPt is not currently adjusted
    % during import.
    fprintf('postimestamps inside cameraHWSync file are unadjusted--using adjusted times instead.\n');
    camlogPt=pt;
  %end
  
else
  warning('No camera HW log/sync file found. Tried:\n %s \n %s \n',...
    camlogfile, camsyncfile);
  error('Camera HW log required to run this version of pos_realign: see warning above')
 
end

% On AVT cameras, HWFrame counts wrap to 0 above a fixed value (usually
% intmax(uint16) = 65535), so unwrap them.
assert(isvector(camHWframeCount),'camHWcounts must be 1-D array (vector)');
assert(numel(camHWframeCount)==numel(pt), 'Mismatch between camHWcounts and postimestamps.');


camHWframeCount = unwrap_inc(camHWframeCount', AVT_camHWframeCount_wrapval);
assert(issorted(camHWframeCount), 'camHWframeCount entries are out of order after unwrapping');

% save out diagnostics
out.n_camHWframeCount = size(camHWframeCount,1);


%%
% Load exported DIOs

if exist(DIOfile, 'file')
  
  tmp = load(DIOfile, 'DIO');
  epDIO = tmp.DIO{sessionNum}{epochnum};
  clear tmp;
  
else
  epDIO = [];
  warning(sprintf('No DIO file found for this session at location: %s', DIOfile));
  use_dios = false;
end


%%%%
% Initial checks of dios + postimestamps

% keep track of valid postimestamps (formerly 'newTimesInds')
% (use array indexing instead of logical to simplify code later)
pt_validi = 1:numel(pt);

cam_framerate = 1./median(diff(pt));
fprintf('Cam_framerate estimated from postimestamp median diff: %0.4f fps\n',cam_framerate);
  
if use_dios
  if isempty(camDioCh_override)
    disp('No camera DIO channel provided, guessing from DIO rates...');
    camDioCh_override = subf_findCamDioCh(epDIO,...
      cam_framerate,...
      ratecheck_framerate_tol,fit_trodes_offset_override);
  else
    warning('Using user-provided camDioCh_override: %d\n', camDioCh_override)
  end
  if ~isempty(camDioCh_override) % able to find a suitable dio
      % use time of rising transitions
      dios = epDIO{camDioCh_override}.times(epDIO{camDioCh_override}.values==1);
  else
      dios = [];   % was unable to find a suitable dioCh
  end
  
else
  dios = [];
end  
% if dios is empty or less than 10% the size of frames
if isempty(dios) || length(dios) < round(length(pt)/10)
  use_dios = false;
end

if use_dios
  % sanity check
  assert(issorted(dios), 'dios must be non-decreasing');
  
  % keep track of valid DIOs for alignment
  dio_validi = 1:numel(dios);
  
  % get cam_framerate based on median inter-dio time
  cam_framerate = 1./median(diff(dios(dio_validi)));
  fprintf('Cam_framerate refined from DIOs: %0.4f fps\n',cam_framerate);
  
else
  fprintf('**Not using DIOS**');
  dios = [];
  dios_validi = [];
end

out.dios = dios; % save out

if ~use_dios && isempty(fit_trodes_offset_override)
  error('If DIOs not available, must provide alignment time with ''fit_trodes_offset_override''');
end

if use_dios && ~isempty(fit_trodes_offset_override);
  warning('Not using fit_trodes_offset_override because DIOs are present!');
end

if unsorted
    if use_dios 
        warning('unsorted timestamps due to "error in data acquisition" message; invalidating those that extend past dios')
        endvalidind = find(pt>dios(end),1);
        pt_validi = 1:endvalidind;
    else
        error('unsorted timestamps due to "error in data acquisition" message; cant handle this without dios')
    end
end

%% Check for large > 0.5s jumps in postimestamps

ptvalid = pt(pt_validi);
pjumps = find(diff(ptvalid)>.5); 
pjumps = pjumps(pjumps>30);% gaps that aren't the alignment gap
if ~isempty(pjumps)
    fprintf('%d time jump(s) >.5s detected..\n', numel(pjumps))
    for i = 1:numel(pjumps)
        jdur = ptvalid(pjumps(i)+1) - ptvalid(pjumps(i));
        fprintf('jump %d / %d : %d seconds \n', i, numel(pjumps), jdur)
    end
    fprintf('treat the jumps as freezes to clip the regression\n')
    ignoreSegs_pi = [ignoreSegs_pi; pjumps'-1 pjumps'];
end

%% Check for large > 15 frame jumps in frame HW count
% this should catch instances where the unwraping creates
% large gaps in frame count because the count at time of reset was 
% not the unwrap val

fcjumps = find(diff(camHWframeCount)>15); 
fcjumps = fcjumps(fcjumps>30); % gaps that aren't the alignment gap
if ~isempty(fcjumps)
    fprintf('%d frame count jump(s) >15 detected..\n', numel(fcjumps))
    fprintf('treat the jumps as freezes to clip the regression\n')
    ignoreSegs_pi = [ignoreSegs_pi; fcjumps'-1 fcjumps'];
end

%% Invalidated 'ignored' times

% Currently hack this by creating a spurious 'Trodes Data Freeze'. This
% forces the segments before/after each ignored region to be aligned
% separately without adding any additional logic.

for k = 1:size(ignoreSegs_pi,1)
  warning('Ignoring region from pt index %d-%d (uncorrected t = %0.4f-%0.4). Will appear as ''Trodes Data Freeze''',...
    ignoreSegs_pi(k,:), pt(ignoreSegs_pi(k,:)));
  pt(ignoreSegs_pi(k,1):ignoreSegs_pi(k,2)) = pt(ignoreSegs_pi(k,1));
end


%% Detect acquisition pause ('gap') at start of file
%
% Trodes versions since late 2016 deliberately introduce a 0.5 second gap at the start
% of video recordings to help align DIOs and postimestamps.

% Detect gap in postimestamps
% (returns index (into pt_valid frames) of last frame before gap)
[FrameBeforeGap_pvi, framegapdur] = subf_gapdetect(pt(pt_validi), gap_nsearch, gap_min_dur, gap_max_dur);

% Save index (into original postimestamps) of frame after gap
FrameAfterGap_pi = pt_validi(FrameBeforeGap_pvi+1);

if FrameBeforeGap_pvi % found a gap
  fprintf(['Alignment gap found in postimestamps: duration %0.2fs; \n'...
           '... first valid frame after gap is #%d at t=%0.4f; \n'...
           '... ** censoring earlier frames **.\n'],...
    framegapdur,...
    FrameAfterGap_pi,...
    pt(FrameAfterGap_pi));
  
  % Ignore frames before gap
  pt_validi(1:FrameBeforeGap_pvi) = [];

else
  fprintf(['No alignment gap in frames found; using all valid frames. \n'...
      '... First valid frame is at t=%0.4f\n'], ...
      pt(pt_validi(1)));
end

% Detect DIO gap:

if use_dios

  [DioBeforeGap_dvi, diogapdur] = subf_gapdetect(dios(dio_validi), gap_nsearch, gap_min_dur, gap_max_dur);
  
  % Handle case where no gap found:
  if isempty(DioBeforeGap_dvi)
      disp('No alignment gap in dios found; starting with first DIO')
      DioBeforeGap_dvi = 0;
  end
  
  % Save dios index after gap
  DioAfterGap_di = dio_validi(DioBeforeGap_dvi+1);
  
  if DioBeforeGap_dvi % found a gap
      fprintf(['Alignment gap found in DIOs: duration %0.2fs;\n'...
          '... first pulse after gap is #%d at t=%0.4f\n'...
          '... ** censoring earlier DIOS **\n'],...
          diogapdur,...
          DioAfterGap_di,...
          dios(DioAfterGap_di));
      
      % Invalidate pre-gap DIOs (Does nothing if DioBeforeGap_dvi = 0)
      dio_validi(1:DioBeforeGap_dvi) = [];
      
  else
      fprintf('No alignment gap in DIOS found; using first DIO (at t=%0.4f)\n', dios(dio_validi(1)))
  end
    
  if pt(pt_validi(1))<(dios(dio_validi(1))+min_latency)
    if DioBeforeGap_dvi, s1 = 'post-gap '; else s1 = 'valid '; end
    if FrameBeforeGap_pvi, s2 = 'post-gap '; else s2 = 'valid '; end
    warning('First %sDIO (t=%0.4fs + min_latency %0.4fs) appears *after* first %sframe (t=%0.4fs). ignoring DIOs and using manual offset_override',...
      s1, dios(dio_validi(1)), min_latency, s2, pt(pt_validi(1)));
    use_dios=false;
    
  end
    
  fprintf('\n');


end  
  

%%%%
% finally do some alignment

%%% synthesize cameraHWClock from frameCount, if we only have frameCounts
% (or we could just let the regression handle this)

if isempty(camHWclock),
  warning('synthesizing camHWclock from camHWframeCount, may fail if camera framerate is not a fixed value');

  % start clock at 0, just like camHWclock
  camHWclock = double(camHWframeCount-camHWframeCount(1))' ./ cam_framerate;
  camlogPt = pt;
  
end

out.camHWclock = camHWclock;
out.camHWframeCount = camHWframeCount;
out.camlogPt = camlogPt;

%% Align position times using regression method

% Do regression piecewise, between possible Trodes Freezes (treat each chunk as a
% separate alignment, but reuse the alignment gap timing from the first
% chunk).

% Detect repeated postimestamps due to Trodes Freezes (return value
% includes first frame in repeat, since it may be invalid)
[freezes_pvi] = subf_findTrodesFreezes(pt_validi, pt, trodes_clockrate, cam_framerate);

% keep a copy for debugging, since we do hacky stuff below:
out.freezes_pvi_copy = freezes_pvi;

% Convert list of 'freezes' (frames with repeated indexes) into valid
% chunks to do piecewise regression on

nfreezes = size(freezes_pvi,1);


if nfreezes == 0
  chunks_pvi = [1 numel(pt_validi)];
  fprintf('No freezes detected, include all valid data in one chunk.\n')

else
  
  chunks_pvi = [];
  chunk_first_good_pvi = 1;
  
  for j = 1:nfreezes
    fprintf('Processing chunks for freeze #%d of %d\n', j, nfreezes');
    
    % exclude weird case where we *begin* file during a freeze
    if freezes_pvi(1,1) ~= 1
      chunks_pvi = [chunks_pvi; chunk_first_good_pvi freezes_pvi(j,1)-1];
      fprintf('Chunk preceding Freeze #%d: pt_validi = %d-%d; time = %0.4f-%0.4f s\n',...
        j, chunks_pvi(j,:),pt(pt_validi(chunks_pvi(j,:))));
    else
      fprintf('Weird: video begins during a Trodes Data Freeze.\n');
    end
  
    % invalidate freeze period (doesn't happen in subf anymore)
    freeze_pvi_list = freezes_pvi(j,1):freezes_pvi(j,2);
    pt_validi(freeze_pvi_list) = [];

    % hackery: freeze indexes are into pt_validi, but we just hacked out a
    % part of pt_validi. Decrement freeze list to account for this in later
    % loops.
    freezes_pvi = freezes_pvi-numel(freeze_pvi_list);

    % post-hackery, we can get accurate index into new_pvi for next chunk
    chunk_first_good_pvi = freezes_pvi(j,2)+1;
    
  end
  
  % append chunk after last freeze (unless freeze continues to end of
  % video)
  if chunk_first_good_pvi <= numel(pt_validi)
    chunks_pvi = [chunks_pvi; chunk_first_good_pvi numel(pt_validi)];
    try
        fprintf('Appending last chunk after Freeze #%d: pt_validi = %d-%d; time = %0.4f-%0.4f s\n',...
          j, chunks_pvi(j+1,:),pt(pt_validi(chunks_pvi(j+1,:))));
    catch
        fprintf('The only freeze is at beginning of video file \n');
    end
  else
    fprintf('Last freeze ends at end of video file. No subsequent data chunk.\n');
  end
  
end

% weird case when there are two different freezes in a row
% the first freeze entry of them will capture the combined freeze
% so just drop the second entry which will have a negative end - start
chunks_pvi = chunks_pvi(chunks_pvi(:,1) <= chunks_pvi(:,2),:);


% This doesn't work until we refactor pvi hackery :(
% % convert 'freezes' to non-freeze chunks (bracket with start/end indexes)
% chunks_pvi2 = [1 reshape(freezes_pvi',1,[]) numel(pt_validi)];
% chunks_pvi2 = reshape(chunks_pvi2,2,[])';
% % Then test for zero-diff (or min_chunklen) chunks (freeze starts at 1 or ends at END)
% chunklen2 = diff(chunks_pvi2,[],2);
% chunks_pvi2(chunklen<min_chunklen,:) = [];

nchunks = size(chunks_pvi,1);

if ~use_dios
  fprintf('Using ''fit_trodes_offset_override'' value of %0.4f s', fit_trodes_offset_override);
  fit_trodes_offset = fit_trodes_offset_override;
  first_dio_after_gap = [];
else
  fit_trodes_offset = []; % will be set by first loop
  first_dio_after_gap = dios(dio_validi(1));
end

% initialize array of corrected pt (hack: _i, not _pvi)
ptNew = NaN(size(pt));

% save a copy before munging below
out.chunks_pvi_copy = chunks_pvi;

for j = 1:nchunks
  
  chunkj_pvi = chunks_pvi(j,1):chunks_pvi(j,2);
  % NB converting to _i from _pvi range here ignores any invalidated regions inside range
  chunkj_i = pt_validi(chunkj_pvi);     
 
  if diff(chunkj_i([1 end]))<min_chunklen
    fprintf('Chunk #%d has < min_chunklen (%d) frames, ignoring frames %d-%d\n', j,...
        min_chunklen, chunkj_i([1 end]));
    pt_validi(chunkj_pvi) = [];
    chunks_pvi(j:end,:) = chunks_pvi(j:end,:)-numel(chunkj_pvi);
%    error('min_chunklen handling currently broken')
    continue % to next chunk
  end
  
  [ptNew_chunk, fit_trodes_offset] = subf_regress_chunk(...
    camHWclock(chunkj_i),...
    camlogPt(chunkj_i),...
    first_dio_after_gap,...
    fit_trodes_offset); 

  assert(sum(isnan(ptNew_chunk))==0, 'nan in result of regression..');

  ptNew(chunkj_i) = ptNew_chunk;

  % display some stats for this chunk
  adjustments = pt(chunkj_i)-ptNew_chunk;
  prctiles = [0 2.5 25 50 75 95 97.5 100];
  adj_prct = prctile(adjustments, prctiles);
  fprintf('Chunk #%d (Frames #%d-%d, %d frames)\n', ...
    j, ...
    chunkj_i(1), chunkj_i(end), diff(chunkj_i([1 end]))+1);
  fprintf('Timestamp adjustment distribution:\n');
  fprintf(' %5g ', prctiles);
  fprintf(' (percentile)\n')
  fprintf(' %5.1f ', adj_prct*1000);
  fprintf(' (ms)\n\n');
  
  
end

adjustments = pt(pt_validi)-ptNew(pt_validi);
prctiles = [0 2.5 25 50 75 95 97.5 100];
adj_prct = prctile(adjustments, prctiles);
fprintf('Timestamp adjustment distribution (all ptNew):\n');
fprintf(' %5g ', prctiles);
fprintf(' (percentile)\n')
fprintf(' %5.1f ', adj_prct*1000);
fprintf(' (ms)\n\n');

nsections = 5;
pad = mod(numel(adjustments), nsections);
if(pad), pad = nsections-pad; end
adj_sections = reshape([adjustments(:) ; NaN(pad,1)],[],nsections);
adj_prct_section = prctile(adj_sections, prctiles, 1);

fprintf('Timestamp adjustments (ms) by percentile over time:\n');
fprintf('         Section (/%d)\n',nsections);
fprintf('    (%%)');
fprintf('%6d  ', 1:nsections);
fprintf('\n');
for k = 1:numel(prctiles)
  fprintf(' %5g', prctiles(k));
  fprintf(' %6.1f ', adj_prct_section(k,:)*1000);
  fprintf('\n');
end
fprintf('\n');

% Return only valid points as ptNew (per function contract);
assert(sum(~isnan(ptNew))==numel(pt_validi),'we don''t have a ptNew for each pt_validi point');
ptNew = ptNew(pt_validi);
ptNew_pi = pt_validi;


% testing and sanity checking

%% Plot final adjustments made to each postimestamp
if ~use_dios
  disp('**figs not currently plotted when use_dios = false**')
  
else
  figh_adj = figure;
  figh_adj.Position([3:4]) = [1000 800];
  axh_adj = subplot(3,1,1, 'parent', figh_adj);
  hold(axh_adj, 'on');
  grid(axh_adj, 'minor');
  
  title(axh_adj, sprintf('Position alignment plots for %s.',pos_basename), 'interpreter', 'none');
  
  dio_plotoffset = 0;
  ts_plotoffset = 0.05;
  yl = [-0.01 0.06];
  
  % Plot DIOs and uncorrected postimestamps
  h(1) = plot(axh_adj, out.dios, repmat(dio_plotoffset,numel(out.dios),1), 'k.');
  h(2) = plot(axh_adj, out.pt, ts_plotoffset+randn(size(out.pt))*0.002,'m.');
  
  % Plot 'adjustment vectors' showing where each postimestamp was reassigned
  n_ptNew = numel(ptNew);
  
  adjX_TD = [ptNew out.pt(ptNew_pi) NaN(n_ptNew,1)]';
  adjY_TD = [repmat(dio_plotoffset, n_ptNew,1) repmat(ts_plotoffset, n_ptNew,1) NaN(n_ptNew,1)]';
  h = [h plot(axh_adj, adjX_TD(:), adjY_TD(:),'color', [0.4 0.7 0.4])];
  
  xlabel(axh_adj, 'Original ''postimestamp'' (s)');
  set(axh_adj, 'ytick',[dio_plotoffset ts_plotoffset]);
  set(axh_adj, 'yticklabel',{'DIO', 'vidts'});
  pan(axh_adj, 'xon'); % horizontal pan
  zoom(axh_adj, 'xon'); % horizontal zoom
  
  legend (h, {'DIO times' 'VideoTimestamps (jittered Y pos)' 'Correction applied'});
  % plot(postimestamps-newTimes_DR)
  ylim(axh_adj, yl);
  
  
  %% diff of postimestamp vs 'real' camHWclock - pt to diagnose drift, freezes, skips, etc.
  axh_lag_t = subplot(3,1,2, 'parent', figh_adj);
  hold(axh_lag_t, 'on');
  grid(axh_lag_t, 'minor');
  
  clocks_diff = (out.camlogPt -out.camHWclock) - out.camlogPt(1);
  
  % plot(out.camHWclock, clocks_diff, 'k.');
  h2(1) = plot(axh_lag_t, out.pt, clocks_diff, 'm.');
  
  xlabel(axh_lag_t, 'Original ''postimestamp'' (s)');
  ylabel(axh_lag_t, {'offset from' 'camera clock (s)'})
  legend (h2, {'postimestamp-camHWclock offset'});
  
  linkaxes([axh_adj axh_lag_t], 'x');
  
  %% Same thing but with indexes (helpful for selecting ignoreSegs_pi)
  
  axh_lag_idx = subplot(3,1,3, 'parent', figh_adj);
  hold(axh_lag_idx, 'on');
  grid(axh_lag_idx, 'minor');
  
  h3(1) = plot(axh_lag_idx, clocks_diff, 'm.');
  
  xlabel(axh_lag_idx, 'Index into ''postimestamp''');
  ylabel(axh_lag_idx, {'offset from' 'camera clock (s)'})
  legend (h3, {'postimestamp-camHWclock offset'});
  
  if save_realignment_diagnostics
      figdir = [destdir '/realignment_diagnostics/'];
      figpath = [figdir sprintf('%s_figh_adj.png',pos_basename)];
      if ~isdir(figdir); mkdir(figdir); end
      fprintf('saving realignment fig to %s', figpath);
      saveas(gcf,figpath);
  end
  close(figh_adj)  % need to close the fig if batch processsing many days, otherwise will eat up all memory!
end


%% Display diagnostic text

fprintf('# postimestamps invalidated: %d\n', numel(pt)-numel(pt_validi));
  
% sanity check1: no original videotimestamp should ever precede corrected timestamps
fprintf('Average lag: %0.1f ms\n',mean(pt(pt_validi)-ptNew)*1000);

if any(pt(pt_validi)-ptNew<0)
  warning('--> No original videotimestamp should ever precede corrected timestamps (negative adjustments); \n press enter key to continue or ctrl-c to abort', 's');
%   input('', 's');
end
  
  % sanity check2: no duplicate timestamps should ever be found
if any(diff(ptNew)<=0)
%   warning('duplicate or inverted timestamps found');
  error('duplicate or inverted timestamps found');
end

% sanity check3: no nan timestamps should happen
if any(isnan(ptNew))
  error('nan timestamps found');
end

% sanity check4: no zero timestamps should happen
if (epochnum==1 && any(ptNew(2:end)==0)) || (epochnum>1 && any(ptNew==0))
  error('zero timestamps found');
end


fprintf('\n----END REALIGNING--------------------\n');

diary off
end


%%%%%%%%%%%%%%
% SUBFUNCTIONS

% Find first gap in list of timestamps meeting criteria
% (returns index of last time before gap, or [] if no gap found)
function [gapstarti, gapdur] = subf_gapdetect(times, gap_nsearch, gap_min_dur, gap_max_dur)
gap_diff = diff(times(1:gap_nsearch));
gapstarti = find((gap_diff > gap_min_dur & gap_diff < gap_max_dur),1);
gapdur = gap_diff(gapstarti);

end % end subfunction

function [freezes_pvi] = subf_findTrodesFreezes(pt_validi, pt, trodes_clockrate, cam_framerate)
%%%%
% Find Trodes Freezes/Headstage dropouts
%
% Look for *exactly* repeated postimestamps, which can happen when the
% Trodes clock freezes. (In old versions of MCU, disconnecting the
% headstage pauses Trodes time; and reconnecting does not reset the clock
% or introduce a gap. DIOs are lost during these gaps.  Newer MCU firmware
% keeps the Trodes clock going across these gaps, and continues to record
% DIOs.)
%
% We assume here that this is the *only* way we can get identical
% postimestamps. (NB: We don't see identical repeats during normal camera
% frame 'pileups', due to video latency. These present as short inter-frame
% intervals, and are handled separately.)
%
% TODO:
% -Move this to main extraction (part of adjust_timestamps?)
% -Generate list of all gaps in data and correct forward?
% -Detect dropouts in later versions of MCU (zeroed data across multiple
%  channels)
% -Use camhwlog/camHWclock to better estimate (and return) Trodes gaps (instead of number of video frames)

freezes_pvi = [];

% logical: is this frame a repeat of the previous one?
framerep = [0; diff(pt(pt_validi))==0];
framerep_n = sum(framerep); % number of repeats

if any(framerep)
  % find 'runs' of framereps (+1 if value begins a repeat, -1 if value 
  % is last repeat in a run)
  % (zero-padding means we always detect end of run at end of file)
  framerep_d = diff([framerep; 0]);
  framerep_starts = find(framerep_d==1);
  framerep_ends = find(framerep_d==-1);  
  freezes_pvi = [framerep_starts(:) framerep_ends(:)];
    
  fprintf('*** TRODES DATA FREEZE: %d video frames with identical repeated Trodes timestamps found. \n(Due to headstage disconnect)\n', ...
    sum(framerep)+size(freezes_pvi,1));

  
  for freezej = freezes_pvi'
    gapstart_t = pt(pt_validi(freezej(1)));
    gapend_t = pt(pt_validi(freezej(2)));
    gaplen_vidframes = diff(freezej)+1;
    
    fprintf(' At time = %0.4f (TS=%d; frame #%d; %0.0f%% of epoch video),\n ...Trodes clock paused for ~%0.2fs (%d video frames). \n ...Data during this gap presumed lost (including DIOs), except video.\n',...
      gapstart_t,...
      round(gapstart_t.*trodes_clockrate),...
      pt_validi(freezej(1)),...
      pt_validi(freezej(1))./numel(pt).*100,...
      gaplen_vidframes./cam_framerate,...
      gaplen_vidframes);
    
    if freezej(2) >= numel(framerep)
      fprintf(' ... --> Trodes data freeze continues to end of video. \n');
    end
  end
end

end % end subfunction



function [camDioCh] = subf_findCamDioCh(epDIO,...
  cam_framerate,...
  ratecheck_framerate_tol,fit_trodes_offset_override)

DIO_rate_mode = NaN(numel(epDIO),1);
DIO_rate_median = NaN(numel(epDIO),1);
DIO_isinput = false(numel(epDIO),1);

for d = 1:numel(epDIO)
  % warning('off', 'MATLAB:mode:EmptyInput')
  
  % We only want Digital inputs
  if epDIO{d}.input
    DIO_isinput(d) = true;
    
    t_hi = epDIO{d}.times(epDIO{d}.values==1);
    
    if numel(t_hi)<=10 % Boo, hard-coded param
        % can't calculate stats on <10 pulses; skip
        continue
    end
        
    % roughly correct framerate?
    DIO_rate_mode(d) = 1/mode(diff(t_hi));
    DIO_rate_median(d) = 1/median(diff(t_hi));
  end
end

ratecheck_framerate_range = cam_framerate + ...
  ([-1 1] * ratecheck_framerate_tol * cam_framerate);

goodrate_i = ...
  DIO_isinput & ...
  DIO_rate_mode > ratecheck_framerate_range(1) &...
  DIO_rate_mode < ratecheck_framerate_range(2) & ...
  DIO_rate_median > ratecheck_framerate_range(1) &...
  DIO_rate_median < ratecheck_framerate_range(2) ;

switch sum(goodrate_i)
    case 0
        camDioCh = [];
        warning('No suitable camerasync chan found in dios.')
        if ~isempty(fit_trodes_offset_override)
            fprintf('Using fit_offset_override rather than dios');
            camDioCh = []; 
        else
            error('Can''t determine best DIO channel, please specify ''camDioCh_override or fit_offset_override'' as an argument to pos_realign.');
        end
    case 1
        %only use pulse onset times (value =1 )
        camDioCh = find(goodrate_i);
        fprintf('Using camerasync chan %d for timestamp realigmnent. (Framerate mode = %g)\n\n',camDioCh, DIO_rate_mode(goodrate_i));
    otherwise
        camDioCh = find(goodrate_i);
        fprintf('Multiple potential camerasync chans found:\n');
        fprintf('Chan Idx, mode framerate (Hz)\n');
        fprintf('%9d,     %9d, %9d%%', camDioCh, DIO_rate_mode(camDioCh));
        fprintf(')\n');
        if ~isempty(intersect(camDioCh,32))
            camDioCh = 32;
            disp('hard setting to 32\n')
        elseif ~isempty(intersect(camDioCh,31))  %14 for incomplete nico dios
            camDioCh = 31;
            disp('hard setting to 31\n')
        else
            warning('No suitable camerasync chan found in dios.')
            if ~isempty(fit_trodes_offset_override)
                fprintf('Using fit_offset_override rather than dios');
                camDioCh = [];
            else
                error('Can''t determine best DIO channel, please specify ''camDioCh_override or fit_offset_override'' as an argument to pos_realign.');
            end
        end
end

end % end subfunction

% Do the regression-based alignment
function [ptNew_chunk, fit_trodes_offset] = subf_regress_chunk(camHWclock, camlogPt, first_dio_after_gap, fit_trodes_offset);

% Align using regression against PosTimestamps (assume HWTime increments
% are locally accurate). Won't capture average latency, but we can recover
% that separately from DIO/pause)
XX = camHWclock; 
Y = camlogPt; 
bls = polyfit(XX,Y,1);

% get fit 
postime_fit = XX .* bls(1) + bls(2);

if isempty(fit_trodes_offset)
  fit_trodes_offset = first_dio_after_gap-postime_fit(1);
  fprintf('Using calculated ''fit_trodes_offset'' value of %0.4f s. \n', fit_trodes_offset);
end

if fit_trodes_offset>0
    error('''fit_trodes_offset'' must be negative (actual camera frames must occur before cameramodule postimestamps)'); 
end

ptNew_chunk = postime_fit + fit_trodes_offset;

end % end subfunction
