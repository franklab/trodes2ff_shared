% Run waves2mda on a group of days and tetrodes
% Mari Sosa, 2017

rawdatadir = '/opt/data40/mari/GeronimoData/'; % must end in /
animal = 'geronimo'; %must be lowercase
daylist = 19;
tetlist = [1:3 7 9 13 14 16:20 21 23 24 27 28];

for day = daylist
    for tet = tetlist
        waves2mda(animal,rawdatadir,day,tet,'applytimefilter',1)
    end
end
