function extractEventConsensus(animaldir, animalprefix, eventname, eventeegname, days, tetfilters, min_suprathresh_duration, nstd, varargin)

% extractEventConsensus(FFanimdir, animalID, sprintf('%sripples',consensusRIParea{riparea}), 'ripple',sessionNum,conRIPtetfilters,RIPmindur,RIPnstd)

%function extractconsensus(directoryname, fileprefix, eventname, day, tetrode, min_suprathresh_duration, nstd, varargin)

%       Whereas extractripples + getriptimes extracts events from single tetrodes
%       and then subsequently constructs nrip vector, extractconsensus combines
%       event-band power across all valid detecting tetrodes then detects threshold SD events.

%       Follows Csicsvari--Buzsaki-1999 except instead of summing squared amplitudes
%       in "1.6 ms" windows, here keeps the original sampling rate (1500 Hz) and instead
%       Gaussian kernel smooths the combined power

% Aftewards, saves <animalprefix><event>con<day>.mat files in
% animdirect under {day}{epoch}

%
%fileprefix	- folder name where the day's data is stored
%
%day		- the day to process
%
%min_suprathresh_duration
%		- the time (in seconds) which the signal
%       must remain above threshold to be counted as as ripple; this guards
%       against short spikes in signal (e.g. noise) as being counted as
%       ripples. Set min_suprathreshold_duration to some small value, like
%       0.015 s.
%
%nstd		- the number of standard dev that ripple must be from mean to
%			be detextractconsensusected. Start with 2.
%
%options	'stdev', stdev   sets the size of the standard deviation used to
%				allow direct comparison across epochs
%       	'baseline', b   sets the size of the baseline used to
%				allow direct comparison across epochs
%           'maxpeakval, m	- ripples with maximal peaks above this value
%				are exluded.  Use this avoid detecting noise
%				events. Default 1000
%           'samethreshperday' - 0 or 1, default 0,
%               0 calculates baseline and threshold per epoch for each
%               tetrode
%               1 uses same threshold for all sessions on the same day on
%               the same tetrode.  Calculates this threshold based on the
%               baseline and stdev for all sessions (run and sleep) for
%               the entire day.    AS added 1-12-10
%
% Outputs:
%ripples 	- structue with various fields, including the following which
%			describe each ripple.
%	starttime - time of beginning of ripple
%	endtime	  - time of end of ripple
%	maxthresh - the largest threshold in stdev units at which this ripple
%			would still be detected.

fprintf('%s Event Konsensus processing %s\n',eventname,animalprefix)
consvariablename = [eventname 'kons'];
stdev = 0;
baseline = 0;
automaxval = 0;
autoclippercent = .01;  % too dangerous - no longer used. 
maxpeakval = []; %2000;
excludenoise = 1;
samethreshperday = 0;
excludeEvents = '';
min_separation = 0.015;
save_powertrace = 1;
% define the standard deviation for the Gaussian smoother which we
% apply before thresholding (this reduces sensitivity to spurious
% flucutations in the ripple envelope)
smoothing_width = 0.004; % 4 ms

if (~isempty(varargin))
    assign(varargin{:});
end

tetinfo = loaddatastruct(animaldir, animalprefix, 'tetinfo');
task = loaddatastruct(animaldir, animalprefix, 'task');

for d = days
    for ep = 1:length(task{d})
        if ep > length(tetinfo{d}) || ~isfield(task{d}{ep},'type') || strcmp(task{d}{ep}.type,'failed sleep')
            disp('task struct has one more epoch than in tetinfo (or aberrant .type) - disregarding this epoch')
            continue
        end
        for TF = 1:length(tetfilters)
            tetfilter = tetfilters{TF};
            % retrieve valid tetrodes to iterate through
            tetlist =  evaluatefilter(tetinfo{d}{ep},tetfilter)';
            fprintf(' %d\n',tetlist);
            if isempty(tetlist)
                fprintf('no valid detecting tetrodes, d %d e %d \n',d,ep);
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = tetfilter;
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
                ev.peak_value = [];
                ev.peak_index = [];
                ev.total_area = [];
                ev.area_midpoint_index = [];
                ev.total_energy = [];
                ev.energy_midpoint_index = [];
                ev.absolute_maxthresh = [];
                eventscons{d}{ep}{TF} = ev;
                clear ev;
                continue
            end
            
            % load eeg data
            eventeeg = loadeegstruct(animaldir, animalprefix, eventeegname, d, ep, tetlist);
            eventeeg{d}{ep}{tetlist(1)}.starttime = double(eventeeg{d}{ep}{tetlist(1)}.starttime);
            % obtain times vector from one of the tetrodes as a reference
            eegtimesvec_ref = geteegtimes(eventeeg{d}{ep}{tetlist(1)});
            epoch_starttime = eegtimesvec_ref(1);
            epoch_endtime = eegtimesvec_ref(end);
            numsamples_refeeg = length(eventeeg{d}{ep}{tetlist(1)}.data(:,1));
            if numsamples_refeeg ~= length(eegtimesvec_ref)
                error('mismatch num samples and times')
            end
            % obtain the squared amplitudes from each tetrode
            sqamps = nan(length(tetlist),length(eegtimesvec_ref));  % initialize
            for tt = 1:length(tetlist)
                tet = tetlist(tt);
                data = double(eventeeg{d}{ep}{tet}.data(:,1))';
                % if there are brief periods of noise (filtered amplitude exceeds maxval),
                % set values to 0 as a patch fix and report
                highnoise = [];
                %                 if automaxval
                %                     datasorted = sort(abs(data));
                %                     maxpeakvalauto = datasorted(ceil(length(datasorted)*(1-autoclippercent))); %trim top .1% of raw values. 20min epoch ~ 1 second cut
                %                     highnoise = (abs(data) > maxpeakvalauto);
                %                 else
                if ~isempty(maxpeakval)
                    fprintf('anim %s day%dep%d tet %d: using %duV as noise threshold for exclusion in event detection\n',animalprefix,d,ep,tet,maxpeakval)
                    highnoise = (abs(data) > maxpeakval);
                else
                    fprintf('anim %s day%dep%d tet %d: No noise threshold specified for event detection\n',animalprefix,d,ep,tet)
                end
                if any(highnoise) && excludenoise
                    ms_of_noise = round(1000*sum(highnoise)/1500);
                    fprintf('high noise periods totalling %d millisec (if 1500 Hz) excluded in threshold calc\n',ms_of_noise)
                    data(highnoise) = 0;
                end
                if length(data) == numsamples_refeeg
                    sqamps(tt,:) = data.^2;
                else
                    % if mismatch in # of samples for this tetrode (different DSPs often start at slightly different times),
                    % 'interpolate' (really just quick way of appending NaNs on the deficient end)
                    eegtimesvec = geteegtimes(eventeeg{d}{ep}{tet});
                    interpdata = interp1(eegtimesvec,data,eegtimesvec_ref,'nearest')';
                    sqamps(tt,:) = interpdata.^2;
                end
            end
            % basic values
            samprate = eventeeg{d}{ep}{tetlist(1)}.samprate;
            mindur = round(min_suprathresh_duration * samprate);
            
            % individually smooth the squared amplitudes, then square root
            kernel = gaussian(smoothing_width*samprate, ceil(8*smoothing_width*samprate));
            rms_amps = nan(size(sqamps));
            for tt2 = 1:length(tetlist)
                rms_amps(tt2,:) = realsqrt(smoothvect(sqamps(tt2,:), kernel));
            end
            % Now take the mean across sites
            powertrace = mean(rms_amps,1);
            
            %NOISE
            excludedtimes = [];
            if strcmp(excludeEvents, 'noise')
                noises = load(sprintf('%s%s%snoisekons%02d',animaldir, animalprefix, ...
                    tetinfo{d}{ep}{tetlist(1)}.area, d));
                try
                    eval(sprintf('exclude = noises.%snoisekons{d}{ep}{1};',...
                        tetinfo{d}{ep}{tetlist(1)}.area))
                    excludedtimes = list2vec([exclude.starttime, exclude.endtime], ...
                        exclude.eegtimesvec_ref);
                    totaltimeexcluded = sum(excludedtimes)/samprate;
                    fprintf('%d seconds of noise events excluded from ripple detection', ...
                        totaltimeexcluded);
                catch
                    disp('no noise events for this epoch')
                end
            end
            %times marked as noise don't go into threshold calculation
            powertrace(logical(excludedtimes)) = NaN;
            ev.excludedtimes = excludedtimes;
            
            % calculate mean and std of powertrace, to use in extractevents
            powertrace_values = powertrace(~isnan(powertrace));
            baseline = mean(powertrace_values);
            stdev = std(powertrace_values);
            thresh = baseline + nstd * stdev;
            if any(~isreal([baseline stdev]))
                error('these should be real valued..')
            end
            % extract the events if this is a valid trace
            if (thresh > 0) && any(find(powertrace<baseline))
                fprintf('day %d epoch %d %s\n',d,ep, consvariablename)
                % bug within the extractevents .mex makes it lock up
                % if the powertrace is not doubled.. so take the first half of the output
                tmpevent = extractevents([powertrace'; powertrace'], thresh, baseline, ...
                    min_separation, mindur, 0)';
                if mod(size(tmpevent,1),2)==0
                    lastind = size(tmpevent,1)/2;
                else
                    lastind = (size(tmpevent,1)-1)/2;
                end
                tmpevent = tmpevent(1:lastind,:);
                % drop any events that contain a noise event
                % this happens if the edge of an excluded period is suprathreshold
                exclev = [];
                for iev = 1:length(tmpevent(:,1))
                    if any(isnan(powertrace(tmpevent(iev,1):tmpevent(iev,2))))
                        exclev(end+1) = iev;
                    end
                end
                tmpevent(exclev,:) = [];
                
                % Assign the fields
                ev.eventname = eventname;
                ev.nstd = nstd;
                ev.min_suprathresh_duration = min_suprathresh_duration;
                ev.tetfilter = tetfilter;
                ev.tetlist = tetlist;
                ev.starttime = epoch_starttime + tmpevent(:,1) / samprate;
                ev.endtime = epoch_starttime + tmpevent(:,2) / samprate;
                ev.maxthresh = (tmpevent(:,9) - baseline) / stdev; %z scored
                ev.peak_value = tmpevent(:,3);
                ev.peak_index = tmpevent(:,4);
                ev.total_area = tmpevent(:,5);
                ev.area_midpoint_index = tmpevent(:,6);
                ev.total_energy = tmpevent(:,7);
                ev.energy_midpoint_index = tmpevent(:,8);
                ev.absolute_maxthresh = tmpevent(:,9); %absolute
            else
                ev.eventname = '';
                ev.nstd = [];
                ev.min_suprathresh_duration = [];
                ev.tetfilter = '';
                ev.tetlist = [];
                ev.starttime = [];
                ev.endtime = [];
                ev.maxthresh = [];
                ev.peak_value = [];
                ev.peak_index = [];
                ev.total_area = [];
                ev.area_midpoint_index = [];
                ev.total_energy = [];
                ev.energy_midpoint_index = [];
                ev.absolute_maxthresh = [];
            end
            
            if any(find(powertrace<baseline))==0
                warning(['No below baseline values in data.  Fields left blank, day ' num2str(d) 'epoch ' num2str(ep)])
            end
            
            ev.timerange = [epoch_starttime epoch_endtime];
            ev.samprate = samprate;
            ev.baseline = baseline;
            ev.std = stdev;
            
            if save_powertrace
                ev.eegtimesvec_ref = eegtimesvec_ref;
                ev.powertrace = powertrace;
            end
            
            clear eventeeg
            eventskons{d}{ep}{TF} = ev;
            clear ev;
        end
    end
    
    if exist('eventskons','var')
        eval([consvariablename ' = eventskons;']);
        save(sprintf('%s/%s%skons%02d.mat', animaldir, animalprefix, eventname,d),consvariablename);
        cd(animaldir)
        clear eventskons;
    else
        fprintf('consensus not saved for %s %s, day %d\n',animalprefix,eventname,d)
    end
    
end

end
