%%
%% make cellinfo
mdir='/home/jojo/RemoteData2/shijie/animals/Jaq/mountainlab_output/';
sessionNum=1;
datestring='20190826';
animalID = 'jaq';
preprocdir = '/home/jojo/RemoteData2/shijie/animals/Jaq/preprocessing/';
FFanimdir = '/home/jojo/RemoteData2/shijie/animals/Jaq/filterframework/';
sorter = 'ms4';
ntrodestosort = [];
Binarymda2FF_SPIKES(animalID, preprocdaydir, FFanimdir, sessionNum, datestring,...
        'sorter',sorter,'ntrodes',ntrodestosort, 'metricsperepoch',1,'mdakind','firings_burst_merged.mda');
    
    
%% test if you have run mountainsort's metrics per epoch correctly
trodeNum=1;
epochNum=2;
%load test json
json_file_name=sprintf('metrics_processed_nt%02d_epoch%d.json',trodeNum,epochNum);
metrics_test=loadjson(fullfile(mountaindir,json_file_name));
metrics_test=metrics_test.clusters;


% load mat, pick session, epoch, trode
load(fullfile(destdir,[animalID 'cellinfo.mat']))
metrics=cellinfo{sessionNum}{epochNum}{trodeNum};
ncluster=length(metrics);

clear T
T=struct('firing_rate',[],'firing_rate_test',[]);

% tally each
e=0;
for c=1:ncluster
    l=metrics{c}.label;
    T(l).firing_rate=metrics{c}.firing_rate_precise;
    
    l=metrics_test{c}.label;
    T(l).firing_rate_test=metrics_test{c}.metrics.firing_rate;
end

% compare 
ncluster=length(T);
e=nan(1,ncluster);
for c=1:ncluster
    if ~isempty(T(c).firing_rate)
        e(c)=(T(c).firing_rate-T(c).firing_rate_test);
    end
end
%disp(['Error is ', num2str(e),'. If E is close to 0, you good.'])