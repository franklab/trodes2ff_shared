function addTaskEnvs(FFanimdir, animalID, day, epoch, varargin)

% ADDTASKINFO(animdirect, fileprefix, days, epochs, property1, value1, property2, value2, ...)
%
% This function adds descriptive information to the 'task' structure of a
% given animal. It saves the information for you.
%
% ANIMDIRECT -- folder where all processed data will be stored.  Example: '/data13/mkarlsso/Con/'
% FILEPREFIX -- also the first three letters of the animal's name (example 'con'), which will attach to
%              the beginning of the .mat files containing the variables.  I recommend using this.
%              If not, type ''.
% DAYS -- this is a vector containing the day numbers to add the information to.
% EPOCHS -- a vector containing the epoch numbers to add info to, example: [2 4 6]
% Properties:
%   These items are added as fields to the structure.  Two reserved
%   properties are:
%   'exposure' - must be the same length as the number of days * number of epochs (counts number of epoch exposures on the environment)
%   'exposureday' - must be the same length as the number of days
%   'experimentday' - must be the same length as the number of days
%   Any other names that the user wants to add are processed and added to the
%   structure.  The value will be copied across all epochs.

fieldvar = [];

if length(varargin) == 1; %DR.. if vargargin is a cell of cells of strings, explode it
    varargin = varargin{:}; %this allows for passing a variable length cell array as the varargin input.. see Trodes_dayprocess.m
end
for option = 1:2:length(varargin)-1
    fieldvarcount = fieldvarcount + 1;
    if isstr(varargin{option})
        fieldvar(fieldvarcount).name = varargin{option};
        fieldvar(fieldvarcount).value = varargin{option+1};
    else
        error('Fields must be strings');
    end
end

loaddatastruct(FFanimdir,animalID, 'task',day)
if ~isempty(fieldvar)
    for k = 1:length(fieldvar)
        task{day}{epoch} = setfield(task{day}{epoch},fieldvar(k).name,fieldvar(k).value);
    end
end
% for each day, save out a task info for legacy compatibility
fulltaskinfo = task;
for iday = 1:length(fulltaskinfo)
    if isempty(fulltaskinfo{iday})
        continue
    end
    task = cell(1,length(fulltaskinfo));
    task{iday} = fulltaskinfo{iday};
    save(sprintf('%s/%stask%02d.mat', FFanimdir, animalID, iday), 'task')
end
%     save(filename,'taskinfo');
    
end
      

