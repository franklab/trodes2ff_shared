function tagNTrodes(FFanimdir, animalID, sessionNum, taginput, varargin)

%Demetris Roumis Feb 2017

% add or update tags on nTrode info struct
% tag input contains an array of cells, with each row  specifying any set
% of tagVariable, tagValue, ntrodes, days in any order.
% leave out or leave empty the days, epochs, ntrodes Tags to include ALL.

% nTrodeTags = [...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]};...
% {[{'var1'}, {[val1]}, {'var2'}, {'val2'},...{'varn'}, {'valn'}]}];

% For example:
% nTrodesTag = [...
% {[{'suparea'}, {'hpc'}, {'area'}, {'ca1'}, {'ntrodes'}, {[16:30]}, {'days'}, {[]}]};
% {[{'suparea'}, {'ctx'}, {'area'}, {'mec'}, {'ntrodes'}, {[1:4 6:10]}]}, {'days'}, {[1:7]}]}, {'epochs'}, {[2 4 6]}]};
% {[{'suparea'}, {'ctx'}, {'area'}, {'por'}, {'ntrodes'}, {[13 15]}]};
% {[{'area'}, {'sub'}, {'ntrodes'}, {[5]}]};
% {[{'suparea'}, {'ctx'}, {'area'}, {'v2l'}, {'ntrodes'}, {[12]}]};
% {[{'subarea'}, {'lr2'}, {'ntrodes'}, {[1 8 9]}]};
% {[{'subarea'}, {'lr3'}, {'ntrodes'}, {[10]}]};
% {[{'subarea'}, {'lr5'}, {'ntrodes'}, {[2 3 7]}]};
% {[{'subarea'}, {'lr6'}, {'ntrodes'}, {[4 6 12:15]}]}];

if ~isempty(varargin)
    assign(varargin{:});
end

cd(FFanimdir);
fprintf('updating tet and cell info structs.sessionNum %d\n', sessionNum)

% check/load info struct
try
    load([FFanimdir, animalID, 'tetinfo']);
    if isempty(tetinfo{sessionNum}); %index exceeds matrix dimensions error if cell(sessionNum) doesn't exist
        error('tetinfo is empty');
    end
catch
    disp('tetinfo structure does not exist or is empty.. running createtetinfostruct')
    createtetinfostruct(FFanimdir, animalID,1); 
    load([FFanimdir, animalID, 'tetinfo']);
end


nTinds = cellfetch(tetinfo, ' '); %this is just a hack to map the nested cell array to indices.. assumes a field 'depth'
targetntrodes = [];

for rowtag = 1:length(taginput(:,1))
    days = []; epochs = []; ntrodes = [];
    rowtaginput = taginput{rowtag}(1,:); %get current rowset of tags
    vars = {rowtaginput{1:2:end}};
    vals = {rowtaginput{2:2:end}};
    DENvarsInds= ismember(vars, [{'days'},{'epochs'},{'ntrodes'}]); %search for Day Epoch Ntrodes variables
    DENvars = vars(DENvarsInds);
    DENvals = vals(DENvarsInds);
    tagvars = vars(~DENvarsInds); %collect tags (excluding day,epoch,ntrodes)
    tagvals = vals(~DENvarsInds);
    for i = 1:length(DENvars)
        eval(sprintf('%s=%s;',DENvars{i},mat2str(DENvals{i}))); %assign in day epoch ntrode vars to workspace
    end
    
    %set day,epoch,ntrode filters to ALL if empty
    if isempty(days)
        days = unique(nTinds.index(:,1));
    end
    if isempty(epochs)
        epochs = unique(nTinds.index(:,2));
    end
    if isempty(ntrodes)
        ntrodes = unique(nTinds.index(:,3));
    end
    
    %filter filterdays, sessionNum,  then epochs, then ntrodes.
    targetdays = nTinds.index(find(ismember(nTinds.index(:,1),sessionNum)),:); %include only sessionNum(s)
    targetdays = targetdays(find(ismember(targetdays(:,1),days)),:); %include only in days tag
    if isempty(targetdays); %if this tag set excludes the current sessionNum (day)
        continue %move on to the next tag set
    end
    targetepochs = targetdays(find(ismember(targetdays(:,2),epochs)),:);
    targetntrodes = targetepochs(find(ismember(targetepochs(:,3),ntrodes)),:);    
    for i =1:size(targetntrodes,1)
        for tagn = 1:length(tagvars) %add tag var and val to this day,epoch,ntrode
            eval(sprintf('tetinfo{targetntrodes(i,1)}{targetntrodes(i,2)}{targetntrodes(i,3)}.%s = %s;', tagvars{tagn}, mat2str(tagvals{tagn})));
        end
    end
end
save([FFanimdir, animalID,'tetinfo'], 'tetinfo');
end

